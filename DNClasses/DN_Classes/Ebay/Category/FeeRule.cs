﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DN_Classes.Ebay.Category
{
    public class FeeRule
    {
        public string Id { get; set; }
        public double Percentage { get; set; }
        public int SalesPriceLimit { get; set; }
        public List<FeeRule> RuleExceptions { get; set; }

        public FeeRule(string id, double percentage, int salesPriceLimit = int.MaxValue)
        {
            Id = id;
            Percentage = percentage;
            SalesPriceLimit = salesPriceLimit;
            RuleExceptions = new List<FeeRule>();
        }

        public FeeRule(string id, double percentage, List<FeeRule> ruleExceptions, int salesPriceLimit = int.MaxValue)
        {
            Id = id;
            Percentage = percentage;
            SalesPriceLimit = salesPriceLimit;
            RuleExceptions = ruleExceptions;
        }

        public double GetFee(double salesPrice)
        {
            var tempSalesPrice = Math.Min(salesPrice, SalesPriceLimit);
            return tempSalesPrice * Percentage / 100;
        }



        public FeeRule GiveDeepestRule(List<string> categoryIds)
        {
            foreach (var rule in RuleExceptions)
            {
                var deepest = rule.GiveDeepestRule(categoryIds);
                if (deepest != null)
                {
                    return deepest;
                }
            }
            if (categoryIds.Contains(Id))
            {
                return this;
            }
            return null;
        }

    }
}
