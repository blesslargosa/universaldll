﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DN_Classes.Ebay.Category
{
    public class EbayFeeFinder
    {
        public List<FeeRule> feeRules = new List<FeeRule>();
        public EbayFeeFinder()
        {
            feeRules.Add(new FeeRule("26395", 9));
            feeRules.Add(new FeeRule("171833", 5, 200));
            feeRules.Add(new FeeRule("159912", 9.5, 200));
            feeRules.Add(new FeeRule("3187", 9.5, 200));
            feeRules.Add(new FeeRule("220", 9.5));
            feeRules.Add(new FeeRule("267", 9.5));
            feeRules.Add(new FeeRule("2984", 9.5));
            feeRules.Add(new FeeRule("11450", 10));

            feeRules.Add(new FeeRule("11232", 7));
            feeRules.Add(new FeeRule("11233", 7));
            feeRules.Add(new FeeRule("1249", 7));
            feeRules.Add(new FeeRule("139977", 5, 200));
            feeRules.Add(new FeeRule("1305", 7));
            feeRules.Add(new FeeRule("11700", 9));
            feeRules.Add(new FeeRule("619", 8, 300));
            feeRules.Add(new FeeRule("3252", 8));
            feeRules.Add(new FeeRule("888", 8));
            feeRules.Add(new FeeRule("353", 9));
            feeRules.Add(new FeeRule("1", 6));
            feeRules.Add(new FeeRule("260", 6));
            feeRules.Add(new FeeRule("11116", 6));
            feeRules.Add(new FeeRule("20710", 5, 200));

            #region creating tree for rule_131090

            var rule_131090 = new FeeRule("131090", 9);
            rule_131090.RuleExceptions.Add(new FeeRule("14258", 5, 200));
            rule_131090.RuleExceptions.Add(new FeeRule("179680", 4.5, 200));
            rule_131090.RuleExceptions.Add(new FeeRule("179679", 4.5, 200));
            rule_131090.RuleExceptions.Add(new FeeRule("179681", 4.5, 200));

            feeRules.Add(rule_131090);
            #endregion

            #region creating tree for rule_58058

            var rule_58058 = new FeeRule("58058", 5, 200);
            var rule_171961 = new FeeRule("171961", 8, 150);
            rule_171961.RuleExceptions.Add(new FeeRule("1245", 5, 200));
            rule_171961.RuleExceptions.Add(new FeeRule("11205", 5, 200));
            rule_58058.RuleExceptions.Add(rule_171961);
            rule_58058.RuleExceptions.Add(new FeeRule("31491", 8, 150));
            rule_58058.RuleExceptions.Add(new FeeRule("31530", 8, 150));
            rule_58058.RuleExceptions.Add(new FeeRule("3676", 8, 150));
            rule_58058.RuleExceptions.Add(new FeeRule("176970", 8, 150));


            feeRules.Add(rule_58058);

            #endregion

            #region creating tree for rule_625

            var rule_625 = new FeeRule("625", 5, 200);
            var rule_15200 = new FeeRule("15200", 8, 150);
            rule_15200.RuleExceptions.Add(new FeeRule("18871", 5, 10));
            var rule_78997 = new FeeRule("78997", 8, 150);
            rule_78997.RuleExceptions.Add(new FeeRule("3323", 5, 10));
            rule_625.RuleExceptions.Add(rule_15200);
            rule_625.RuleExceptions.Add(rule_78997);
            rule_625.RuleExceptions.Add(new FeeRule("30090", 8, 150));

            feeRules.Add(rule_625);
            #endregion

            #region creating tree for rule_15032

            var rule_15032 = new FeeRule("15032", 5, 200);
            var rule_9394 = new FeeRule("9394", 8, 150);
            rule_9394.RuleExceptions.Add(new FeeRule("96991", 5, 10));
            rule_15032.RuleExceptions.Add(rule_9394);
            feeRules.Add(rule_15032);

            #endregion

            #region creating tree for rule_293

            var rule_293 = new FeeRule("293", 5, 200);
            rule_293.RuleExceptions.Add(new FeeRule("48446", 8, 150));
            rule_293.RuleExceptions.Add(new FeeRule("14961", 8, 150));
            rule_293.RuleExceptions.Add(new FeeRule("56169", 8, 150));
            rule_293.RuleExceptions.Add(new FeeRule("38583", 7));
            rule_293.RuleExceptions.Add(new FeeRule("156595", 7));
            rule_293.RuleExceptions.Add(new FeeRule("156597", 7));
            rule_293.RuleExceptions.Add(new FeeRule("187", 7));

            var rule_54968 = new FeeRule("54968", 8, 150);
            rule_54968.RuleExceptions.Add(new FeeRule("117045", 5, 10));

            rule_293.RuleExceptions.Add(rule_54968);
            feeRules.Add(rule_293);

            #endregion

            #region creating tree for rule_281

            var rule_281 = new FeeRule("281", 10.5, 500);
            rule_281.RuleExceptions.Add(new FeeRule("14324", 9.5, 500));
            rule_281.RuleExceptions.Add(new FeeRule("10682", 9.5, 500));
            rule_281.RuleExceptions.Add(new FeeRule("20558", 9.5, 500));
            rule_281.RuleExceptions.Add(new FeeRule("9295", 9.5, 500));


            feeRules.Add(rule_281);
            #endregion


        }

        public double GetFee(List<string> categoryList, double salesPrice)
        {
            FeeRule feerule = GetPrimaryFeeRule(categoryList);

            return feerule.GetFee(salesPrice);
            //var rule = feeRules.Where(x => x.GiveDeepestRule(categoryList) != null);
            //if (rule.Count() > 0)
            //{
            //    return rule.First().GetFee(salesPrice);
            //}
            //else
            //{
            //    return salesPrice * 8.5 / 100;
            //}
        }

        public FeeRule GetPrimaryFeeRule(List<string> categoryList)
        {
            FeeRule defaultFeeRule = new FeeRule("default", 8.5);
            var rule = feeRules.Where(x => x.GiveDeepestRule(categoryList) != null);
            if (rule.Count() > 0)
            {
                return rule.First();
            }

            return defaultFeeRule;
        }

    }
}
