﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using eBay.Service.Call;
using eBay.Service.Core.Sdk;
using eBay.Service.Util;
using eBay.Service.Core.Soap;

using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace DN_Classes.Entities
{
    [BsonIgnoreExtraElements]
    public class EbayOrderEntity
    {
        public ObjectId id { get; set; }
        public OrderType order { get; set; }
        public BsonDocument plus { get; set; }


        public EbayOrderEntity()
        { }

        public EbayOrderEntity(OrderType orderType)
        {
            order = orderType; 
        }
    }


    
}
