﻿using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DN_Classes.Rules.SellingRules
{
    public class DefaultProductSelling : IProductSelling
    {
        public bool IsSellingReady<T>(T obj)
        {
            if (obj == null)
            {
                throw new ArgumentNullException("argument obj must be set");
            }

            BsonDocument plus = new BsonDocument();
            try
            {
                plus = (BsonDocument)(object)obj;
            }
            catch { }

            if (!plus.Contains("startselling")|| (plus.Contains("startselling") && DateTime.ParseExact(plus["startselling"].AsString, "dd-MM-yyyy", CultureInfo.InvariantCulture) <= DateTime.Now.Date))
            {
                return true; 
            }
            else
            {
                return false;
            }
        }
    }
}
