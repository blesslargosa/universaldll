﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DN_Classes.Rules.SellingRules
{
    public interface IProductSelling
    {
        bool IsSellingReady<T>(T obj);
    }
}
