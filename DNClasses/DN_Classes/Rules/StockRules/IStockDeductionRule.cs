﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DN_Classes.Rules.StockRules
{
    public interface IStockDeductionRule
    {
        int GetFinalStock(string ean, int currentStock);
    }
}
