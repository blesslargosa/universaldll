﻿using DN_Classes.DBCollection.SingleCalculatedOrders;
using MongoDB.Bson;
using MongoDB.Driver.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DN_Classes.Rules.StockRules
{
    public class SoldAndUnshipStockDeductionRule : IStockDeductionRule
    {
        public int GetFinalStock(string ean, int currentStock)
        {
            var matchedProducts = CalculatedDBCollection.SingleCalculatedCollection.Find(Query.And(Query.EQ("Ean", ean), Query.GT("PurchasedDate", DateTime.Now.AddDays(-31)))).SetFields("plus");

            int deductionCount = 0;

            if (matchedProducts.Count() > 0)
            {
                deductionCount = matchedProducts
                     .Where(x =>
                                x.plus == null ||
                                (x.plus != null && !x.plus.Contains("dVersandt")) ||
                                (x.plus != null && x.plus.Contains("dVersandt") && x.plus["dVersandt"].AsBsonValue == BsonNull.Value) ||
                                (x.plus != null && x.plus.Contains("dVersandt") && x.plus["dVersandt"].AsBsonValue == null) ||
                                (x.plus != null && x.plus.Contains("dVersandt") && x.plus["dVersandt"].AsString == "")

                         )
                     .Count();
            }

            return currentStock - deductionCount;
        }
    }
}
