﻿using DN_Classes.Entities.Warehouse;
using DN_Classes.Interfaces.Warehouse;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DN_Classes.Classes.Warehouse
{
   public class WareHouseItems : IWareHouse
    {
       
        public List<WarehouseEntity> warehouse { get; set; }
        public double totalAllItemsAmount { get; set; }
		public double totalSupplierItemsAmount { get; set; }
		public double totalUsedBooksItemsAmount { get; set; }
		public int totalSupplierQuantity { get; set; }
		public int totalUsedBooksQuantity { get; set; }
        public List<WarehouseEntity> usedbooks { get; set; }
        public List<WarehouseEntity> supplierItems { get; set; }

       public WareHouseItems(List<WarehouseEntity> usedbooks, List<WarehouseEntity> supplierItems)
       {
           this.usedbooks = usedbooks;
           this.supplierItems = supplierItems;



           this.warehouse = GetWareHouseItems();
           this.totalAllItemsAmount = GetTotalAmount();
		   this.totalSupplierItemsAmount = GetSupplierItemsTotalAmount();
		   this.totalUsedBooksItemsAmount = GetUsedBooksTotalAmount();

		   this.totalSupplierQuantity = GetTotalSupplierQuantity();
		   this.totalUsedBooksQuantity = GetTotalUsedBooksQuantity();

       }
       public List<WarehouseEntity> GetWareHouseItems()
        {
            List<WarehouseEntity> we = new List<WarehouseEntity>();
            we.AddRange(this.usedbooks);
            we.AddRange(this.supplierItems);

            return we;

        }

	   public int GetTotalSupplierQuantity()
	   {
		   return supplierItems.Sum(x => x.totalQuantity);
	   }

	   public int GetTotalUsedBooksQuantity()
	   {
		   return usedbooks.Sum(x => x.totalQuantity);
	   }

        public double GetTotalAmount()
        {
            return warehouse.Sum(x => x.totalAmount);
        }

		public double GetSupplierItemsTotalAmount()
		{
			return supplierItems.Sum(x => x.totalAmount);
		}

		public double GetUsedBooksTotalAmount()
		{
			return usedbooks.Sum(x => x.totalAmount);
		}
    }
}
