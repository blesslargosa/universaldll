﻿using DN_Classes.Entities.Warehouse;
using DN_Classes.Interfaces.Warehouse;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DN_Classes.Classes.Warehouse
{
    public class UsedBooks :IWareHouse
    {
        public List<JtlEntity> items { get; set; }
        public List<WarehouseEntity> warehouse { get; set; }
        public double totalUsedBookAmount {get; set;}
		public int totalUsedBookQuantity { get; set; }

        public UsedBooks(List<JtlEntity> items)
        {
            this.items = items;
            this.warehouse = GetWareHouseItems();
            this.totalUsedBookAmount = GetTotalAmount();
			this.totalUsedBookQuantity = GetTotalQuantity();
        }

		private int GetTotalQuantity()
		{
			return warehouse.Sum(x => x.totalQuantity);
		}

        public List<WarehouseEntity> GetWareHouseItems()
        {
           List<WarehouseEntity> we = new List<WarehouseEntity>();

           this.items.ForEach(x => x.product.supplier = "USED BOOKS");

           var usedbooks = this.items.GroupBy(y => y.product.supplier)
							.Select(z =>
								new WarehouseSingleItem(
									z.Key, z.ToList()) 
									).ToList();


			we.AddRange(usedbooks);

           return we;
        }

        public double GetTotalAmount()
        {
           return warehouse.Sum(x => x.totalAmount);
        }
    }
}
