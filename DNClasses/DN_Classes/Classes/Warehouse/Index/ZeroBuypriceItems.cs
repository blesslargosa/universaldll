﻿using DN_Classes.Entities.Warehouse;
using DN_Classes.Interfaces.Warehouse;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DN_Classes.Classes.Warehouse.Index
{
    public class ZeroBuypriceItems : IWareHouse
    {

        public List<JtlEntity> items { get; set; }
        public List<WarehouseEntity> warehouse { get; set; }
        public int totalSupplierQuantity { get; set; }

        public ZeroBuypriceItems(List<JtlEntity> items)
        { 
        
          this.items = items;
          this.warehouse = GetWareHouseItems();
		  this.totalSupplierQuantity = GetTotalQuantity();
        }

        private int GetTotalQuantity()
        {
            return warehouse.Count;
        }

        public List<WarehouseEntity> GetWareHouseItems()
        {
            List<WarehouseEntity> we = new List<WarehouseEntity>();
            var supplierItems = this.items.Where( x => x.product.supplier == "").GroupBy( i => i)
                .Select(z => new WarehouseSingleItem("", z.ToList())).ToList();

            we.AddRange(supplierItems);

            return we;
 
        }

        public double GetTotalAmount()
        {
            throw new NotImplementedException();
        }
    }
}
