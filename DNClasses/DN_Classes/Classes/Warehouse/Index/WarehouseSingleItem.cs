﻿using DN_Classes.Entities.Warehouse;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DN_Classes.Classes.Warehouse
{
    public class WarehouseSingleItem : WarehouseEntity
    {
        public WarehouseSingleItem(string supplier, List<JtlEntity> product)
        {
            var wp = new WarehouseProducts().GetWarehouseProducts(product.ToList()); 
          
            this.supplier = supplier;
            //this.product = wp.OrderByDescending(x => x.quantity).ThenByDescending(x => x.prodtotalamount).ToList();
			this.product = wp.OrderByDescending(x => x.prodtotalamount).ToList();
            this.totalAmount = wp.Sum(x => x.prodtotalamount);
			this.totalQuantity = wp.Sum(x => x.quantity);
        }
    }
}
