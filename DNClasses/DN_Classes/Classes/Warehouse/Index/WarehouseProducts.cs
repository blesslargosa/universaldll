﻿using DN_Classes.Entities.OrderingTool;
using DN_Classes.Entities.Warehouse;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DN_Classes.Classes.Warehouse
{
    public class WarehouseProducts
    {
        public List<WarehouseProductEntity> GetWarehouseProducts(List<DN_Classes.Entities.Warehouse.JtlEntity> jtlList)
        {
            List<WarehouseProductEntity> WarehouseItems = new List<WarehouseProductEntity>();

            jtlList.ForEach(x => WarehouseItems.Add(new SetEntity(x.product.artikelnummer,
                                               x.product.ean,
                                               x.product.buyprice,
                                               Convert.ToInt32(x.plus["quantity"]),
											   properArtikelname(x.product.artikelname),
											   x.product.pricetype)));


            return WarehouseItems;
        }

		public string properArtikelname(string item)
		{
			string newArtnr = "";

			byte[] bytes = Encoding.Default.GetBytes(item);
			newArtnr = Encoding.Default.GetString(bytes);

			return newArtnr;
		}
    }
}
