﻿using DN_Classes.Entities.Warehouse;
using DN_Classes.Interfaces.Warehouse;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DN_Classes.Classes.Warehouse
{
    public class SupplierItems : IWareHouse
    {

        public List<JtlEntity> items { get; set; }
        public List<WarehouseEntity> warehouse { get; set; }
        public double totalSupplierItemsAmount { get; set; }
		public int totalSupplierQuantity { get; set; }
        public List<JtlEntity> zeroBuypricesItems { get; set; }
        public int countZeroBuyprices { get; set; }

        public SupplierItems(List<JtlEntity> items) { 
        
          this.items = items;
          this.warehouse = GetWareHouseItems();
          this.zeroBuypricesItems = GetZeroBuyprices();
          this.totalSupplierItemsAmount = GetTotalAmount();
		  this.totalSupplierQuantity = GetTotalQuantity();
          this.countZeroBuyprices = GetZeroBuypricesCount();
        }

        private int GetZeroBuypricesCount()
        {
            return zeroBuypricesItems.Select(x => x.product).ToList().Count;
        }

        private List<JtlEntity> GetZeroBuyprices()
        {
            return items.Where(x => x.product.supplier == "" && x.product.buyprice == 0.0 && GetNotYetImportedTOFake(x)== true).ToList();
        }

        private bool GetNotYetImportedTOFake(JtlEntity x)
        {
            if (x.plus.Contains("importedToFake"))
            {

                return false;

            }
            else { return true; }
        }

		private int GetTotalQuantity()
		{
			return warehouse.Sum(x => x.totalQuantity);
		}

        //Get supplier items from jtlorders
        public List<WarehouseEntity> GetWareHouseItems()
        {
            List<WarehouseEntity> we = new List<WarehouseEntity>();
            var supplierItems = this.items.GroupBy(y => y.product.supplier)
                .Select(z => new WarehouseSingleItem(z.Key, z.ToList())).ToList();

           we.AddRange(supplierItems);

           return we;
        }


        public double GetTotalAmount()
        {
            return warehouse.Sum(x => x.totalAmount);
        }
    }
}
