﻿using DN_Classes.Entities.Warehouse;
using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DN_Classes.Classes.Warehouse.ItemsStatus
{
    public class OrderStatus
    {
        public List<JTLOrderEntity> orders { get; set; }
        public List<JTLOrderEntity> filteredOrders { get; set; }

        public OrderStatus(List<JTLOrderEntity> orders, String supplierName, string agentName, string ean)
        {

            this.orders = orders;
            filteredOrders = getFilteredOrders(supplierName, agentName, ean);

        }

        public List<JTLOrderEntity> getFilteredOrders(String supplierName, string agentName, string ean)
        {
            List<JTLOrderEntity> filtered = new List<JTLOrderEntity>();

            if (supplierName.ToLower() != "" && supplierName.ToLower() != "select supplier")
            {
                filtered = this.orders.Select(x => x).Where(x => CheckContains(supplierName.ToLower(), CheckPlusExistence(x, "ItemField"), "SupplierOrderId") == true).ToList();
            }

            if (ean != null && ean != String.Empty)
            {

                filtered = filtered.Select(x => x).Where(x => CheckContains(ean, CheckPlusExistence(x, "ItemField"), "Ean") == true).ToList();
            }

            if (agentName.ToLower() != "all" && !String.IsNullOrEmpty(agentName))
            {
                filtered = filtered.Select(x => x).Where(x => CheckPlusExistence(x, "AgentField").AsString.ToLower() == agentName.ToLower()).ToList();
            }



            return filtered;
        }

        private BsonValue CheckPlusExistence(JTLOrderEntity order, string returnField)
        {

            if (order.plus != null && order.plus["orderDetails"] != null)
            {

                if (returnField == "ItemField")
                {
                    return order.plus["orderDetails"]["Items"];
                }

                if (returnField == "AgentField")
                {
                    return order.plus["orderDetails"]["AgentName"];
                }
            }

            return null;
        }

        private bool CheckContains(string compare, BsonValue items, string field)
        {
            int count = 0;

            if (items != null && items.AsBsonArray.ToList().Count() > 0)
            {
                count = items.AsBsonArray.ToList().Where(x => x.AsBsonDocument[field].ToString().ToLower().Contains(compare)).Count();
            }

            return count > 0 ? true : false;
        }
    }
}