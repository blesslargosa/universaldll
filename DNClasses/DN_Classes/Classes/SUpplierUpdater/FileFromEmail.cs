﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DN_Classes.Classes.SupplierUpdater
{
    public class FileFromEmail
    {
        public static string GetEan(string[] splitted)
        {
            if (!string.IsNullOrEmpty(splitted[10]))
                return splitted[10].Trim();

            else if (!string.IsNullOrEmpty(splitted[11]))
                return splitted[11].Trim();

            else if (!string.IsNullOrEmpty(splitted[12]))
                return splitted[12].Trim();

            else
                return "";
        }
    }
}
