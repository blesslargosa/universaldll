﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace DN_Classes.Classes
{
    public static class DN_Regex
    {
        public static string GetAmazonOrderId(string value)
        {
            return Match(value, @"\d{3}-\d{7}-\d{7}"); 
        }

        private static string Match(string value, string pattern)
        {
            var regex = new Regex(pattern);
            return regex.Match(value).Value;
        }


        public static List<string> GetAmazonOrderIds(string text)
        {
            Regex regex = new Regex(@"\d{3}-\d{7}-\d{7}");
            var amazonOrderIds = regex.Matches(text).Cast<Match>().Select(x => x.Value).Distinct().ToList();

            return amazonOrderIds;
        }
    }
}
