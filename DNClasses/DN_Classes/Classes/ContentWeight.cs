﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DN_Classes.Classes
{
    public class ContentWeight
    {
        public string unit { get; set; }
        public double amount { get; set; }
    }
}
