﻿using DN_Classes.DBCollection.AsinMultiplier;
using DN_Classes.Entities.AsinMultiplier;
using MongoDB.Driver.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DN_Classes.QueriesCommands.AsinMultiplier
{
    public class AsinMultiplierQueries : AsinMultiplierDBCollection
    {
        public void UpdateAsinMultipliers(List<AsinMultiplierEntity> asins)
        {
            foreach (AsinMultiplierEntity am in asins)
            {
                AsinMultiplierCollection.Update(Query.EQ("_id", am.asin), Update.Set("multiplier", am.multiplier));

                
            }
        }

    }
}
