﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace DN_Classes.QueriesCommands.StringIdentifier
{
    
   public class StringIdentifier
   {

       public List<string> regexes { get; set; }
      
       public List<string> eans { get; set; }
       public List<string> artnr { get; set; }
       public List<string> all { get; set; }
       public List<string> asins { get; set; }
       public StringIdentifier(string input, List<string> regexes) 
       {
           //regexes.Add(@"\b[A-Z0-9]{7,12}\b");
           this.regexes = regexes;
           
 
        


           this.eans = this.GetAllEans(input);
           this.artnr = this.GetAllArtNr(input);
           this.asins = this.GetAllAsins(input);
           this.all = this.GetAll();

       
       }

       private List<string> GetAllAsins(string input)
       {
           List<string> asins = new List<string>();

           string pattern = @"\b[a-zA-Z\d]{10}\b";
           var matches = Regex.Matches(input, pattern);
           var asinz = matches.Cast<Match>().Select(match => match.Value).ToList();
           
           asins.AddRange(asinz);

           return asins;
       }

       public string RemoveEan(string input) 
       {

            
           string pattern = @"[0-9]{13}";
           var matches = Regex.Matches(input, pattern);
           var eanz = matches.Cast<Match>().Select(match => match.Value).ToList();
           foreach (var ean in eanz)
           {

               input = input.Replace(ean.ToString(), "");

           }

           return input;
       
       }

       public string RemoveAsin(string input) {


           string pattern = @"\b[a-zA-Z\d]{10}\b";
           var matches = Regex.Matches(input, pattern);
           var asins = matches.Cast<Match>().Select(match => match.Value).ToList();
           foreach (var asin in asins)
           {

               input = input.Replace(asin.ToString(), "");

           }

           return input;
       
       
       }
       public string IdentifyStringType(string numbers)
       {
           
           string value = "not found";

           if (numbers.Trim().Length == 13)
           {
               value = "EAN";
           }
           else if (numbers.Trim().Length == 10)
           {
               value = "ASIN";

           }
           else
           {
               
               foreach (var regex in this.regexes)
               {
                   
                   if (Regex.Match(numbers, regex).Success)
                   {
                       value = "ARTNR";
                       break;
                   }
               }
           }
           return value;
       }

       public List<string> GetAllArtNr(string input)
       {
           
           string withoutEan = RemoveEan(input);
           string withoutEanAsin = RemoveEan(withoutEan);

           //string withoutEanAsin2 = withoutEanAsin.Remove('\r');
           //string withoutEanAsin3 = withoutEanAsin2.Remove('\n');
           List<string> articleNos = new List<string>();
           foreach (var regex in this.regexes)
           {
               MatchCollection matchList = Regex.Matches(withoutEanAsin, regex);
               var list = matchList.Cast<Match>().Select(match => match.Value).ToList();
               articleNos.AddRange(list);
           }

           return articleNos;

       }

       public List<string> GetAllEans(string input) {

           List<string> eans = new List<string>();

           string pattern = "[0-9]{13}";
           var matches = Regex.Matches(input, pattern);
           var eanz = matches.Cast<Match>().Select(match => match.Value).ToList();
           foreach (var ean in eanz) {

               input.Replace(ean.ToString(), "");
           
           }
           eans.AddRange(eanz);

           return eans;
       
       }

       public List<string> GetAll() {

           List<string> all = new List<string>();


           all.AddRange(this.eans);
           all.AddRange(this.artnr);
           all.AddRange(this.asins);

           return all;
       
       }
    }
}
