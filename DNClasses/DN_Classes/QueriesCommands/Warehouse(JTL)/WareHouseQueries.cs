﻿using DN_Classes.Entities;
using DN_Classes.Entities.Warehouse;
using DN_Classes.QueriesCommands.SingleCalculatedOrders;
using MongoDB.Driver;
using MongoDB.Driver.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using DN_Classes.QueriesCommands.Warehouse_JTL_;
using System.Globalization;
using DN_Classes.Agent;
using MongoDB.Bson;
using DN_Classes.Entities.SunBursts;
using DN_Classes.Entities.Warehouse.UnshippedItems;
using DN_Classes.Entities.Warehouse.UnshippedItems.JTLUnshippedItems;

namespace DN_Classes.Queries
{
    public class WareHouseQueries : WarehouseDBCollection
    {
        public List<JTLOrderEntity> GetAmazonOrdersByAgentNameForEmail(string agentName) //an email for apology
        {
            List<JTLOrderEntity> jtlOrderEntities = new List<JTLOrderEntity>();

            foreach (JTLOrderEntity jtlOrderEntity in JtlOrdersCollection.FindAll()/*.SetLimit(3000)*/)
            {
                try
                {
                    if (jtlOrderEntity.plus["orderDetails"] != null
                        && jtlOrderEntity.order.dVersandt.Trim() == string.Empty
                        && jtlOrderEntity.plus["orderDetails"]["LatestShipmentDate"] != null
                        && jtlOrderEntity.plus["orderDetails"]["BuyerEmail"] != null
                        && jtlOrderEntity.plus["orderDetails"]["BuyerName"].AsString != string.Empty
                        && jtlOrderEntity.plus["orderDetails"]["AgentName"].AsString == agentName
                        && jtlOrderEntity.plus["orderDetails"]["EmailSent"].AsBoolean != true) //needed
                        jtlOrderEntities.Add(jtlOrderEntity);
                }

                catch
                {                    
                }
            }

            return jtlOrderEntities;
        }

        private bool MatchSKU(string sku)
        {
            Match match = new Regex(@"A\d*-.*").Match(sku);
            return (match.Success && sku.ToUpper().StartsWith("A")) ? true : false;
        }

        public List<JtlEntity> GetSupplierItems(string priceType = "")
        {
            if (priceType != string.Empty)
            {
                return JtlCollection.Find(Query.And(Query.EQ("product.pricetype", priceType), Query.GT("plus.quantity", 0))).Where(x => MatchSKU(x.product.artikelnummer) == false).ToList();
            }
            else
            {
                return JtlCollection.Find(Query.GT("plus.quantity", 0)).Where(x => MatchSKU(x.product.artikelnummer) == false).ToList();
            }
        }

		public List<JtlEntity> GetSupplierAmountItems(string priceType = "")
		{
			if (priceType != string.Empty)
			{
				return JtlCollection.Find(Query.And(Query.EQ("product.pricetype", priceType), Query.GT("plus.quantity", 0))).SetFields("product.artikelnummer", "totalAmount", "plus.quantity", "product.buyprice" ,"product.artikelname").Where(x => MatchSKU(x.product.artikelnummer) == false).ToList();
			}
			else
			{
				return JtlCollection.Find(Query.GT("plus.quantity", 0)).SetFields("product.artikelnummer", "totalAmount", "plus.quantity", "product.buyprice", "product.artikelname").Where(x => MatchSKU(x.product.artikelnummer) == false).ToList();
			}	   
		}

		public List<JtlEntity> GetSupplierAmountItems(string titleFilter, string priceType = "")
		{
			string title = "";

			if (!String.IsNullOrEmpty(titleFilter))
			{
				if (titleFilter != "null")
				{
					title = titleFilter;
				}
			}


			if (priceType != string.Empty)
			{
				return JtlCollection.Find(Query.And(
													Query.EQ("product.pricetype", priceType),
													Query.GT("plus.quantity", 0),
													Query.Matches("product.artikelname", BsonRegularExpression.Create(new Regex(title, RegexOptions.IgnoreCase)))
										)).SetFields("product.artikelnummer", 
													"totalAmount",
													"plus.quantity", 
													"product.buyprice", 
													"product.artikelname")
										.Where(x => MatchSKU(x.product.artikelnummer) == false).ToList();
			}
			else
			{
				return JtlCollection.Find(Query.And(
													Query.GT("plus.quantity", 0),
													Query.Matches("product.artikelname", BsonRegularExpression.Create(new Regex(title, RegexOptions.IgnoreCase)))
										)).SetFields("product.artikelnummer",
													"totalAmount",
													"plus.quantity",
													"product.buyprice",
													"product.artikelname")
										.Where(x => MatchSKU(x.product.artikelnummer) == false).ToList();
			}
		}


        public List<JtlEntity> GetUsedBooks(string priceType = "")
        {
            if (priceType != string.Empty)
            {
                return JtlCollection.Find(Query.And(Query.EQ("product.pricetype", priceType), Query.GT("plus.quantity", 0))).Where(x => MatchSKU(x.product.artikelnummer) == true).ToList();
            }
            else
            {

				return JtlCollection.Find(Query.GT("plus.quantity", 0)).Where(x => MatchSKU(x.product.artikelnummer) == true).ToList();
            }
        }

		public List<JtlEntity> GetUsedBooks(string titleFilter,string priceType = "")
		{
			string title = "";

			if (!String.IsNullOrEmpty(titleFilter))
			{
				if (titleFilter != "null")
				{
					title = titleFilter;
				}
			}

			if (priceType != string.Empty)
			{
				
				return JtlCollection.Find(Query.And(
													Query.EQ("product.pricetype", priceType),
													Query.GT("plus.quantity", 0),
													Query.Matches("product.artikelname",BsonRegularExpression.Create(new Regex(title,RegexOptions.IgnoreCase)))
										)).Where(x => MatchSKU(x.product.artikelnummer) == true).ToList();

			}
			else
			{

				return JtlCollection.Find(Query.And(
													Query.GT("plus.quantity", 0),
													Query.Matches("product.artikelname", BsonRegularExpression.Create(new Regex(title, RegexOptions.IgnoreCase))) )
										).Where(x => MatchSKU(x.product.artikelnummer) == true).ToList();
			}
		}

		public List<JtlEntity> GetSupplierItems(string titleFilter, string priceType = "")
		{
			string title = "";

			if (!String.IsNullOrEmpty(titleFilter))
			{
				if (titleFilter != "null")
				{
					title = titleFilter;
				}
			}

			if (priceType != string.Empty)
			{
				return JtlCollection.Find(Query.And(
													Query.EQ("product.pricetype", priceType),
													Query.GT("plus.quantity", 0),
													Query.Matches("product.artikelname", BsonRegularExpression.Create(new Regex(title, RegexOptions.IgnoreCase)))
										)).Where(x => MatchSKU(x.product.artikelnummer) == false).ToList();
			}
			else
			{
				return JtlCollection.Find(Query.And(
													Query.GT("plus.quantity", 0),
													Query.Matches("product.artikelname", BsonRegularExpression.Create(new Regex(title, RegexOptions.IgnoreCase)))
													)).Where(x => MatchSKU(x.product.artikelnummer) == false).ToList();
			}
		}


		public List<JtlEntity> GetTotalAmountUsedBooks(string priceType = "")
		{

			if (priceType != string.Empty)
			{
				return JtlCollection.Find(Query.And(Query.EQ("product.pricetype", priceType), Query.GT("plus.quantity", 0))).SetFields("product.artikelnummer", "totalAmount", "plus.quantity", "product.buyprice").Where(x => MatchSKU(x.product.artikelnummer) == true).ToList();
			}
			else
			{

				return JtlCollection.Find(Query.GT("plus.quantity", 0)).SetFields("product.artikelnummer", "totalAmount", "plus.quantity", "product.buyprice", "product.artikelname").Where(x => MatchSKU(x.product.artikelnummer) == true).ToList();
			}
		}

		public List<JtlEntity> GetTotalAmountUsedBooks(string titleFilter,string priceType = "")
		{
			string title = "";

			if (!String.IsNullOrEmpty(titleFilter))
			{
				if (titleFilter != "null")
				{
					title = titleFilter;
				}
			}

			if (priceType != string.Empty)
			{
				return JtlCollection.Find(Query.And(
													Query.EQ("product.pricetype", priceType),
													Query.GT("plus.quantity", 0),
													Query.Matches("product.artikelname", BsonRegularExpression.Create(new Regex(title, RegexOptions.IgnoreCase)))
										)).SetFields("product.artikelnummer",
												"totalAmount",
												"plus.quantity",
												"product.buyprice")
										.Where(x => MatchSKU(x.product.artikelnummer) == true).ToList();
			}
			else
			{

				return JtlCollection.Find(Query.And(
													Query.GT("plus.quantity", 0),
													Query.Matches("product.artikelname", BsonRegularExpression.Create(new Regex(title, RegexOptions.IgnoreCase)))
										)).SetFields("product.artikelnummer",
													"totalAmount", 
													"plus.quantity",
													"product.buyprice",
													"product.artikelname")
										.Where(x => MatchSKU(x.product.artikelnummer) == true).ToList();
			}
		}

        public List<JtlEntity> GetWarehouseItemsBySupplier(string supplier)
        {
                return JtlCollection.Find(Query.EQ("product.supplier",supplier)).ToList();
        }

        //public List<JtlEntity> GetSupplierItems()
        //{
        //    return JtlCollection.Find(Query.GT("plus.quantity", 0)).Where(x => MatchSKU(x.product.artikelnummer) == false).ToList();

        //}


        //public List<JtlEntity> GetUsedBooks()
        //{

        //    return JtlCollection.Find(Query.GT("plus.quantity", 0)).Where(x => MatchSKU(x.product.artikelnummer) == true).ToList();
        //}


        public List<JTLOrderEntity> GetItemsUnShippedByAmazonOrderId(Dictionary<string, List<CalculatedProfitEntity>> dic)
        {
            List<JTLOrderEntity> jtlOrderEntities = new List<JTLOrderEntity>();
                       
            int _count = 0;
            foreach (KeyValuePair<string, List<CalculatedProfitEntity>> pair in dic)
            {
                int count = 4011;
                IMongoQuery query = Query.And(Query.EQ("order.cInetBestellNr", pair.Key),
                                              Query.EQ("order.dVersandt", ""));
                jtlOrderEntities.Add(JtlOrdersCollection.FindOne(query)); //304-4604713-4805109
                Console.WriteLine(count+_count++);
            }

            foreach (JTLOrderEntity jtl in jtlOrderEntities)
            {
                if (jtl != null)
                {
                    jtlOrderEntities.Add(jtl);
                }
            }

            return jtlOrderEntities;
        }

        public List<JTLOrderEntity> GetAllItems()
        {
            return JtlOrdersCollection.FindAll().ToList();
        }

        public List<JTLOrderEntity> GetItemsForDetails()
        {
           //return JtlOrdersCollection.FindAll().ToList();
            //return JtlOrdersCollection.Find(Query.EQ("order.cInetBestellNr", "028-8069123-0981949")).ToList();
            return JtlOrdersCollection.Find(
               
                Query.Or(
                Query.NotExists("plus.orderDetails"),
                Query.And(Query.Exists("plus.orderDetails"), Query.NotExists("plus.orderDetails.ReUpdate")),
                Query.And(Query.Exists("plus.orderDetails"),
                            Query.Exists("plus.orderDetails.ReUpdate"),
                            Query.EQ("plus.orderDetails.ReUpdate", true),
                            Query.NE("plus.orderDetails.Items.SupplierName","not available")
                            )
              )).Where( x => x.order.dErstellt >=  DateTime.Now.AddDays(-30)).ToList();
        }

        public List<JTLOrderEntity> GetItemsWithDetails()
        {
            return JtlOrdersCollection.Find(Query.Exists("plus.orderDetails")).ToList();
        }

        public List<JTLOrderEntity> GetItems()
        {
            return JtlOrdersCollection.Find(Query.Exists("plus.UpdatedSingleCalculated")).ToList();
        }

        public List<JTLOrderEntity> GetItemsForSingleCalcUpdate()
        {
            return JtlOrdersCollection.Find(Query.And(Query.NotExists("plus.UpdatedSingleCalculated"), Query.NE("order.cInetBestellNr", ""), Query.Or((Query.NE("order.dVersandt", String.Empty)), (Query.NE("order.dVersandt", ""))))).ToList();
        }

       
        public List<JTLOrderEntity> GetItemsUnShipped()
        {
            return JtlOrdersCollection.Find(Query.EQ("order.dVersandt", "")).ToList();
        }

        public List<JTLOrderEntity> GetItemsShippedWithin30days()
        {
            return JtlOrdersCollection.Find(Query.And(Query.NotExists("plus.UpdatedSingleCalculated"), Query.NE("order.dVersandt", ""), Query.NE("order.cInetBestellNr", ""), Query.GTE("order.dErstellt", DateTime.Now.AddDays(-30)))).ToList();
        }

      
        public List<JTLOrderEntity> GetAllOrdersWithin45Days() {

            return JtlOrdersCollection.Find(Query.And(Query.NE("order.cInetBestellNr", ""), Query.GTE("order.dErstellt", DateTime.Now.AddDays(-45))))

                .SetFields(Fields.Include("order.cInetBestellNr").Include("order.dErstellt"))
                .ToList();
        }

        public List<JTLOrderEntity> GetItemsShipped()
        {
            return JtlOrdersCollection.Find(Query.NE("order.dVersandt", "")).ToList();
        }

        public List<JTLOrderEntity> GetShippedItemsBySupplier(string supplier)
        {
            var items = (supplier.ToLower() != "all") ?
                JtlOrdersCollection.Find(Query.And(
                            Query.NE("order.dVersandt", ""),
                            Query.Matches("plus.orderDetails.SupplierOrderId", supplier.ToUpper())
                            )).ToList() :
                GetItemsShipped();

            return items;
        }

        public List<JTLOrderEntity> GetAllItemsBySupplier(string supplier)
        {
            return JtlOrdersCollection.FindAll().ToList();
        }

        public List<JTLOrderEntity> GetUnShippedItemsBySupplier(string supplier)
        {
            var items = (supplier.ToLower() != "all") ?
                    JtlOrdersCollection.Find(Query.And(
                                Query.EQ("order.dVersandt", ""),
                                Query.Matches("plus.orderDetails.SupplierOrderId", supplier.ToUpper())
                                )).ToList() :
                    GetItemsUnShipped();

            return items;
        }

        public List<JTLOrderEntity> GetItemsBySupplier(string status, string supplier, string agentName)
        {
            IMongoQuery shippedquery = (status.ToLower() == "shipped") ? Query.NE("order.dVersandt", "") : Query.EQ("order.dVersandt", "");
            IMongoQuery agentQuery = Query.EQ("plus.orderDetails.AgentName", agentName);


            if (agentName == "All")
            {

                 return JtlOrdersCollection.Find(Query.And(
                                                        shippedquery,
                                                        Query.Exists("plus.orderDetails"),
                                                        Query.Matches("plus.orderDetails.SupplierOrderId", supplier.ToUpper())
                    )).ToList();

            }


                return JtlOrdersCollection.Find(Query.And(
                                                        shippedquery,
                                                        agentQuery,
                                                        Query.Exists("plus.orderDetails"),
                                                        Query.Matches("plus.orderDetails.SupplierOrderId", supplier.ToUpper())
                    )).ToList();

        }

        public List<AgentEntity> GetAllAgentsInformation()
        {
            return AgentsCollection.FindAll().ToList();
        }

        public List<DateTime> GetItemsDatesShippedWithDetails()
        {
            return JtlOrdersCollection.Find(Query.And(Query.NE("order.dVersandt", ""), Query.Exists("plus.orderDetails"))).SetFields("plus.orderDetails.PurchasedDate").Where(x => modifyDate(x.plus["orderDetails"]["PurchasedDate"].AsDateTime.Date)).Select(x => x.plus["orderDetails"]["PurchasedDate"].AsDateTime.Date).ToList();
            // return JtlOrdersCollection.Find(Query.And(Query.NE("order.dVersandt", ""), Query.Exists("plus.orderDetails"))).ToList();
        }

        public List<DateTime> GetItemDatesUnshippedWithDetails()
        {
            return JtlOrdersCollection.Find(Query.And(Query.EQ("order.dVersandt", ""), Query.Exists("plus.orderDetails"))).SetFields("plus.orderDetails.PurchasedDate").Where(x => modifyDate(x.plus["orderDetails"]["PurchasedDate"].AsDateTime.Date)).Select(x => x.plus["orderDetails"]["PurchasedDate"].AsDateTime.Date).ToList();
        }

        public List<DateTime> GetItemsDatesUnorderedWithDetails()
        {
            return JtlOrdersCollection.Find(Query.And(Query.EQ("order.dVersandt", ""), Query.Exists("plus.orderDetails"), Query.EQ("plus.orderDetails.SupplierOrderId", ""))).SetFields("plus.orderDetails.PurchasedDate").Where(x => modifyDate(x.plus["orderDetails"]["PurchasedDate"].AsDateTime.Date)).Select(x => x.plus["orderDetails"]["PurchasedDate"].AsDateTime.Date).ToList();
        }

        public bool modifyDate(DateTime dt)
        {
            if (dt.Date >= DateTime.Now.Date.AddDays(-30))
            {

                return true;
            }
            return false;
        }

        public List<JTLOrderEntity> GetItemsShippedWithDetails()
        {
            return JtlOrdersCollection.Find(Query.And(Query.NE("order.dVersandt", ""), Query.Exists("plus.orderDetails"))).ToList();
        }

        public List<JTLOrderEntity> GetItemsUnshippedWithDetails()
        {
			return JtlOrdersCollection.Find(Query.And(Query.EQ("order.dVersandt", ""), Query.Exists("plus.orderDetails.PurchasedDate")))
				.SetFields(Fields.Include("order.dVersandt", "plus.orderDetails", "order.cInetBestellNr")).Where(x => !filterEmptySupplierOrderId(x.plus["orderDetails"]["Items"].AsBsonArray)).ToList();
        }

        public List<JTLOrderEntity> GetItemsUnshippedWithDetailsWithoutCancelledOrders()
        {
            return JtlOrdersCollection.Find(Query.And(Query.EQ("order.dVersandt", ""), Query.Exists("plus.orderDetails.PurchasedDate"), Query.EQ("order.nStorno", "0")))
                .SetFields(Fields.Include("order.dVersandt", "plus.orderDetails", "order.cInetBestellNr")).Where(x => !filterEmptySupplierOrderId(x.plus["orderDetails"]["Items"].AsBsonArray)).ToList();
        }

		public List<JTLOrderEntity> GetItemsUnshippedWithDetailsWithoutCancelledOrders(string amazonOrderid="",string ean="")
		{
			IMongoQuery mainmongoQuery = Query.And(Query.EQ("order.dVersandt", ""), Query.Exists("plus.orderDetails.PurchasedDate"), Query.EQ("order.nStorno", "0"));
			IMongoQuery mongoQuery;

			if (!String.IsNullOrEmpty(amazonOrderid) && !String.IsNullOrEmpty(ean))
			{
				mongoQuery = Query.And(Query.Matches("plus.orderDetails.AmazonOrderId", amazonOrderid), Query.Matches("plus.orderDetails.Items.Ean", ean));
				mainmongoQuery = Query.And(mainmongoQuery, mongoQuery);
			}
			else if (String.IsNullOrEmpty(amazonOrderid) && !String.IsNullOrEmpty(ean))
			{
				mongoQuery = Query.Matches("plus.orderDetails.Items.Ean", ean);
				mainmongoQuery = Query.And(mainmongoQuery, mongoQuery);
			}
			else if (!String.IsNullOrEmpty(amazonOrderid) && String.IsNullOrEmpty(ean))
			{
				mongoQuery = Query.Matches("plus.orderDetails.AmazonOrderId", amazonOrderid);
				mainmongoQuery = Query.And(mainmongoQuery, mongoQuery);
			}

			return JtlOrdersCollection.Find(mainmongoQuery)
				.SetFields(Fields.Include("order.dVersandt", "plus.orderDetails", "order.cInetBestellNr")).Where(x => !filterEmptySupplierOrderId(x.plus["orderDetails"]["Items"].AsBsonArray)).ToList();
		}


        //.Where(x => modifyDate(x.plus["orderDetails"]["PurchasedDate"].AsDateTime.Date))
        public List<JTLOrderEntity> GetItemsUnorderedWithDetails()
        {
            return JtlOrdersCollection.Find(Query.And(Query.EQ("order.dVersandt", ""), Query.Exists("plus.orderDetails"))).Where(x => filterEmptySupplierOrderId(x.plus["orderDetails"]["Items"].AsBsonArray)).ToList();
        }

        public List<JTLUnshippedItems> GetJTLUnshippedItems(List<JTLOrderEntity> jtlOrders)
        {
            List<JTLUnshippedItems> jtlUnshippedItems = new List<JTLUnshippedItems>();
            jtlOrders.ForEach(
                x =>{
                    if (checkNoEmptySupplierOrderId(x.plus["orderDetails"]["Items"].AsBsonArray))
                    {
                        GetOnlyJTL(jtlUnshippedItems, x);
                    }
                });

            return jtlUnshippedItems;
        }

        private static void GetOnlyJTL(List<JTLUnshippedItems> jtlUnshippedItems, JTLOrderEntity x)
        {
            foreach (var item in x.plus["orderDetails"]["Items"].AsBsonArray)
            {
                //if (item["SupplierName"].AsString.Trim().ToUpper() == "RAVENSBURGER" || item["SupplierName"].AsString.Trim().ToUpper() == "WUERTH" || item["SupplierName"].AsString.Trim().ToUpper() == "HABA")
                //{
				SingleCalculatedOrdersQueries query = new SingleCalculatedOrdersQueries();
                    try
                    {
                        JTLUnshippedItems jtlUnshippedItemsEntity = new JTLUnshippedItems()
                        {
                            dVersandt = x.order.dVersandt,
                            amazonOrderId = x.plus["orderDetails"]["AmazonOrderId"].AsString,
                            purchasedDate = x.plus["orderDetails"]["PurchasedDate"].AsDateTime,
                            ean = item["Ean"].AsString,
                            supplierOrderId = item["SupplierOrderId"].AsString,
                            supplierName = item["SupplierName"].AsString,
							buyprice = query.GetItemBuyPriceByAmazonOrderId(x.plus["orderDetails"]["AmazonOrderId"].AsString)
                        };

                        jtlUnshippedItems.Add(jtlUnshippedItemsEntity);
                    }

                    catch { }
               // }
            }
        }
        //Dictionary<date, Dictionary<supplier, Dictionary<ean, JTLUnshippedItems>>>>
        public Dictionary<string/*date*/, Dictionary<string/*amazonOId*/, List<JTLUnshippedItems>>> JTLItemsGroupByPurchasedDate(List<JTLUnshippedItems> jtlUnshippedItems)
        {
            return jtlUnshippedItems.GroupBy(v => DateTime.Parse(v.purchasedDate.ToString("yyyy-MM-dd")))
                .ToDictionary(x => x.Key.ToString("yyyy-MM-dd"), y => y.GroupBy(a => a.amazonOrderId)
                    .ToDictionary(b => b.Key, c => c.ToList()));
        }

        public Dictionary<string/*date*/, Dictionary<string/*supplier*/, Dictionary<string/*amazonOId*/, List<JTLUnshippedItems>>>> ItemsGroupByPurchasedDate(List<JTLUnshippedItems> items)
        {
            return items.GroupBy(v => DateTime.Parse(v.purchasedDate.ToString("yyy-MM-dd")))
                .ToDictionary(x => x.Key.ToString("yyyy-MM-dd"),
							  y => y.GroupBy(a => a.supplierName).ToDictionary(z => z.Key, 
																			   j => j.GroupBy(s => s.amazonOrderId).ToDictionary(t => t.Key,
																																m => m.ToList()) 
																			  )
							);
        }


        public List<ItemsGroupByPurchasedDateEntity> GetItemsGroupByPurchasedDate(List<JTLUnshippedItems> items) {


            return items.GroupBy(x => DateTime.Parse(x.purchasedDate.ToString("yyy-MM-dd"))).Select(y => 
                new ItemsGroupByPurchasedDateEntity(
                y.Key,
                y.ToList()
                )
            ).ToList();

        }


        public UnshippedByDate UnshippedItemsGrouping(List<JTLUnshippedItems> items)
        {
            UnshippedByDate byDate = new UnshippedByDate();
            items.GroupBy(v => v.purchasedDate.ToString("yyyy-MM-dd")).ToList();

            //return allItemsBySupplier.GroupBy(v => v.supplierOrderId).ToDictionary(x => x.Key, y => y.ToList());
            return null;
        }

        private bool filterEmptySupplierOrderId(BsonArray orderDetailsItems)
        {
            return orderDetailsItems.ToList().Where(x => x["SupplierOrderId"].AsString == string.Empty).Count() != 0 ? true : false;
        }

        private bool checkNoEmptySupplierOrderId(BsonArray orderDetailsItems)
        {
            return orderDetailsItems.ToList().Where(x => x["SupplierOrderId"].AsString != string.Empty).Count() > 0 ? true : false;
        }

        public List<JTLOrderEntity> testQuery()
        {
            return JtlOrdersCollection.Find(Query.EQ("order.cInetBestellNr", "304-9176030-9954703")).Where(x => filterEmptySupplierOrderId(x.plus["orderDetails"]["Items"].AsBsonArray)).ToList();
        }
    
        public List<JTLOrderEntity> GetItemByAmazonOrderId(string amazonOrderId)
        {
            return JtlOrdersCollection.Find(Query.EQ("order.cInetBestellNr", amazonOrderId)).SetFields(Fields.Include("order.cInetBestellNr").Include("order.nStorno").Include("order.dVersandt").Include("plus")).ToList(); 
        }

        public JTLOrderEntity GetItemByAmazonOrderId_OneItem(string amazonOrderId)
        {
            return JtlOrdersCollection.FindOne(Query.EQ("order.cInetBestellNr", amazonOrderId));
        }

        public bool JtlOrdersPlusHasCustomTimestamp(string amazonOrderId, string customName)
        {
            if (string.IsNullOrEmpty(amazonOrderId))
            {
                return true; 
            }

            var orderRows = JtlOrdersCollection.Find(Query.EQ("order.cInetBestellNr", amazonOrderId)).ToList();
            if (orderRows.Count() > 0)
            {
                foreach (var row in orderRows)
                {
                    if (row.plus.IndexOfName(customName) >= 0)
                    {
                        return true;
                    }
                }
            }
            else
            {
                return true;
            }
            return false;
        }

        /// <summary>
        /// checks if an Order is cancelled
        /// </summary>
        /// <param name="amazonOrderId"></param>
        /// <returns></returns>
        public bool checkIfOrderIsCancelled(string amazonOrderId)
        {
            if (IsStorno(GetItemByAmazonOrderId(amazonOrderId)) == false)
            {
                return IsCancelledNewRule(GetItemByAmazonOrderId(amazonOrderId));

            }
            else {

                return true;
            
            }
        }

        /// <summary>
        /// checks if an order was cancelled with new rule
        /// </summary>
        /// <param name="checkStorno">its a list, because of possible duplicates</param>
        /// <returns></returns>
        private bool IsCancelledNewRule(List<JTLOrderEntity> list)
        {
            foreach (var item in list)
            {
                if (item.plus.Contains("cancelled") && item.plus["cancelled"] == true)
                {
                    return true;
                }
            }

            return false;
        }

        /// <summary>
        /// checks if an order was cancelled
        /// </summary>
        /// <param name="checkStorno">its a list, because of possible duplicates</param>
        /// <returns></returns>
        private bool IsStorno(IList<JTLOrderEntity> checkStorno)
        {
            foreach (var item in checkStorno)
            {
                if (int.Parse(item.order.nStorno) > 0)
                {
                    return true;
                }
            }

            return false;
        }


        /// <summary>
        /// checks if an Order is shipped
        /// </summary>
        /// <param name="amazonOrderId"></param>
        /// <returns></returns>
        public bool checkIfOrderIsShipped(string amazonOrderId)
        {
            return IsShipped(GetItemByAmazonOrderId(amazonOrderId));
        }


        /// <summary>
        /// checks if Order is shipped
        /// </summary>
        /// <param name="list"></param>
        /// <returns></returns>
        private bool IsShipped(List<JTLOrderEntity> list)
        {
            foreach (var item in list)
            {
                if (!string.IsNullOrEmpty(item.order.dVersandt))
                {
                    return true;
                }
            }

            return false;
        }



        public bool CheckOrderIfExisted(string amazonOrderId)
        {
            int count = GetItemByAmazonOrderId(amazonOrderId).Count();

            if (count > 0) return true;
            else return false;
        }


        public List<JTLOrderEntity> GetAllBefore30daysOrders()
        {
            List<JTLOrderEntity> orders = new List<JTLOrderEntity>();

            orders = JtlOrdersCollection.Find(Query.GTE("order.dErstellt", DateTime.Now.AddDays(-30).Date))
                .SetFields("order.cInetBestellNr", "order.dErstellt").ToList();

            return orders;
        }


        public List<JtlEntity> GetAllWarehouseItems(string priceType)
        {
            if (priceType != string.Empty)
            {
                return JtlCollection.Find(Query.And(Query.EQ("product.pricetype", priceType), Query.GT("plus.quantity", 0))).ToList();
            }
            else
            {

                return JtlCollection.Find(Query.GT("plus.quantity", 0)).ToList();
            }
        }

        public List<JtlEntity> GetUsedBooks(List<JtlEntity> jtlAllItems, string priceType)
        {
            if (priceType != string.Empty)
            {
                return jtlAllItems.Where(x => MatchSKU(x.product.artikelnummer) == true && x.product.pricetype == priceType).ToList();
            }
            else
            {

                return jtlAllItems.Where(x => MatchSKU(x.product.artikelnummer) == true).ToList();
            }
        }


        public List<JtlEntity> GetSupplierItems(List<JtlEntity> jtlAllItems, string priceType)
        {
            if (priceType != string.Empty)
            {
                return jtlAllItems.Where(x => MatchSKU(x.product.artikelnummer) == false && x.product.pricetype == priceType).ToList();
            }
            else
            {

                return jtlAllItems.Where(x => MatchSKU(x.product.artikelnummer) == false).ToList();
            }
        }

        
    }
}
