﻿using DN_Classes.Entities.Warehouse;
using DN_Classes.Queries;
using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DN_Classes.QueriesCommands.Warehouse_JTL_
{
   public class WareHouseCommands: WarehouseDBCollection
    {
       public void UpdatePDJTLZeroBuyprices(JtlEntity item) {

           if (item.product.buyprice == 0.0 && item.product.supplier == "")
           {

               JtlCollection.Update(Query.EQ("product.ean", item.product.ean),
                     Update.Set("plus.importedToFake", true),
                     UpdateFlags.Multi);
           }
       }
       public void WriteUnshippedItemsToCollection(List<JTLOrderEntity> unshippedItems)
       {

           JtlOrdersUnshippedCollection.InsertBatch(unshippedItems);
       
       
       }
       public void UpdateUnshippedCollection(string amazonOrderId)
       {

           //JtlOrdersUnshippedCollection.Find(Query.Find(Query.EQ("


       }

       public void UpdateJtlOrdersPlus(string orderId, WareHouseOrdersEntity details)
       {
           //BsonDocument bson = new BsonDocument();
           //JtlOrdersCollection.Update(Query.EQ("order.cInetBestellNr", orderId), Update.Set("plus.orderDetails", bson));


           JtlOrdersCollection.Update(Query.EQ("order.cInetBestellNr", orderId), 
               Update.Set("plus.orderDetails", details.ToBsonDocument()),
               UpdateFlags.Multi);
       
       }

       public void UpdateJtlOrdersPlus(string orderId, WareHouseOrdersEntity_Ebay details)
       {
           //BsonDocument bson = new BsonDocument();
           //JtlOrdersCollection.Update(Query.EQ("order.cInetBestellNr", orderId), Update.Set("plus.orderDetails", bson));
             JtlOrdersCollection.Update(Query.EQ("order.cInetBestellNr", orderId),
              Update.Set("plus.orderDetails", details.ToBsonDocument()),
              UpdateFlags.Multi);
       }

       /// <summary>
       /// 
       /// </summary>
       /// <param name="orderId"></param>
       /// <param name="fieldName"></param>
       public void UpdateJtlOrdersPlusCustomTimestamp(string orderId, string fieldName)
       {
           if (!string.IsNullOrEmpty(orderId))
           {
               JtlOrdersCollection.Update(Query.EQ("order.cInetBestellNr", orderId),
                Update.Set("plus." + fieldName, DateTime.Now),
                UpdateFlags.Multi);
           }
       }
     

       public void UpdateJtlOrdersPlus_Exclude(string orderId, bool exclude) {

          
           JtlOrdersCollection.Update(Query.EQ("order.cInetBestellNr", orderId),
               Update.Set("plus.UpdatedSingleCalculated", exclude).Set("plus.cancelled", true),UpdateFlags.Multi);
      
       
       }

       public void UpdateJtlOrdersPlus_Unset(string orderId)
       {

           JtlOrdersCollection.Update(Query.EQ("order.cInetBestellNr", orderId), Update.Unset("plus.orderDetails"));

       }

       public void UnSetJtlOrdersPlus(string AmazonOrderId)
       {
          

               JtlOrdersCollection.Update(Query.EQ("order.cInetBestellNr", AmazonOrderId), Update.Unset("plus.UpdatedSingleCalculated"));
           
       }
    }
}
