﻿using DN_Classes.DBCollection.SingleCalculatedOrders;
using DN_Classes.Entities;
using DN_Classes.Entities.Warehouse;
using MongoDB.Driver;
using MongoDB.Driver.Builders;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DN_Classes.QueriesCommands.SingleCalculatedOrders
{
    public class SingleCalculatedOrdersQueries : CalculatedDBCollection
    {
        //public WareHouseOrdersEntity GetWareHouseDetailsFromOrders(string amazonOrderId, string supplierName) {

            
        //  var details = SingleCalculatedCollection.FindOne(Query.And(Query.EQ("AmazonOrder.AmazonOrderId",amazonOrderId),Query.EQ("SupplierName",supplierName)));
        //  try
        //  {
        //      WareHouseOrdersEntity whOrdersDetails = new WareHouseOrdersEntity
        //      {

        //          AmazonOrderId = details.AmazonOrder.AmazonOrderId,
        //          PurchasedDate = details.PurchasedDate,
        //          SellerSKU = details.SellerSku,
        //          SupplierOrderId = details.plus["supplierOrderId"].ToString(),
        //          AgentName = details.Agentname
        //      };
        //      return whOrdersDetails;
        //  }
        //  catch { }

        //    return null;
        //}


        //public WareHouseOrdersEntity GetWareHouseDetailsFromOrders(string amazonOrderId)
        //{
        //    string SupplierOderId = "";
        //    var details = SingleCalculatedCollection.FindOne(Query.EQ("AmazonOrder.AmazonOrderId", amazonOrderId));

        //    if(details != null){


        //    DateTime latestShipmentDate = DateTime.MinValue;
        //    try{
        //        latestShipmentDate = COnvertStringToDate(details.AmazonOrder.LatestShipDate);
        //    if (details.SupplierName == "JTL")
        //    {
        //        SupplierOderId = "JTL";
        //    }
        //    else {
        //        try
        //        {
        //            SupplierOderId = details.plus["supplierOrderId"].ToString();
        //        }
        //        catch {

        //            SupplierOderId = "";
        //        }
        //    }
        //    }
        //    catch(Exception m)  {

        //        Console.WriteLine(m);
            
        //    }
        //    try
        //    {
        //        WareHouseOrdersEntity whOrdersDetails = new WareHouseOrdersEntity
        //        {

        //            AmazonOrderId = details.AmazonOrder.AmazonOrderId,
        //            PurchasedDate = details.PurchasedDate,
        //            SellerSKU = details.SellerSku,
        //            SupplierOrderId = SupplierOderId,
        //            LatestShipmentDate = latestShipmentDate,
        //            BuyerEmail = details.AmazonOrder.BuyerEmail,
        //            BuyerName = details.AmazonOrder.BuyerName,
        //            AgentName = details.Agentname,
        //            SupplierName = details.SupplierName

        //        };
        //        return whOrdersDetails;
        //    }
        //    catch(Exception m)  {

        //        Console.WriteLine(m);
            
        //    }
        //    }
        //    return null;
        //}

		public double GetItemBuyPriceByAmazonOrderId(string id)
		{

			double price = SingleCalculatedCollection.Find(Query.EQ("AmazonOrder.AmazonOrderId", id)).SetFields("Item.NewBuyPrice").Select(x => Convert.ToDouble(x.Item.NewBuyPrice)).First();


			return price;
		}
        private DateTime COnvertStringToDate(string shipmentDate)
        {
            DateTime newShipmentDate = DateTime.Now;
            if (shipmentDate.Contains("."))
            {
                newShipmentDate = DateTime.Parse(shipmentDate, new CultureInfo("de-DE"));
            }
            else
            {
                newShipmentDate = DateTime.ParseExact(shipmentDate, "M/d/yyyy h:mm:ss tt", CultureInfo.InvariantCulture);

            }

            return newShipmentDate;
        }

        private List<CalculatedProfitEntity> singleCaclColList = new List<CalculatedProfitEntity>();

        public List<CalculatedProfitEntity> GetOrders(string orderId, string ean, string supplierName) {


            return SingleCalculatedCollection.Find(Query.And(Query.EQ("AmazonOrder.AmazonOrderId", orderId), Query.EQ("SupplierName", supplierName), Query.EQ("Ean", ean))).ToList();
        
        
        }

        public List<CalculatedProfitEntity> GetAllOrderNotYetUpdatedFromJtlOrders() {

            return SingleCalculatedCollection.Find(Query.GTE("PurchasedDate", DateTime.Now.AddDays(-30)))
                .SetFields(Fields.Include("AmazonOrder.AmazonOrderId", "PurchasedDate", "Agentname")).ToList();
        }

        public List<CalculatedProfitEntity> GetOrders(string ean, string supplierName)
        {


            return SingleCalculatedCollection.Find(Query.And( Query.EQ("SupplierName", supplierName), Query.EQ("Ean", ean))).ToList();

        }

    }
}
