﻿using DN_Classes.DBCollection.SingleCalculatedOrders;
using DN_Classes.Entities;
using MongoDB.Driver;
using MongoDB.Driver.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DN_Classes.QueriesCommands.SingleCalculatedOrders
{
    public class SingleCalculatedOrdersCommand : CalculatedDBCollection
    {  
        public void UpdateSingleOrders(string amazonId, string datestring) {

            SingleCalculatedCollection.Update(Query.EQ("AmazonOrder.AmazonOrderId", amazonId), 
                Update.Set("plus.excludeFromOrdering", true).Set("plus.dVersandt", datestring), UpdateFlags.Multi);
        }



        public void UpdateSingleOrdersExclude(string orderId)
        {
            SingleCalculatedCollection.Update(Query.EQ("AmazonOrder.AmazonOrderId", orderId),
               Update.Set("plus.excludeFromOrdering", true).Set("plus.cancelled",true),UpdateFlags.Multi);
        }

        public void UpdateSingleOrdersInclude(string orderId)
        {
            SingleCalculatedCollection.Update(Query.EQ("AmazonOrder.AmazonOrderId", orderId),
               Update.Unset("plus.excludeFromOrdering"), UpdateFlags.Multi);
        }

        
    }
}
