﻿using DN_Classes.DBCollection.Orders;
using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace DN_Classes.QueriesCommands.Orders
{
    public class OrdersQueries : OrdersDBCollection
    {


        public List<BsonDocument> GetAmazonOrders(string jtlVersion)
        {

            var unImported = Query.NotExists("plus.importedToJTL_" + jtlVersion);
            var protectonly = Query.EQ("Order.AgentId", "9");
            // var ordersYesterday = Query.EQ("Order.PurchasedDate", DateTime.Now.AddDays(-10));
            var thisYear = Query.Matches("Order.PurchaseDate",BsonRegularExpression.Create(new Regex(".*2016.*")));
            var notCanceled = Query.NE("Order.OrderStatus", "Canceled");
            var notPending = Query.NE("Order.OrderStatus", "Pending"); 
            var query = Query.And( unImported, protectonly, thisYear, notCanceled, notPending);

            var returnList = AmazonOrdersCollection.Find(query).ToList();
            return returnList;
        }


        public List<BsonDocument> GetEbayOrders(string jtlVersion)
        {
            var unImported = Query.NotExists("plus.importedToJTL_" + jtlVersion);

            //var transSize = Query.Size("order.TransactionArray",2);
            //return EbayOrdersCollection.Find(Query.And(unImported,transSize)).SetLimit(100).ToList();
            return EbayOrdersCollection.Find(unImported).SetLimit(100).ToList();
        }



        public void setImported(string orderId, string jtlVersion)
        {
            if (!string.IsNullOrEmpty(orderId))
            {
                AmazonOrdersCollection.Update(Query.EQ("Order.AmazonOrderId", orderId),
                    Update.Set("plus.importedToJTL_" + jtlVersion, DateTime.Now),
                    UpdateFlags.Multi);
            }
        }


    }
}
