﻿using DN_Classes.DBCollection.SingleCalculatedOrders;
using DN_Classes.Entities;
using MongoDB.Driver.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DN_Classes.QueriesCommands.EbaySingleCalculated
{
    public class EbaySingleCalculatedQueries : CalculatedDBCollection
    {
        public List<CalculatedProfitEntity> GetAllOrderNotYetUpdatedFromJtlOrders()
        {



            return EbayCalculatedCollection.Find(Query.GTE("PurchasedDate", DateTime.Now.AddDays(-30)))
                .SetFields(Fields.Include("EbayOrder.OrderID", "PurchasedDate","Agentname")).ToList();

        }
    }
}
