﻿using DN_Classes.DBCollection.SingleCalculatedOrders;
using MongoDB.Driver;
using MongoDB.Driver.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DN_Classes.QueriesCommands.EbaySingleCalculated
{
    public class EbaySingleCalculatedCommands : CalculatedDBCollection
    {
       public void UpdateSingleOrders(string ebayId, string datestring)
       {
           EbayCalculatedCollection.Update(Query.EQ("EbayOrder.OrderID", ebayId),
               Update.Set("plus.excludeFromOrdering", true).Set("plus.dVersandt", datestring), UpdateFlags.Multi);
       }

       public void UpdateSingleOrdersExclude(string orderId)
       {
           EbayCalculatedCollection.Update(Query.EQ("EbayOrder.OrderID", orderId),
             Update.Set("plus.excludeFromOrdering", true).Set("plus.cancelled", true), UpdateFlags.Multi);
       }

       public void UpdateSingleOrdersInclude(string orderId)
       {
           EbayCalculatedCollection.Update(Query.EQ("EbayOrder.OrderID", orderId),
             Update.Unset("plus.excludeFromOrdering"), UpdateFlags.Multi);
       }
    }
}
