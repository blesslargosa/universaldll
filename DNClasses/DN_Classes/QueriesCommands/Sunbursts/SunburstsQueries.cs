﻿using DN_Classes.Entities.SunBursts;
using DN_Classes.Entities.Warehouse;
using DN_Classes.Queries;
using MongoDB.Bson;
using MongoDB.Driver.Builders;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace DN_Classes.QueriesCommands.Sunbursts
{
    public class SunburstsQueries: WarehouseDBCollection
    {
        //Caching every supplier.
        public List<JTLOrderEntity> GetAllItemsBySupplier(string supplier, int daysOfAge, bool localTesting)
        {        
            int takeOnly = 0;
            List<JTLOrderEntity> ordersFromSupplier = new List<JTLOrderEntity>();

            if (localTesting)
                takeOnly = 3000;            
            else            
                takeOnly = int.MaxValue;
            
            var supplierNameExists = Query.Exists("plus.orderDetails.Items");
            var dVersandtExists = Query.Exists("order.dVersandt");
            var supplierNameQuery = Query.EQ("plus.orderDetails.Items.SupplierName", supplier.ToUpper());

            //WithinTimeRange PurchasedDate - 40 for all suppliers except JTL;
            var withinTimeRange = Query.GT("plus.orderDetails.PurchasedDate", DateTime.Now.AddDays(-daysOfAge));
            //WithinTimeRange PurchasedDate - 28 for jtl
            var jtlWithinTimeRange = Query.GT("plus.orderDetails.PurchasedDate", DateTime.Now.AddDays(-28));

            //Getting items for JTL into chart
            if (supplier.ToLower() == "jtl")
            {
                ordersFromSupplier = JtlOrdersCollection.Find(Query.And(supplierNameQuery, jtlWithinTimeRange, dVersandtExists)).SetFields("plus", "order.dVersandt").ToList();
            }

            //For suppliers except JTL
            else
            {
                ordersFromSupplier = JtlOrdersCollection.Find(Query.And(/*supplierNameExists,*/ supplierNameQuery, withinTimeRange, dVersandtExists)).SetFields("plus", "order.dVersandt").ToList();
                //ordersFromSupplier = ordersFromSupplier.Where(v => AllOrderItemsAreInTimeRange(v.plus["orderDetails"]["Items"].AsBsonArray, daysOfAge, supplier)).ToList();
                //ordersFromSupplier = ordersFromSupplier.Where(v => v.plus["orderDetails"]["Items"].AsBsonArray.Where(x => AreInTimeRange(x.ToString(), daysOfAge, supplier))).ToList();
            }

            return ordersFromSupplier;    
        }

        //Foreach supplierOrderId inside ItemsArray for every documents.
        private bool AllOrderItemsAreInTimeRange(BsonArray items, int daysOfAge, string supplierName)
        {
            bool check = false;

            foreach (var item in items)
            {
                if (IsInTimeRange(item.AsBsonDocument["SupplierOrderId"].AsString, daysOfAge, supplierName) == true)
                    check = true;

                else
                    check = false;
            }

            return check;
        }

        private bool AreInTimeRange(string item, int daysOfAge, string supplierName)
        {
            bool check = false;

            if (IsInTimeRange(item, daysOfAge, supplierName) == true)
                check = true;

            else
                check = false;     

            return check;
        }

        private bool IsInTimeRange(string supplierOrderId, int days, string supplierName)
        {
            Regex regex = new Regex(@"\d{18}");
            Match match = regex.Match(supplierOrderId);
            bool ret = false;

            if (match.Success)
            {
                var supplierOrderIdDateTime = new DateTime(long.Parse(match.Value));
                if (supplierOrderIdDateTime > DateTime.Now.AddDays(-days) && supplierOrderId != string.Empty)
                {
                    ret = true;
                }
                else
                {
                    //ret;
                }
            }

            //RAVENSBURGER_2015-12-18---00-28-52-379
            else if (supplierOrderId.Contains("---") && supplierOrderId != string.Empty)
            {
                try
                {
                    string date = supplierOrderId.Replace("---", "\t").Split('\t')[0].Split('_')[1];
                    string time = supplierOrderId.Replace("---", "\t").Split('\t')[1];
                    string hr = supplierOrderId.Replace("---", "\t").Split('\t')[1].Split('-')[0];
                    string min = supplierOrderId.Replace("---", "\t").Split('\t')[1].Split('-')[1];
                    string sec = supplierOrderId.Replace("---", "\t").Split('\t')[1].Split('-')[2];

                    string month = date.Split('-')[2];
                    string day = date.Split('-')[1];
                    string year = date.Split('-')[0];

                    string full = date.Replace("-", "/") + " " + hr + ":" + min + ":" + sec;
                    string time2 = DateTime.Parse(full).ToString("MMMM dd, yyyy hh:mm");

                    if (DateTime.Parse(time2, new CultureInfo("de-DE")) > DateTime.Now.AddDays(-days))
                    {
                        ret = true;
                    }
                    else
                    {
                        //ret;
                    }
                }

                //In SupplierOrderId no "-" maybe it is JTL
                catch
                {
                    //ret;
                }
            }

            else if (!supplierOrderId.Contains("---") && supplierOrderId != string.Empty)
            {
                //RAVENSBURGER_2015-11-23-23-32-46-574
                try
                {
                    string year = supplierOrderId.Split('_')[1].Split('-')[0];
                    string mon = supplierOrderId.Split('_')[1].Split('-')[1];
                    string day = supplierOrderId.Split('_')[1].Split('-')[2];
                    string hr = supplierOrderId.Split('_')[1].Split('-')[3];
                    string min = supplierOrderId.Split('_')[1].Split('-')[4];
                    string sec = supplierOrderId.Split('_')[1].Split('-')[5];

                    string full = year + "/" + mon + "/" + day + " " + hr + ":" + min;
                    string time2 = DateTime.Parse(full).ToString("MMMM dd, yyyy hh:mm");

                    if (DateTime.Parse(time2, new CultureInfo("de-DE")) > DateTime.Now.AddDays(-days))
                    {
                        ret = true;
                    }

                    else 
                    {
                        //ret;
                    }
                }

                    //In SupplierOrderId no "-" maybe it is JTL
                catch
                {
                    //ret;
                }
            }

            return ret;
        }

        //Each SOId into SOIdEntity.
        public List<SOIdEntity> CollectSOIdsBySupplierName(List<JTLOrderEntity> allItemsBySupplier, string supplierName)
        {
            List<SOIdEntity> groupedSOIdEntities = new List<SOIdEntity>();
            
            foreach (JTLOrderEntity item in allItemsBySupplier)
            {
                foreach (var i in item.plus["orderDetails"]["Items"].AsBsonArray)
                {

                    SOIdEntity groupedSOIdEntity = new SOIdEntity();

                    if (i["SupplierName"].AsString.ToUpper() == supplierName.ToUpper())
                    {
                        try
                        {
                            groupedSOIdEntity.amazonOrderId = item.plus["orderDetails"]["AmazonOrderId"].AsString;
                        }       
                        //No amazonOrderId
                        catch { groupedSOIdEntity.amazonOrderId = string.Empty; }
                        groupedSOIdEntity.dVersandt = item.order.dVersandt;
                        groupedSOIdEntity.purchasedDate = item.plus["orderDetails"]["PurchasedDate"].ToUniversalTime();
                        groupedSOIdEntity.supplierOrderId = i["SupplierOrderId"].AsString;
                        groupedSOIdEntity.ean = i["Ean"].AsString;
                        groupedSOIdEntity.artnr = i["ArtNr"].AsString;

                        groupedSOIdEntities.Add(groupedSOIdEntity);
                    }
                }
            }
            return groupedSOIdEntities;
        }

        public List<SOIdEntity> CollectOldSOIdsBySupplierName(List<JTLOrderEntity> jtlOrders, string supplier)
        {
            WareHouseQueries wq = new WareHouseQueries();
            List<SOIdEntity> items = new List<SOIdEntity>();

            jtlOrders.ForEach(
             x =>
             {
                 foreach (var item in x.plus["orderDetails"]["Items"].AsBsonArray)
                 {
                     try
                     {
                         Regex regex = new Regex(@"\d{18}");
                         Match match = regex.Match(item.AsString);

                         if (match.Success &&
                             item["SupplierName"].AsString.Contains(supplier) &&
                             item["SuppllierOrderId"].AsString != string.Empty &&
                             x.plus["orderDetails"]["AmazonOrderId"].AsString != string.Empty)
                         {
                             IntoSOIdEntity(wq, items, x, item);
                         }
                     }
                         //No SupplierOrderId
                     catch { }
                 }
             });
            return items;
        }

        private static void IntoSOIdEntity(WareHouseQueries wq, List<SOIdEntity> items, JTLOrderEntity x, BsonValue item)
        {
            bool orderIsCancelled = false;

            if (wq.checkIfOrderIsCancelled(x.plus["orderDetails"]["AmazonOrderId"].AsString))
                orderIsCancelled = true;

            SOIdEntity soid = new SOIdEntity()
            {
                amazonOrderId = x.plus["orderDetails"]["AmazonOrderId"].AsString,
                dVersandt = x.order.dVersandt,
                artnr = x.order.cBestellNr,
                ean = item["Ean"].AsString,
                purchasedDate = x.plus["orderDetails"]["PurchasedDate"].AsDateTime,
                supplierOrderId = item["SupplierOrderId"].AsString,
                orderIsCancelled = orderIsCancelled
            };

            items.Add(soid);
        }

        public List<SOIdEntity> CollectNewSOIdsBySupplierName1(List<JTLOrderEntity> jtlOrders, string supplier)
        {
            WareHouseQueries wq = new WareHouseQueries();
            List<SOIdEntity> items = new List<SOIdEntity>();

            jtlOrders.ForEach(
             x =>
             {
                 foreach (var item in x.plus["orderDetails"]["Items"].AsBsonArray)
                 {
                     try
                     {

                         if (item["SupplierOrderId"].AsString.Contains("---") 
                             && item["SupplierName"].AsString.Contains(supplier)
                             && item["SupplierName"].AsString != string.Empty)
                         {
                             IntoSOIdEntity(wq, items, x, item);
                         }
                     }
                     //No SupplierOrderId
                     catch { }
                 }
             });
            return items;
        }

        public List<SOIdEntity> CollectNewSOIdsBySupplierName2(List<JTLOrderEntity> jtlOrders, string supplier)
        {
            WareHouseQueries wq = new WareHouseQueries();
            List<SOIdEntity> items = new List<SOIdEntity>();

            jtlOrders.ForEach(
             x =>
             {
                 foreach (var item in x.plus["orderDetails"]["Items"].AsBsonArray)
                 {
                     try
                     {
                         if (!item["SupplierOrderId"].AsString.Contains("---") 
                             && item["SupplierName"].AsString.Contains(supplier)
                             && item["SupplierOrderId"].AsString != string.Empty)
                         {
                             IntoSOIdEntity(wq, items, x, item);
                         }
                     }
                     //No SupplierOrderId
                     catch { }
                 }
             });
            return items;
        }

        //Grouping SOId for all suppliers except JTL
        public Dictionary<string, List<SOIdEntity>> GroupBySupplierOrderId(List<SOIdEntity> allItemsBySupplier)
        {
            return allItemsBySupplier.GroupBy(v => v.supplierOrderId).ToDictionary(x => x.Key, y => y.ToList());
        }

        //Grouping SOId by week for jtl items
        public Dictionary<string, List<SOIdEntity>> GroupBySupplierOrderIdJTL(List<SOIdEntity> allItemsBySupplier)
        {
            Dictionary<string, List<SOIdEntity>> groupedBy7Days = new Dictionary<string, List<SOIdEntity>>();

             GroupBy7Days(allItemsBySupplier, groupedBy7Days);

            return groupedBy7Days;
        }

        //For JTL grouping.
        private static void GroupBy7Days(List<SOIdEntity> allItemsBySupplier, Dictionary<string, List<SOIdEntity>> groupedBy7Days)
        {
            //Sort Dates.
            allItemsBySupplier = allItemsBySupplier.OrderBy(v => v.purchasedDate.ToUniversalTime()).ToList();

            //Oldest and latest from allItems
            DateTime oldest = allItemsBySupplier.Min(j => j.purchasedDate.ToUniversalTime());
            DateTime latest = allItemsBySupplier.Max(a => a.purchasedDate.ToUniversalTime());

            //Groupings by week here.
            double minDateIncrementor = 0.0;
            double maxdateIncrementor = 168.0;
            for (int s = 0; s < 4; s++)
            {
                List<SOIdEntity> dayRange = allItemsBySupplier.Where(x => x.purchasedDate.ToUniversalTime() >= oldest.AddHours(minDateIncrementor) &&
                                                      x.purchasedDate.ToUniversalTime() <= oldest.AddHours(maxdateIncrementor)).ToList();
                DateTime minDate = dayRange.Min(j => j.purchasedDate.ToUniversalTime());
                DateTime maxDate = dayRange.Max(a => a.purchasedDate.ToUniversalTime());

                groupedBy7Days.Add(minDate.ToString("MM-dd-yyyy") + " to " + maxDate.ToString("MM-dd-yyyy"), dayRange.ToList());

                minDateIncrementor += 168.0;
                maxdateIncrementor += 168.0;
            }

            #region
            //foreach (var a in groupedBy7Days)
            //{
            //    a.Value.ForEach(x =>
            //        {
            //            try
            //            {
            //                if (x.amazonOrderId == "028-1243357-4473944")
            //                {
            //                    string check = x.amazonOrderId;
            //                    SOIdEntity jtl = x;
            //                }
            //            }
            //            catch { }

            //            try
            //            {
            //                if (x.amazonOrderId == "028-8929432-3489140")
            //                {
            //                    string check2 = x.amazonOrderId;
            //                    SOIdEntity jtl2 = x;
            //                }
            //            }

            //            catch { }
            //        });
            //}
            #endregion
        }

    }
}
