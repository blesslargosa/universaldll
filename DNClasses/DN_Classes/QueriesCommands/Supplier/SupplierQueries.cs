﻿using DN_Classes.DBCollection.Supplier;
using DN_Classes.Supplier;
using MongoDB.Driver.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DN_Classes.QueriesCommands.Supplier
{
    public class SupplierQueries : SupplierDBCollection
    {
        public List<SupplierEntity> GetAllSuppliers() {

            return SupplierCollection.FindAll().ToList();
            //return SupplierCollection.FindAll()/*.SetLimit(5)*/.ToList(); 
            //return SupplierCollection.Find(Query.EQ("_id", "JTL")).ToList();

        }

        public SupplierEntity GetSupplier(string supplier) {


            return SupplierCollection.FindOne(Query.EQ("_id", supplier));
        
        }
    }
}
