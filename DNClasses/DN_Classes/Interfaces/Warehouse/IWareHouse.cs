﻿using DN_Classes.Entities.Warehouse;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DN_Classes.Interfaces.Warehouse
{
   public interface IWareHouse
    {

       List<WarehouseEntity> GetWareHouseItems();
       double GetTotalAmount();

    }
}
