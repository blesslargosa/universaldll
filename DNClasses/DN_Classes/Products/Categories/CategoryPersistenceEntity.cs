﻿using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DN_Classes.Products.Categories
{
    [BsonIgnoreExtraElements]
    public class CategoryPersistenceEntity
    {
        [BsonId]
        public string ean { get; set; }
        public List<CategoryEntity> categories { get; set; }
    }
}
