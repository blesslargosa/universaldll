﻿using DN_Classes.Amazon.Orders;
using DN_Classes.Products.Categories;
using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DN_Classes.Products
{
    public class CategoriesDB
    {
        string ConnectionString = @"mongodb://client148:client148devnetworks@136.243.44.111/";
        MongoClient Client = null;
        MongoServer Server = null;
        MongoDatabase Database = null;


        MongoClient ebayClient = null;
        MongoServer ebayServer = null;
        MongoDatabase ebayDatabase = null;


        public MongoCollection<CategoriesEntity> collection = null;
        public MongoCollection<CategoryPersistenceEntity> ebaycollection = null;

        public CategoriesDB()
        {
            string strDatabase = "AmazonAsinCategories";
            Client = new MongoClient(ConnectionString + strDatabase);
            Server = Client.GetServer();
            Database = Server.GetDatabase(strDatabase);
            collection = Database.GetCollection<CategoriesEntity>("categories");

            string strEbayDatabase = "EbayCategories";
            ebayClient = new MongoClient(ConnectionString + strEbayDatabase);
            ebayServer = ebayClient.GetServer();
            ebayDatabase = ebayServer.GetDatabase(strEbayDatabase);
            ebaycollection = ebayDatabase.GetCollection<CategoryPersistenceEntity>("ebaycategories");

        }


		public string getAmazonCategory(string ean)
		{
			string category = "";
			try
			{
				category = collection.FindOne(Query.EQ("_id", ean)).category.ToString(); 
			}
			catch
			{

			}

			return category;
		}
    }
}
