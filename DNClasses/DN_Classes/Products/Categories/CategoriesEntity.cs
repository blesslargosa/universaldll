﻿using DN_Classes.Amazon.Orders;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DN_Classes.Products
{
    [BsonIgnoreExtraElements]
    public class CategoriesEntity
    {
        public string _id { get; set; }
        public string ean { get; set; }
        public string category { get; set; }
        public string type { get; set; }

        public BsonDocument plus { get; set; }
    }
}
 