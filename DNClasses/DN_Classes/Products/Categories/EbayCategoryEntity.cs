﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DN_Classes.Products.Categories
{
    [BsonIgnoreExtraElements]
    public class EbayCategoryEntity
    {
        public ObjectId _id { get; set; }
        public BsonArray eans { get; set; }
        public string category { get; set; }
        public BsonDocument plus { get; set; }
    }
}
