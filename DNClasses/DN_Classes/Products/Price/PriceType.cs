﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DN_Classes.Products.Price
{
    public enum PriceType
    {
        fixedPrice, flexiblePrice
    }
}
