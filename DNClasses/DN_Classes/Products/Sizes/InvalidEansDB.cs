﻿using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DN_Classes.Products
{
    public class InvalidEansDB
    {


        private  List<string> invalidEans = new List<string>();
        private  MongoCollection<BsonDocument> invalidEansCollection;
        private  MongoCollection<BsonDocument> Initialize()
        {
            if (invalidEansCollection == null)
            {
                invalidEansCollection = new MongoClient(@"mongodb://client148:client148devnetworks@136.243.44.111/ProductSize")
                    .GetServer()
                    .GetDatabase("ProductSize")
                    .GetCollection<BsonDocument>("invalideans");
            }

            return invalidEansCollection;
        }

        public InvalidEansDB()
        {
            Initialize();
        }

        public  bool IsValidEan(string ean)
        {
            bool valid = true;

            if (!invalidEans.Any())
            {
                invalidEans = invalidEansCollection.FindAll().SetFields("ean").SetFlags(QueryFlags.NoCursorTimeout).Select(x => x["ean"].AsString).ToList();
            }

            if (invalidEans.Contains(ean))
            {
                valid = false;
            }

            return valid;

        }

        public void AddInvalidEan(string ean, string exceptionMessage)
        {
            invalidEansCollection.Update(Query.EQ("ean", ean), Update.Set("exception-message", exceptionMessage), UpdateFlags.Upsert);
            invalidEans.Add(ean);
        }

    }
}
