﻿using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DN_Classes.Products
{
    [BsonIgnoreExtraElements]
    public class SimpleSize
    {
        public int Length { set; get; }
        public int Width { set; get; }
        public int Height { set; get; }
        public int Weight { set; get; }

        public bool isValid { private set; get; }

        public SimpleSize(List<int> sizes, int weight)
        {
            setSizes(sizes, weight);
        }

        public SimpleSize(int length, int width, int height, int weight)
        {
            List<int> sizes = new List<int>();

            sizes.Add(length);
            sizes.Add(height);
            sizes.Add(width);

            setSizes(sizes, weight);
        }


        public SimpleSize()
        {

        }


        protected void setSizes(List<int> sizes, int weight)
        {
            sizes.Sort();
            sizes.Reverse();

            if (sizes.Count() == 3 && sizes.Where(x => x == 0).Count() == 0 && weight != 0)
            {
                Length = sizes[0];
                Width = sizes[1];
                Height = sizes[2];

                this.Weight = weight;

                isValid = true;
            }
            else
            {
                isValid = false;
            }
        }

        public bool fitsInside(SimpleSize inside)
        {
            if ((inside.Length < Length && inside.Width < Width && inside.Height < Height) && inside.Weight <= Weight && isValid && inside.isValid)
            {
                return true;
            }
            return false;
        }

        /// <summary>
        /// Gets the Volume in cm^3
        /// </summary>
        /// <returns>volume in cm^3</returns>
        public double GetVolume()
        {
            return ((double)Height)/10 * ((double)Length)/10 * ((double)Width) /10; 
        }

    }
}
