﻿using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DN_Classes.Products
{
    [BsonIgnoreExtraElements]
    public class FbaProductSize
    {
        public int lenght {get; set;} 
        public int width {get; set;}
        public int height {get;set;}
        public int weight {get; set;}

        public FbaProductSize(List<int> measurements, int weight)
        {
            measurements = measurements.OrderByDescending(x => x).ToList();

            if (measurements.Count == 3)
            {
                this.lenght = measurements[0];
                this.width = measurements[1];
                this.height = measurements[2];

                this.weight = weight; 
            }
        }
    
        
    }
}
