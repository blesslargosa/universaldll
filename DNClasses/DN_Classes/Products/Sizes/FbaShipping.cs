﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DN_Classes.Products
{
    public static class FbaShipping
    {
        public static double getFbaShippingCosts(FbaProductSize productSize)
        {
            double returnValue = 1.38;

            if (productSize.lenght < 200 && productSize.width < 100 && productSize.height < 10 && productSize.weight < 50)
            {
                return returnValue + 0.25;
            }
            else if (productSize.lenght < 300 && productSize.width < 200 && productSize.height < 20 && productSize.weight < 500)
            {
                return returnValue + 0.40;
            }
            else if (productSize.lenght < 300 && productSize.width < 200 && productSize.height < 50 && productSize.weight < 1000)
            {
                return returnValue + 0.90;
            }
            else if (productSize.lenght < 450 && productSize.width < 340 && productSize.height < 260)
            {
                if (productSize.weight < 500)
                {
                    return returnValue + 1.16;
                }
                else if (productSize.weight < 2000)
                {
                    return returnValue + 1.81;
                }
                else if (productSize.weight < 12000)
                {
                    return returnValue + 3.06;
                }
            }
            return 100;
        }
    }
}
