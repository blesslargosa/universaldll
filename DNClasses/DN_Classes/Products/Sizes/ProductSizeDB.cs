﻿using MongoDB.Driver;
using MongoDB.Driver.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DN_Classes.Products
{
    public class ProductSizeDB
    {
        private MongoCollection<ProductSizeEntity> Collection;
        private InvalidEansDB invalidEansDB=new InvalidEansDB();
        public MongoCollection<ProductSizeEntity> ProductSizeCollection()
        {
            if (Collection == null)
            {
                Collection = new MongoClient(@"mongodb://client148:client148devnetworks@136.243.44.111/ProductSize")
                    .GetServer()
                    .GetDatabase("ProductSize")
                    .GetCollection<ProductSizeEntity>("productsizes");
            }

            return Collection;
        }


        public ProductSizeDB()
        {
            Collection = ProductSizeCollection(); 
        }


        public ProductSizeEntity GetProductSizeByEan(string ean)
        {
            return Collection.FindOne(Query.EQ("ean",ean));
        }

        public List<string> GetAllNoMeasure()
        {
            var nomeasures = Collection.Find(
                Query.Or(
                         Query.NotExists("measure.length"),
                         Query.And(Query.EQ("measure.length", 0), Query.EQ("measure.width", 0), Query.EQ("measure.height", 0), Query.EQ("measure.weight", 0))
                         )).SetFields("ean");

            return nomeasures
                .Select(x => x.ean)
                .Where(x =>
                    invalidEansDB.IsValidEan(x)
                 )
                .Distinct()
                .ToList();
        }
       
    }
}
