﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DN_Classes.Products
{
    [BsonIgnoreExtraElements]
    public class ProductSizeEntity
    {
        public ObjectId id { get; set; }
        public string ean { get; set; }
        public Measure measure { get; set; }

        public class Measure
        {
            public int length { get; set; }
            public int width { get; set; }
            public int height { get; set; }
            public int weight { get; set; }
        }

        public SimpleSize GetSimpleSize()
        {
            return new SimpleSize(new List<int>() { measure.length, measure.width, measure.height }, measure.weight);
        }
    }
}
