﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DN_Classes.DBCollection.Orders;
using DN_Classes.Queries;
using MongoDB.Driver.Builders;
using EnumerableExtensions = Microsoft.Practices.ObjectBuilder2.EnumerableExtensions;

namespace DN_Classes.Sales
{
    public class EansSold : ISold
    {
        public IEnumerable<string> GetSoldItems()
        {
            List<string> soldEans = new List<string>();

            #region amazon
            var soldAmaOrders = WarehouseDBCollection.JtlOrdersCollection.Find(Query.Exists("plus.orderDetails.Items")).SetFields("plus.orderDetails.Items.Ean");
            soldAmaOrders.ToList().ForEach(x =>
            {
                x.plus["orderDetails"]["Items"].AsBsonArray.ToList().ForEach(y =>
                {
                    if (y.AsBsonDocument.Contains("Ean")) { soldEans.Add(y["Ean"].AsString); }
                });
            });
            #endregion

            #region ebay
            OrdersDBCollection ordersDbCollections = new OrdersDBCollection();
            var soldEbayOrders =
                ordersDbCollections.EbayOrdersCollection.Find(Query.Exists("order.TransactionArray.Item.SKU"))
                    .SetFields("order.TransactionArray.Item.SKU");
            soldEbayOrders.ToList().ForEach(x =>
            {
                EnumerableExtensions.ForEach(x["order"]["TransactionArray"].AsBsonArray, y =>
                {
                    if (y.AsBsonDocument.Contains("Item") && y["Item"].AsBsonDocument.Contains("SKU"))
                    {
                        soldEans.Add(y["Item"]["SKU"].AsString);
                    }
                });
            });

            #endregion

            return soldEans;
        }
    }
}
