﻿using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DN_Classes.Agent
{
    [BsonIgnoreExtraElements]
    public class htmlgeneratorproperties
    {
        public string imagelinkformat { get; set; }
        public string logolink { get; set; }
        public string mvcviewlink { get; set; }
        public string imageforwarderlink { get; set; }

    }
}
