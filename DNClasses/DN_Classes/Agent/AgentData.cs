﻿
using DN_Classes.Conditions;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DN_Classes.Agent
{
    [BsonIgnoreExtraElements]
    public class AgentData
    {
        public int agentID { get; set; }
        public string agentname { get; set; }
        public string companyname { get; set; }
        public string address1 { get; set; }
        public string address2 { get; set; }
        public string city { get; set; }
        public string stateOrRegion { get; set; }
        public string postcode { get; set; }
        public string countryCode { get; set; }
        public string phone { get; set; }
        public string email { get; set; }
        public string financialAuthorityString { get; set; }
        public string sendBackAddress { get; set; }
        public string agentLogoFilePath { get; set; }
        public string ebayname { get; set; }
        public string amazonname { get; set; }
        public PlatformKeys keys { get; set; }
        public List<Platform> platforms { get; set; }
        public htmlgeneratorproperties htmlgeneratorproperties {get; set;}
    }
}
