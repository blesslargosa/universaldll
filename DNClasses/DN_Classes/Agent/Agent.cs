﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.Builders;
using MongoDB.Driver.Linq;
using System.IO;

namespace DN_Classes.Agent
{
    public class Agent
    {
        public static string _agentId = string.Empty;
        private static MongoCollection<AgentEntity> AgentCollection;

        public AgentEntity agentEntity = null; 

        /// <summary>
        /// Create an new Instance of the Agent
        /// </summary>
        public Agent()
        {
            var agentId = GetAgentId();
            AgentCollection = new MongoClient("mongodb://client148:client148devnetworks@136.243.44.111/Agents")
                .GetServer()
                .GetDatabase("Agents")
                .GetCollection<AgentEntity>("agents");
            agentEntity = AgentCollection.FindOne(Query.EQ("agent.agentID", int.Parse(agentId))); 

        }

        public Agent(int id)
        {
            AgentCollection = new MongoClient("mongodb://client148:client148devnetworks@136.243.44.111/Agents").GetServer().GetDatabase("Agents").GetCollection<AgentEntity>("agents");
            agentEntity = AgentCollection.FindOne(Query.EQ("agent.agentID", id)); 
        }



        //public static AmazonTokens GetAmazonTokens()
        //{
        //    SetCollection();

        //    AmazonTokens amazontoken = new AmazonTokens();
        //    string agentid = GetAgentId();
        //    var agent = GetAgent(agentid);
        //    if (agent != null)
        //    {
        //        amazontoken.agentID = Convert.ToInt32(agentid);
        //        amazontoken.accesskeyid = agent.agent["keys"]["amazon"]["accesskeyid"].AsString;
        //        amazontoken.secretaccesskeyid = agent.agent["keys"]["amazon"]["secretaccesskeyid"].AsString;
        //        amazontoken.merchantId = agent.agent["keys"]["amazon"]["merchantid"].AsString;
        //        amazontoken.marketplaceId = agent.agent["keys"]["amazon"]["marketplaceid"].AsString;
        //    }
        //    if(agent==null)
        //        throw new AgentException("Agent not found");
        //    if (amazontoken.accesskeyid.Trim().Equals(""))
        //        throw new AgentException("Invalid credentials");
            

        //    return amazontoken;

        //}

        //public static EbayTokens GetEbayTokens()
        //{
        //    SetCollection();

        //    EbayTokens ebaytoken = new EbayTokens();
        //    string agentid = GetAgentId();
        //    var agent = GetAgent(agentid);
        //    if (agent != null)
        //    {
        //        ebaytoken.agentID = Convert.ToInt32(agentid);
        //        ebaytoken.token = agent.agent["keys"]["ebay"]["token"].AsString;
        //        ebaytoken.email = agent.agent["keys"]["ebay"]["email"].AsString;
        //    }
        //    if (agent == null)
        //        throw new AgentException("Agent not found");
        //    if (ebaytoken.token.Trim().Equals(""))
        //        throw new AgentException("Invalid credentials");
        //    return ebaytoken;
        //}

        //public static BooklookerTokens GetBooklookerTokens()
        //{
        //    SetCollection();

        //    BooklookerTokens booklookertoken = new BooklookerTokens();
        //    string agentid = GetAgentId();
        //    var agent = GetAgent(agentid);
        //    if (agent != null)
        //    {
        //        booklookertoken.agentID = Convert.ToInt32(agentid);
        //        booklookertoken.username = agent.agent["keys"]["booklooker"]["username"].AsString;
        //        booklookertoken.password = agent.agent["keys"]["booklooker"]["password"].AsString;
        //    }
        //    if (agent == null)
        //        throw new AgentException("Agent not found");
        //    if (booklookertoken.username.Trim().Equals(""))
        //        throw new AgentException("Invalid credentials");
        //    return booklookertoken;
        //}

        private static AgentEntity GetAgent(string agentid)
        {
            return AgentCollection.FindOne(Query.EQ("agent.agentID", Convert.ToInt32(agentid)));

        }

        private static string GetAgentId()
        {
            if (_agentId == string.Empty)
            {
                string agentid = "";

                string agentdir = "C:\\Agent";
                if (!Directory.Exists(agentdir))
                {
                    Directory.CreateDirectory(agentdir);
                }
                string agentfile = "C:\\Agent\\agentId.txt";


                if (!File.Exists(agentfile))
                {
                    Console.WriteLine("Agent file info not found!\nEnter agent id!");
                    agentid = Console.ReadLine();
                    File.WriteAllText(agentfile, agentid);
                }
                else
                {
                    agentid = File.ReadAllText(agentfile).Trim();
                    if (agentid.Equals(""))
                        throw new Exception("Invalid agent ID");
                }

                return agentid;
            }
            else
            {
                return _agentId;
            }

            
        }
    }
}
