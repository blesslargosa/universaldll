﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace DN_Classes.Agent
{
    [BsonIgnoreExtraElements]
    public class AgentEntity
    {
        public ObjectId id { get; set; }
        public AgentData agent { get; set; }
        public BsonDocument plus { get; set; }

    }
}
