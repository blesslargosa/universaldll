﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DN_Classes.Conditions
{
    public class PlatformConditions
    {
        private double defaultPlatformFeeRate;

        public double DefaultPlatformFeeRate
        {
            get { return defaultPlatformFeeRate; }
            set { defaultPlatformFeeRate = value; }
        }

        private double paymentFeeRate;

        public double PaymentFeeRate
        {
            get { return paymentFeeRate; }
            set { paymentFeeRate = value; }
        }


        private double paymentFeeFix;

        public double PaymentFeeFix
        {
            get { return paymentFeeFix; }
            set { paymentFeeFix = value; }
        }

        private PlatformCategory platformCategory;

        public PlatformCategory PlatformCategory
        {
            get { return platformCategory; }
            set { platformCategory = value; }
        }


        public PlatformConditions(double defaultPlatformFeeRate, double paymentFeeRate, double paymentFeeFix)
        {
            this.defaultPlatformFeeRate = defaultPlatformFeeRate;
            this.paymentFeeRate = paymentFeeRate;
            this.paymentFeeFix = paymentFeeFix;
        }
    }
}
