﻿using DN_Classes.Amazon;
using DN_Classes.Ebay;
using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DN_Classes.Conditions
{
    public class PlatformKeys
    {
        public AmazonKeys amazon { get; set; }
        public EbayKeys ebay { get; set; }

        public BsonDocument booklooker { get; set; }
    }
}
