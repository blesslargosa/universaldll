﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DN_Classes.Conditions
{
    public class ContractConditions
    {
        private double weightFee = 0;

        public double WeightFee
        {
            get { return weightFee; }
            set { weightFee = value; }
        }
        private double discount = 0;

        public double Discount
        {
            get { return discount; }
            set { discount = value; }
        }
        private double bonus = 0;

        public double Bonus
        {
            get { return bonus; }
            set { bonus = value; }
        }



        public ContractConditions(double weightFee, double discount, double bonus)
        {

            this.weightFee = weightFee;
            this.discount = discount;
            this.bonus = bonus;
        }
    }
}
