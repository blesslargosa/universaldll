﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DN_Classes.Supplier;
using DN_Classes.Conditions;
using DN_Classes.Ebay.Category;

namespace DN_Classes.Conditions
{
    public class PlatformConditionByCategory
    {
        public static PlatformConditions platformConditionsDefault = new PlatformConditions(15, 0, 0);

        public static PlatformConditions GetPlatformConditionsFromCategory(string category, string marketPlatform, FeeRule feeRule)
        {

            var platformCondition = (marketPlatform.ToLower() == "amazon") ? getAmazonPlatformCondtion(category) : getEbayPlatformCondtion( feeRule);

            return platformCondition;
        }

        private static PlatformConditions getAmazonPlatformCondtion(string category)
        {
            #region Category List
            if (category == "book_display_on_website")
            {
                return new PlatformConditions(15, 0, 0.56);
            }
            if (category == "music_display_on_website")
            {
                return new PlatformConditions(15, 0, 0.56);
            }
            if (category == "jewelry_display_on_website")
            {
                return new PlatformConditions(20, 0, 0);
            }
            if (category == "dvd_display_on_website")
            {
                return new PlatformConditions(15, 0, 0.56);
            }
            if (category == "toy_display_on_website")
            {
                return new PlatformConditions(15, 0, 0);
            }
            if (category == "musical_instruments_display_on_website")
            {
                return new PlatformConditions(12, 0, 0);
            }
            if (category == "pc_display_on_website")
            {
                return new PlatformConditions(12, 0, 0);
            }
            if (category == "ce_display_on_website")
            {
                return new PlatformConditions(12, 0, 0);
            }
            if (category == "office_product_display_on_website")
            {
                return new PlatformConditions(15, 0, 0);
            }
            if (category == "kitchen_display_on_website")
            {
                return new PlatformConditions(15, 0, 0);
            }

            #endregion

            return platformConditionsDefault;
        }

        private static PlatformConditions getEbayPlatformCondtion(FeeRule feeRule)
        {

            var platforms = new PlatformConditions(feeRule.Percentage, 0.017, 0.35);

            return platforms;
        }




    }
}
