﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DN_Classes.Conditions
{
    public class ConditionSet
    {
        public ProfitConditions Profit { private set; get; }
        public ContractConditions Contract { private set; get; }
        public PlatformConditions Platform { private set; get; }

        public ConditionSet(ProfitConditions profit, ContractConditions contract, PlatformConditions platform)
        {
            Profit = profit;
            Contract = contract;
            Platform = platform;
        }
    }
}
