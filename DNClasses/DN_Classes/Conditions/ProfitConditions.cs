﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DN_Classes.Conditions
{
    public class ProfitConditions
    {
        private double profitWanted;

        public double ProfitWanted
        {
            get { return profitWanted; }
            set { profitWanted = value; }
        }

        private double riskFactor;

        public double RiskFactor
        {
            get { return riskFactor; }
            set { riskFactor = value; }
        }


        private double itemHandlingCosts;

        /// <summary>
        /// price for handling an Item in the Warehouse
        /// </summary>
        public double ItemHandlingCosts
        {
            get { return itemHandlingCosts; }
            set { itemHandlingCosts = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="profitWanted"></param>
        /// <param name="riskFactor">like 2% risk of item lost</param>
        /// <param name="itemHandlingCosts"></param>
        public ProfitConditions(double profitWanted, double riskFactor, double itemHandlingCosts)
        {
            this.profitWanted = profitWanted;
            this.riskFactor = riskFactor;
            this.itemHandlingCosts = itemHandlingCosts;
        }

    }
}
