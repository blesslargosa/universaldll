﻿using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DN_Classes.Conditions
{
    [BsonIgnoreExtraElements]
    public class Platform
    {
        public string name { get; set; }
        public List<Supplier.Supplier> suppliers { get; set; }
    }
}
