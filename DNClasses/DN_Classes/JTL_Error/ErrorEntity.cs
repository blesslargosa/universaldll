﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MongoDB.Bson;

namespace DN_Classes.JTL_Error
{
    public class ErrorEntity
    {
        public ObjectId id { get; set; }
        public string errorLog { get; set; }
        public DateTime date { get; set; }
        public string origin { get; set; }

        public ErrorEntity(string error, string origin)
        {
            this.errorLog = error;
            this.origin = origin;
            this.date = DateTime.UtcNow;
        }

    }
}
