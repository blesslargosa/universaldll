﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MongoDB.Driver;
using MongoDB.Driver.Builders;
using MongoDB.Driver.Linq;

namespace DN_Classes.JTL_Error
{
    public class JTLErrorLog
    {
        public static MongoClient Client;
        public static MongoServer Server;
        public static MongoDatabase PDatabase;
        public static MongoCollection<ErrorEntity> mongoCollection;

        public JTLErrorLog()
        {
            Client = new MongoClient(@"mongodb://client148:client148devnetworks@136.243.44.111/JTLErrors");
            Server = Client.GetServer();
            PDatabase = Server.GetDatabase("JTLErrors");
            mongoCollection = PDatabase.GetCollection<ErrorEntity>("logs");
        }

        public void AddError(string message, string origin)
        {
            mongoCollection.Save(new ErrorEntity(message, origin));
        }

        public List<ErrorEntity> ListErrors()
        {
            return mongoCollection.FindAll().ToList();
        }

        public List<ErrorEntity> ListErrors(DateTime from, DateTime to)
        {
            return mongoCollection.Find(Query.And(Query.GTE("date", from), Query.LTE("date", to))).ToList();
        }
    }
}
