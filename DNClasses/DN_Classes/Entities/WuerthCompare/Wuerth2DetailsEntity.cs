﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DN_Classes.Entities.WuerthCompare
{
    public class Wuerth2DetailsEntity
    {
        public string ean_wur2 { get; set; }
        public string name_wur2 { get; set; }
        public List<string> asins_wur2 { get; set; }
        public double buyprice_wur2 { get; set; }
        public int available_wur2 { get; set; }
        public int itemsold_wur2 { get; set; }
    }
}
