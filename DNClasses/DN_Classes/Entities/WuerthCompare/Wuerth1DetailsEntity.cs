﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DN_Classes.Entities.WuerthCompare
{
    public class Wuerth1DetailsEntity
    {
        public string ean_wur1 { get; set; }
        public string name_wur1 { get; set; }
        public List<string> asins_wur1 { get; set; }
        public double buyprice_wur1 { get; set; }
        public int available_wur1 { get; set; }
        public int itemsold_wur1 { get; set; }
    }
}
