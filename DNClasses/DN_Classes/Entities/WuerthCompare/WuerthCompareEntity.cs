﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DN_Classes.Entities.WuerthCompare
{
    public class WuerthCompareEntity
    {
        public string ean { get; set; }
        public Wuerth1DetailsEntity wuerth1_details { get; set; }
        public Wuerth2DetailsEntity wuerth2_details { get; set; }
    }
}
