﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DN_Classes.Entities.Warehouse
{
   public class WareHouseOrdersDetailsEntity
    {
       public string SupplierName { get; set; }
       public string Ean { get; set; }
       public string SellerSKU { get; set; }
       public string ArtNr { get; set; }
       public string SupplierOrderId { get; set; }

    }
}
