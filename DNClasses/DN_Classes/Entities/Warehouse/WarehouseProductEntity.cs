﻿using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DN_Classes.Entities.Warehouse
{
    [BsonIgnoreExtraElements]
    public class WarehouseProductEntity
    {
        public string artnr { get; set; }

        public string ean { get; set; }

        public double buyprice { get; set; }

        public int quantity { get; set; }

        public double prodtotalamount { get; set; }

		public string title { get; set; }

		public string pricetype { get; set; }
    }
}
