﻿
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace DN_Classes.Entities.Warehouse
{
    [BsonIgnoreExtraElements]
    public class JtlEntity
    {
        public ObjectId id { get; set; }

        public Product product { get; set; }

        public BsonDocument plus { get; set; }
    }
}
