﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DN_Classes.Entities.Warehouse
{
    [BsonIgnoreExtraElements]
    public class WarehouseEntity
    {
        // public BsonArray group { get; set; }
        public string supplier { get; set; }
        public List<WarehouseProductEntity> product { get; set; }
        public double totalAmount { get; set; }
		public int totalQuantity { get; set; }
    }

}
