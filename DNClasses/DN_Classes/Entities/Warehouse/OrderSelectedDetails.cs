﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DN_Classes.Entities.Warehouse
{
    public class OrderSelectedDetails
    {
        public string orderId { get; set; }
        public DateTime date { get; set; }
        public string AgentName { get; set; }
    }
}
