﻿using DN_Classes.Entities.Warehouse;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DN_Classes.Entities.Warehouse
{
    public class SetEntity : WarehouseProductEntity
    {
        public SetEntity(string artnr, string ean, double buyprice, int quantity , string title, string pricetype)
        {
            this.artnr = artnr;
            this.ean = ean;
            this.buyprice = buyprice;
            this.quantity = quantity;
            this.prodtotalamount = Math.Round((buyprice * quantity), 2);
			this.title = title;
			this.pricetype = pricetype;
        }

    }
}
