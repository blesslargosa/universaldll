﻿using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DN_Classes.Entities.Warehouse
{
    [BsonIgnoreExtraElements]
    public class ItemStatusEntity
    {
        public string cInetBestellNr { get; set; }
        public string ExtractedPurchasedDate { get; set; }
        public List<ItemDetails> Details { get; set; }

        public class ItemDetails
        {
            public string Ean { get; set; }
            public string Artnr { get; set; }
            public string SKU { get; set; }
            public string SupplierOrderId { get; set; }
            public string SupplierName { get; set; }
        }
    }
}