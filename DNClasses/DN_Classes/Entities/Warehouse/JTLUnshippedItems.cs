﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DN_Classes.Entities.Warehouse
{
    public class JTLUnshippedItems
    {
        public string dVersandt { get; set; }
        public string amazonOrderId { get; set; }
        public DateTime purchasedDate { get; set; }
        public string ean { get; set; }
        public string supplierOrderId { get; set; }
        public string supplierName { get; set; }
		public double buyprice { get; set; }
    }
}
