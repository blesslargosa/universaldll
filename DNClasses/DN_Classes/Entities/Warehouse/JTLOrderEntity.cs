﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DN_Classes.Entities.Warehouse
{
    [BsonIgnoreExtraElements]
    public class JTLOrderEntity
    {
        public ObjectId id { get; set; }
        public Order order { get; set; }
        public BsonDocument plus { get; set; }
    }

    [BsonIgnoreExtraElements]
    public class Order
    {
        public string kBestellung { get; set; }
        public string tRechnung_kRechnung { get; set; }
        public long tBenutzer_kBenutzer { get; set; }
        public string tAdresse_kAdresse { get; set; }
        public long tText_kText { get; set; }
        public long tKunde_kKunde { get; set; }
        public string cBestellNr { get; set; }
        public string cType { get; set; }
        public string cAnmerkung { get; set; }
        public DateTime dErstellt { get; set; }
        public string nZahlungsziel { get; set; }
        public string tVersandArt_kVersandArt { get; set; }
        public string fVersandBruttoPreis { get; set; }
        public string fRabatt { get; set; }
        public string kInetBestellung { get; set; }
        public string cVersandInfo { get; set; }
        public string dVersandt { get; set; }
        public DateTime shipDateTime { get; set; }
        public string cIdentCode { get; set; }
        public string cBeschreibung { get; set; }
        public string cInet { get; set; }
        public string dLieferdatum { get; set; }
        public string kBestellHinweis { get; set; }
        public string cErloeskonto { get; set; }
        public string cWaehrung { get; set; }
        public string fFaktor { get; set; }
        public string kShop { get; set; }
        public string kFirma { get; set; }
        public string kLogistik { get; set; }
        public string nPlatform { get; set; }
        public string kSprache { get; set; }
        public string fGutschein { get; set; }
        public string dGedruckt { get; set; }
        public string dMailVersandt { get; set; }
        public string cInetBestellNr { get; set; }
        public string kZahlungsArt { get; set; }
        public string kLieferAdresse { get; set; }
        public string kRechnungsAdresse { get; set; }
        public string nIGL { get; set; }
        public string nUStFrei { get; set; }
        public string cStatus { get; set; }
        public string dVersandMail { get; set; }
        public string dZahlungsMail { get; set; }
        public string cUserName { get; set; }
        public string cVerwendungszweck { get; set; }
        public string fSkonto { get; set; }
        public string kColor { get; set; }
        public string nStorno { get; set; }
        public string cModulID { get; set; }
        public string nZahlungsTyp { get; set; }
        public string nHatUpload { get; set; }
        public string fZusatzGewicht { get; set; }
        public string nKomplettAusgeliefert { get; set; }
        public string dBezahlt { get; set; }
        public string kSplitBestellung { get; set; }
    }
}
