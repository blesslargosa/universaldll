﻿using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DN_Classes.Entities.Warehouse
{
    [BsonIgnoreExtraElements]
   public class WareHouseOrdersEntity_Ebay
    {
        public string OrderId { get; set; }
        public DateTime PurchasedDate { get; set; }
        public string AgentName { get; set; }
        public bool ReUpdate { get; set; }
        public string Platform { get; set; }
        public List<WareHouseOrdersDetailsEntity> Items { get; set; }
    }
}
