﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DN_Classes.Entities.Warehouse.UnshippedItems
{
    public class UnshippedByDate
    {
        public string date { get; set; }
        public List<UnshippedBySupplier> supplier {get; set;}
    }
}
