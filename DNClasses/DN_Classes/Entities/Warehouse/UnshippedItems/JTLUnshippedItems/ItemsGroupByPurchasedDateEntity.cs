﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DN_Classes.Entities.Warehouse.UnshippedItems.JTLUnshippedItems
{
    public class ItemsGroupByPurchasedDateEntity
    {
        public string purchasedDate { get; set; }
        public double totalPurchasedatePrice { get; set; }
        public List<Warehouse.JTLUnshippedItems> ItemsGroupdByPurchasedDate { get; set; }
        public List<UnshippedSupplierItemsEntity> supplierItems { get; set; }

        public ItemsGroupByPurchasedDateEntity(DateTime purchasedDate, List<Warehouse.JTLUnshippedItems> ItemsGroupdByPurchasedDate)
        {
            // TODO: Complete member initialization
			this.purchasedDate = purchasedDate.ToString("dd.MM.yyyy");
            this.ItemsGroupdByPurchasedDate = ItemsGroupdByPurchasedDate;

            this.supplierItems = GetSupplierItems();
            this.totalPurchasedatePrice = this.supplierItems.Sum(x => x.totalSupplierPrice);
        }

        private List<UnshippedSupplierItemsEntity> GetSupplierItems()
        {
            return this.ItemsGroupdByPurchasedDate.GroupBy(x => x.supplierName).Select(y => new UnshippedSupplierItemsEntity(y.Key, y.ToList())).ToList(); 
        }



      
    }
}
