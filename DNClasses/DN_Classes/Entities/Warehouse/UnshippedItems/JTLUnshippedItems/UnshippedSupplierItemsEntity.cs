﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DN_Classes.Entities.Warehouse.UnshippedItems.JTLUnshippedItems
{
    public class UnshippedSupplierItemsEntity
    {
       
        private List<Warehouse.JTLUnshippedItems> ItemsGroupdBySupplier;

        public string supplier { get; set; }
        public double totalSupplierPrice { get; set; }
        public List<UnshippedOrderIdItemsEntity> orderidItems { get; set; }
        public UnshippedSupplierItemsEntity(string supplier, List<Warehouse.JTLUnshippedItems> ItemsGroupdBySupplier)
        {
            // TODO: Complete member initialization
            this.supplier = supplier;
            this.ItemsGroupdBySupplier = ItemsGroupdBySupplier;

            this.orderidItems = GetOrderIdItems();
            this.totalSupplierPrice = this.orderidItems.Sum(x => x.totalOrderIdPrice);
        }

        private List<UnshippedOrderIdItemsEntity> GetOrderIdItems()
        {
            return this.ItemsGroupdBySupplier.GroupBy(x => x.amazonOrderId).Select(y => new UnshippedOrderIdItemsEntity(y.Key, y.ToList())).ToList();
        }


      
      

    }
}
