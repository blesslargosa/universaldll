﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DN_Classes.Entities.Warehouse.UnshippedItems.JTLUnshippedItems
{
    public class UnshippedOrderIdItemsEntity
    {

        private List<Warehouse.JTLUnshippedItems> ItemsGroupedByOrderId;

        public string orderId { get; set; }
        public double totalOrderIdPrice { get; set; }
        public int numberofItems { get; set; }
        public List<UnshippedEansEntity> eansBuyPriceItems { get; set; }

        public UnshippedOrderIdItemsEntity(string orderId, List<Warehouse.JTLUnshippedItems> ItemsGroupedByOrderId)
        {
            // TODO: Complete member initialization
            this.orderId = orderId;
            this.ItemsGroupedByOrderId = ItemsGroupedByOrderId;

            this.eansBuyPriceItems = GetEanBuyPriceItems();
            this.numberofItems = this.eansBuyPriceItems.Count();
            this.totalOrderIdPrice = this.eansBuyPriceItems.Sum(x => x.buyprice);
        }

        private List<UnshippedEansEntity> GetEanBuyPriceItems()
        {
            return this.ItemsGroupedByOrderId.Select(x => new UnshippedEansEntity { ean = x.ean, buyprice = x.buyprice }).ToList();
        }
       
       
    
     
    }
}
