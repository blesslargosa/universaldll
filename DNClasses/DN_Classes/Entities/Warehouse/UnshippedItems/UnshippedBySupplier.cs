﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DN_Classes.Entities.Warehouse.UnshippedItems
{
    public class UnshippedBySupplier
    {
        public string supplier { get; set; }
        public List<UnshippedByEan> ean { get; set; }
    }
}
