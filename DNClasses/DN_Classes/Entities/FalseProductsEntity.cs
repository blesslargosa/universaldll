﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DN_Classes.Entities
{
    [BsonIgnoreExtraElements]
    public class FalseProductsEntity
    {
        public ObjectId id { get; set; }
        public string ean { get; set; }
        public string eanSupplier { get; set; }
        public string supplier { get; set; }
        public bool isTerminated { get; set; }
        public BsonDocument plus { get; set; }
    }
}
