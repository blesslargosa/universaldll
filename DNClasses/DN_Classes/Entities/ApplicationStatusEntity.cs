﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DN_Classes.Entities
{
    [BsonIgnoreExtraElements]
    public class ApplicationStatusEntity
    {
        public ObjectId id { get; set; }
        public string AppName { get; set; }
        public DateTime RunningTime {get;set;}
        public string ExecutionTime { get; set; }
        public string UpTimeRange { get; set; }
        public string Status { get; set; }
        public bool Success { get; set; }
        public string RepoLink { get; set; }
        public string DNWikiLink { get; set; }
        public string LogFileLink { get; set; }
        public string ComputerName { get; set; }
        public string ComputerGlobalIP { get; set; }

    }
}