﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DN_Classes.Entities
{
    public enum EntityType
    {
        berk,
        bvw,
        gallay,
        goki,
        hoffmann,
        jpc,
        knv,
        kosmos,
        libri,
        nlg,
        norisspiele,
        ravensburger,
        schmidt,
        soundofspirit,
        sww,
        vtech,
        clementoni,
        boltze,
        primavera,
        bieco,
        fischer,
        gardena
    }
}
