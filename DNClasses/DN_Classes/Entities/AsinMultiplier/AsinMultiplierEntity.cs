﻿using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DN_Classes.Entities.AsinMultiplier
{
    public class AsinMultiplierEntity
    {
        [BsonId]
        public string asin { get; set; }
        public int multiplier { get; set; }
    }
}
