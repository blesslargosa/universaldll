﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DN_Classes.Entities.ProductEditor
{
    public class MultipleProductContainerEntity
    {
        public List<MultipleProductEntity> multipleProductInOne { get; set; }
        public string editOption { get; set; }
        public string value { get; set; }
    }
}
