﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DN_Classes.Entities.ProductEditor
{
    public class EditProductContainerEntity
    {

        public string supplier { get; set; }
        public List<EditProductEntity> allList { get; set; }

        public int value { get; set; }
        public string option { get; set; }
        public List<string> regexes { get; set; }
        public int quantity { get; set; }
    }
}
