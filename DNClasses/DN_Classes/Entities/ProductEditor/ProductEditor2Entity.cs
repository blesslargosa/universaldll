﻿using DN_Classes.Entities.AsinMultiplier;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DN_Classes.Entities.ProductEditor
{
    [BsonIgnoreExtraElements]
    public class ProductEditor2Entity
    {
        public string ean { get; set; }
        public string newEan { get; set; }
        public string artnr { get; set; }
        public List<string> asins { get; set; }
        public int quantity { get; set; }
        public string inputtype { get; set; }
        public string suppliername { get; set; }
        public List<AsinMultiplierEntity> asinmultipliers { get; set; }
        public string input { get; set; }
        public double buypricewithtax { get; set; }
        public double buypriceWithouttax { get; set; }
        public string title { get; set; }
        public double buyprice { get; set; }
        public string availabilityDate { get; set; }
        public BsonDocument plus { get; set; }
    }
}
