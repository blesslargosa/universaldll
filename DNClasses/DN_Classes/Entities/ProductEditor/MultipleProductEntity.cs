﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DN_Classes.Entities.ProductEditor
{
    public class MultipleProductEntity
    {
        public string eanAsinArtNr { get; set; }
        public string type { get; set; }
        public string supplier { get; set; }

    }
}
