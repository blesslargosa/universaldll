﻿using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DN_Classes.Entities.Repricer
{
    public class RepricerEntity
    {
        
            public ObjectId Id { get; set; }
            public string ean { get; set; }
            public string asin { get; set; }
            public double price { get; set; }
            public double priceAmazon { get; set; }
            public double priceFBA { get; set; }
            public DateTime lastRun { get; set; }
            public DateTime lastPriceDate { get; set; }
            public List<string> sellerList { get; set; }
        
    }
}
