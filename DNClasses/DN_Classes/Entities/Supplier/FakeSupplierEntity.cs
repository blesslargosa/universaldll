﻿using DN_Classes.Entities.AsinMultiplier;
using DN_Classes.Entities.Warehouse;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DN_Classes.Entities.Supplier
{
    [BsonIgnoreExtraElements]
    public class FakeSupplierEntity
    {
        
        public ObjectId _id { get; set; }
        public Product product { get; set; }
        public BsonDocument plus { get; set; }

        public class Product
        {
            public string ean { get; set; }
            public string artnr { get; set; }
            public string title { get; set; }
            public double buyprice { get; set; }
            public string supplier { get; set; }
            
            
        } 
    }

    public class EditFakeItemEntity
    {
        public string _id { get; set; }
        public string ean { get; set; }
        public string artnr { get; set; }
        public string title { get; set; }
        public double buyprice { get; set; }
        public string supplier { get; set; }
        //public List<string> asins { get; set; }
        //public List<AsinMultiplierEntity> asinmultipliers { get; set; }
        //public BsonDocument plus { get; set; }
    }

    public class FakeSupplierwithJTLQuantityEntity
    {
        public ObjectId _id { get; set; }
        public string ean { get; set; }
        public string artnr { get; set; }
        public string title { get; set; }
        public double buyprice { get; set; }
        public int jtlqty { get; set; }
        public string supplier { get; set; }
    }
}
