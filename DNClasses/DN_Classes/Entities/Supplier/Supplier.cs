﻿using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DN_Classes.Supplier
{
    [BsonIgnoreExtraElements]
    public class Supplier
    {
        public string name { get; set; }
        public double ProfitWanted { get; set; }
        public double RiskFactor { get; set; }
        public double ItemHandlingCosts { get; set; }
        public bool Paused { get; set; }
        public int DaysToDelivery
        {
            get;
            set; 
        }
        public DateTime DaysToDeliveryTimestamp { get; set; }


        public int GetDaysToDelivery()
        {

                return Math.Min(Math.Max(1, DaysToDelivery), 30);
        }
    }
}
