﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DN_Classes.Entities.Supplier
{
    public class SupplierOrderIdEntity
    {
        public string supplierOrderId { get; set; }
        public DateTime purchsedDate { get; set; }
    }
}
