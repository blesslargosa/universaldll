﻿using DN_Classes.Conditions;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DN_Classes.Entities.Supplier;
using MongoDB.Bson.Serialization;

namespace DN_Classes.Supplier
{
    [BsonIgnoreExtraElements]
    public class SupplierEntity
    {
        [BsonId]
        public string _id {get; set;}
        public bool active {get; set;}
        public ContractConditions contractCondition { get; set; }
        public PlatformConditions platformCondition { get; set; }
        public BsonDocument plus { get; set; }

        public SupplierEntity(string name, double weightFee, double discount, double bonus)
        {
            this._id = name;
            this.contractCondition = new ContractConditions(weightFee, discount, bonus);
        }

        public string GetTransferFile()
        {
            if (plus != null && plus.Contains("transferfile") && plus["transferfile"] != null)
            {
                return plus["transferfile"].AsString;
            }

            return "";
        }

        public string GetJTLTransferFile()
        {
            if (plus != null && plus.Contains("jtltransferfile") && plus["jtltransferfile"] != null)
            {
                return plus["jtltransferfile"].AsString;
            }

            return "";
        }

        public SupplierUpdate getSupplierUpdateInfo()
        {
            return BsonSerializer.Deserialize<SupplierUpdate>(plus["SupplierUpdate"].ToJson());
                //(SupplierUpdateEntity) plus["SupplierUpdate"];
        }
    }
}
