﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DN_Classes.Entities.Supplier
{
	public class SupplierItemAvailabilityEntity
	{
		public string ean { get; set; }
		public string artnr { get; set; }
		public string title { get; set; }
		//public double buyprice { get; set; }
		public int ve { get; set; }
		public int stock { get; set; }
		public bool availability { get; set; }
		public string supplierName { get; set; }
		public int kwnumber { get; set; }
		public int indexId { get; set; }
		public int expectedStock { get; set; }
		public string availabilityDate { get; set; }

	}
}
