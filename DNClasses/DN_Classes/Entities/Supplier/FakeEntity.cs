﻿using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DN_Classes.Entities.Supplier
{
    public class FakeEntity
    {
        public ObjectId _id { get; set; }
        public Product product { get; set; }
        public BsonDocument plus { get; set; }

        public class Product
        {
            public string ean { get; set; }
            public string artnr { get; set; }
            public string title { get; set; }
            public double amazonbuyprice { get; set; }
            public double buyprice { get; set; }
        } 
    }

    public class EditFakeEanEntity
    {
        public ObjectId  _id { get; set; }
        public string ean { get; set; }
        public string artnr { get; set; }
        public double buyprice { get; set; }
        public string title { get; set; }
    }
}
