﻿using DN_Classes.Conditions;
using MongoDB.Driver;
using MongoDB.Driver.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DN_Classes.Supplier
{
    public class SupplierDB
    {
        public MongoClient Client = null;
        public MongoServer Server = null;
        public MongoDatabase PDatabase = null;

        public MongoCollection<SupplierEntity> mongoCollection;

        private List<SupplierEntity> _suppliers;

        public List<SupplierEntity> Suppliers
        {
            get
            {
                if (_suppliers == null)
                {
                    _suppliers = mongoCollection.FindAll().ToList();
                }
                return _suppliers;
            }
            set { _suppliers = value; }
        }


        public SupplierDB()
        {
            Client = new MongoClient(@"mongodb://client148:client148devnetworks@136.243.44.111/Supplier");
            Server = Client.GetServer();
            PDatabase = Server.GetDatabase("Supplier");
            mongoCollection = PDatabase.GetCollection<SupplierEntity>("supplier");
        }

        public ContractConditions GetSpecificContractCondition(string supplierNameString)
        {
            var supplierName = supplierNameString.Replace("Entity","").ToUpper(); 
            return Suppliers.Where(x => x._id.ToUpper().ToString() == supplierName.ToUpper().ToString()).First().contractCondition;
        }

        public SupplierEntity GetSupplier(string id)
        {
            return mongoCollection.FindOne(Query.EQ("_id", id));
        }
    }
}
