﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DN_Classes.Entities.Supplier
{
    public class SupplierUpdateEntity
    {
        public SupplierUpdate SupplierUpdate { get; set; }

    }

    public class SupplierUpdate
    {
        public string value { get; set; }
        public suCredentials credentials { get; set; }
    }

    public class suCredentials
    {
        public string username { get; set; }
        public string password { get; set; }
        public string url { get; set; }
        public string path { get; set; }
    }

}