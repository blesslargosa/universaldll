﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DN_Classes.Entities.WCF
{
    public class EanSupplierEntity
    {

        public string ean { get; set; }
        public int multiplier { get; set; }
        public string supplier { get; set; }
    }
}
