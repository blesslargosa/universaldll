﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DN_Classes.Entities.FBA
{
    [BsonIgnoreExtraElements]
    public class ItemsEntity
    {
        public ObjectId id { get; set; }
        public string ean { get; set; }
        public double price { get; set; }
        public double minAllowedPrice { get; set; }
        public double maxAllowedPrice { get; set; }
        public int qty { get; set; }
        public bool approved { get; set; }
        public string condition { get; set; }
        public bool shipsInternationally { get; set; }
        public string deliveryTime { get; set; }
        public string expiditedShipping { get; set; }
        public string shippingLeadTime { get; set; }
        public string notes { get; set; }
        public BsonDocument plus { get; set; }
    }
}
