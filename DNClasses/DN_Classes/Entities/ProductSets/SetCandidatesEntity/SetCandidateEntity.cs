﻿using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DN_Classes.Entities.ProductSets.SetCandidatesEntity
{
    public class SetCandidateEntity
    {
        public ObjectId _id { get; set; }
        public string amazonOrderId { get; set; }
        public List<Items> items { get; set; }
        public BsonDocument plus { get; set; }
    }

    public class Items
    {
        public string supplier { get; set; }
        public string sku { get; set; }
        public string title { get; set; }
        public string ean { get; set; }
        public string supplierOrderId { get; set; }
        public string artnr { get; set; }
    }
}
