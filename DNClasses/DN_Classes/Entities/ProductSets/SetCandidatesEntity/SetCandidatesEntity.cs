﻿using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DN_Classes.Entities.ProductSets.SetCandidatesEntity
{
    public class SetCandidatesEntity
    {
        public ObjectId _id { get; set; }
        //public string amazonOrderId { get; set; }
        public BsonDocument plus { get; set; }
    }

    //public class Details
    //{
    //    public string amazonOrderId { get; set; }
    //    public string supplierName { get; set; }
    //    public string ean { get; set; }
    //    public string sellerSku { get; set; }
    //    public string artnr { get; set; }
    //    public string supplierOrderId { get; set; }
    //    public BsonArray eanMultiplier { get; set; }
    //}

}
