﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DN_Classes.Entities.ProductSets
{
    public class ProductSetEntity
    {
        public string ean { get; set; }
        public string asin { get; set; }
        public int qty { get; set; }
    }
}
