﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DN_Classes.Entities.SunBursts
{
    public class SOIdEntity
    {
        public string amazonOrderId { get; set; }
        public string dVersandt { get; set; }
        public DateTime purchasedDate { get; set; }
        public string supplierOrderId { get; set; }
        public string ean { get; set; }
        public string artnr { get; set; }
        public bool orderIsCancelled { get; set; }
    }
}
