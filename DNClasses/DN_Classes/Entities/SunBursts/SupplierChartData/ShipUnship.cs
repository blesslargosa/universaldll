﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DN_Classes.Entities.SunBursts.SupplierChartData
{
    public class ShipUnship
    {
        public string name { get; set; }
        public int size { get; set; }
    }
}
