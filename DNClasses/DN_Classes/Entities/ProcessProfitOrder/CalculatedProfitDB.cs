﻿using DN_Classes.Entities.ProcessProfitOrder;
using MongoDB.Driver;
using MongoDB.Driver.Builders;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace DN_Classes.Entities
{
    public class CalculatedProfitDB
    {
        string ConnectionString = @"mongodb://client148:client148devnetworks@136.243.44.111/CalculatedProfit";
        MongoClient Client = null;
        MongoServer Server = null;
        MongoDatabase Database = null;

        MongoClient EbayClient = null;
        MongoServer EbayServer = null; 
        MongoDatabase EbayDatabase = null;

        public MongoCollection<CalculatedProfitEntity> collection = null;
        public MongoCollection<EbayCalculatedEntity> ebaycollection = null;

        public CalculatedProfitDB()
        {
             Client = new MongoClient(ConnectionString);
             Server = Client.GetServer();
             Database = Server.GetDatabase("CalculatedProfit");
            collection = Database.GetCollection<CalculatedProfitEntity>("SingleCalculatedItem");

            EbayClient = new MongoClient(ConnectionString);
            EbayServer = EbayClient.GetServer();
            EbayDatabase = EbayServer.GetDatabase("CalculatedProfit");
            ebaycollection = EbayDatabase.GetCollection<EbayCalculatedEntity>("EbaySingleCalculatedItemV2");

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="agentId"></param>
        /// <param name="daysFromPurchaseDateMin">how many days is the purchase day ago min</param>
        /// <param name="daysFromPurchaseDateMax">how many days is the purchase day ago min</param>
        /// <returns></returns>
        public List<CalculatedProfitEntity> FindUnsentAmazonOrders(string agentId, int daysFromPurchaseDateMin, int daysFromPurchaseDateMax)
        {
            var byAgentId = Query.EQ("AmazonOrder.AgentId", agentId);
            Func<CalculatedProfitEntity, TimeSpan, bool> olderThan = (profitEntity,timeSpan)=> profitEntity.PurchasedDate < DateTime.Now.Subtract(timeSpan);
            Func<CalculatedProfitEntity, TimeSpan, bool> youngerThan = (profitEntity, timeSpan) => profitEntity.PurchasedDate > DateTime.Now.Subtract(timeSpan);

            Func<CalculatedProfitEntity, bool> inTimeFrame = x => olderThan(x, TimeSpan.FromDays(daysFromPurchaseDateMin)) && youngerThan(x, TimeSpan.FromDays(daysFromPurchaseDateMax + daysFromPurchaseDateMin));

            Func<CalculatedProfitEntity, bool> isNotShipped = x => x.plus == null || !x.plus.ToString().Contains("dVersandt");
            Func<CalculatedProfitEntity, bool> emailHasNorBeenSentYet = x => x.plus == null || !x.plus.ToString().Contains("EmailSentDate");

            ////Func<CalculatedProfitEntity, bool> isShipped = x => x.plus != null && x.plus.ToString().Contains("dVersandt"); 

            var unsentAmazonOrders = collection.Find(byAgentId).ToList();

            unsentAmazonOrders = unsentAmazonOrders.Where(inTimeFrame).ToList();


            unsentAmazonOrders = unsentAmazonOrders.Where(isNotShipped).Where(emailHasNorBeenSentYet).ToList()

                /* removes the duplicates*/
                .GroupBy(x => x.AmazonOrder.AmazonOrderId)
                .Select(x=>x.First())

                .ToList();

            return unsentAmazonOrders;

        }


        public CalculatedProfitEntity FindByAmazonOrderId(string amazonOrderId)
        {
            var byAmazonOrderId = Query.EQ("AmazonOrder.AmazonOrderId", amazonOrderId);
            var unsentAmazonOrders = collection.FindOne(byAmazonOrderId);

            Console.WriteLine("Got Order:" + amazonOrderId); 
            return unsentAmazonOrders;
        }


        //public List<CalculatedProfitEntity> FindFastShipped(string agentId, int age)
        //{
        //    var byAgentId = Query.EQ("AmazonOrder.AgentId", agentId);
        //    Func<CalculatedProfitEntity, TimeSpan, bool> olderThan = (profitEntity, timeSpan) => profitEntity.PurchasedDate > DateTime.Now.Subtract(timeSpan);

        //    Func<CalculatedProfitEntity, bool> inTimeFrame = x => olderThan(x, TimeSpan.FromDays(age));

        //    Func<CalculatedProfitEntity, bool> isFastShipped = x => x.plus != null && x.plus.ToString().Contains("dVersandt") && DaysNeeded(x) < 5;
        //    Func<CalculatedProfitEntity, bool> emailHasNotBeenSentYet = x => x.plus == null || !x.plus.ToString().Contains("EmailSentDate");

        //    ////Func<CalculatedProfitEntity, bool> isShipped = x => x.plus != null && x.plus.ToString().Contains("dVersandt"); 

        //    var fastSentAmazonOrders = collection.Find(byAgentId)
        //        .Where(inTimeFrame)
        //        .Where(isFastShipped)
        //        .Where(emailHasNotBeenSentYet)

        //        /* removes the duplicates*/
        //        .GroupBy(x => x.AmazonOrder.AmazonOrderId)
        //        .Select(x => x.First())

        //        .ToList();



        //    return fastSentAmazonOrders;
        //}


        public List<CalculatedProfitEntity> FindFastShipped(string agentId, int age)
        {
            var byAgentId = Query.EQ("AmazonOrder.AgentId", agentId);
            Func<CalculatedProfitEntity, TimeSpan, bool> olderThan = (profitEntity, timeSpan) => profitEntity.PurchasedDate > DateTime.Now.Subtract(timeSpan);

            Func<CalculatedProfitEntity, bool> inTimeFrame = x => olderThan(x, TimeSpan.FromDays(age));

            Func<CalculatedProfitEntity, bool> isFastShipped = x => x.plus != null && x.plus.ToString().Contains("dVersandt") && DaysNeeded(x) < 5;
            Func<CalculatedProfitEntity, bool> emailHasNotBeenSentYet = x => x.plus == null || !x.plus.ToString().Contains("EmailSentDate");

            ////Func<CalculatedProfitEntity, bool> isShipped = x => x.plus != null && x.plus.ToString().Contains("dVersandt"); 

            var notAvailable = collection.Find(byAgentId)
                .Where(inTimeFrame)
                .Where(isFastShipped)
                .Where(emailHasNotBeenSentYet)

                /* removes the duplicates*/
                .GroupBy(x => x.AmazonOrder.AmazonOrderId)
                .Select(x => x.First())

                .ToList();



            return notAvailable;
        }



        private int DaysNeeded(CalculatedProfitEntity entity)
        {
            try
            {
                string versand = entity.plus["dVersandt"].ToString();
                var dateTime = DateTime.ParseExact(versand, "M/d/yyyy h:mm:ss tt", CultureInfo.InvariantCulture);

                var days = (dateTime - entity.PurchasedDate).Days;

                return days;
            }
            catch
            {
                return int.MaxValue;
            }
        }


        public void SetAsSent(string amazonOrderId)
        {
            MongoUpdateOptions mongoUpdateOptions = new MongoUpdateOptions();
            mongoUpdateOptions.Flags = UpdateFlags.Multi;
            
            collection.Update(Query.EQ("AmazonOrder.AmazonOrderId", amazonOrderId), Update.Set("plus.EmailSentDate", DateTime.Now),UpdateFlags.Multi); 
        }
    }
}
