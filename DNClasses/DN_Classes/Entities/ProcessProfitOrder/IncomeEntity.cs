﻿using DN_Classes;
using DN_Classes.Conditions;
using DN_Classes.Products;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace DN_Classes.Entities
{
    [BsonIgnoreExtraElements]
    public class IncomeEntity
    {
        public double ShippingIncome { get; set; }
        public double NetShippingIncome { get; set; }
        public double SellPrice { get; set; }
    }
}
