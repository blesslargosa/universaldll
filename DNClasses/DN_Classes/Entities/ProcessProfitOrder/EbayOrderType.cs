﻿using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DN_Classes.Entities.ProcessProfitOrder
{
    [BsonIgnoreExtraElements]
	public class EbayOrderType
	{
		public string OrderID { get; set; }
		public DateTime CreatedTime { get; set; }
		public string SellerUserID { get; set; }
		public string SellerEmail { get; set; }
		public TotalPrice Total { get; set; }
		public transactionArray[] TransactionArray { get; set; }

        [BsonIgnoreExtraElements]
		public class TotalPrice
		{
			public int CurrencyID { get; set; }
			public double Value { get; set; }
		}
        [BsonIgnoreExtraElements]
		public class transactionArray
		{
			public int QuantityPurchased { get; set; }
			public string OrderLineItemID { get; set; }
			public TransactionPrice TransactionPrice { get; set; }
			public item Item { get; set; }

		}
        [BsonIgnoreExtraElements]
		public class TransactionPrice
		{
			public int currencyID { get; set; }
			public double Value { get; set; }
		}
        [BsonIgnoreExtraElements]
		public class item
		{
			public string ItemID { get; set; }
			public string Title { get; set; }
			public string SKU { get; set; }
		}
	}
}