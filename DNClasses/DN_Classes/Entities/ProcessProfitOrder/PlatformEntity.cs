﻿using DN_Classes.Amazon.Orders;
using DN_Classes.Conditions;
using DN_Classes.Supplier;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DN_Classes.Entities
{
    [BsonIgnoreExtraElements]
    public class PlatformEntity
    {
        public string Category { get; set; }
        public double FeePercentage { get; set; }
        public double FeeinEuro { get; set; }
        public double VariableFixcost { get; set; }
        public double PaymentFeePercentage { get; set; }
        public double PaymentFeeEuro { get; set; }

    }
}
