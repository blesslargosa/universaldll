﻿using DN_Classes;
using DN_Classes.Amazon.Orders;
using DN_Classes.Products;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DN_Classes.Entities
{
    [BsonIgnoreExtraElements]
    public class TaxEntity
    {
        public double TaxRate { get; set; }
        public double ItemTax { get; set; }
        public double ShippingIncomeTax { get; set; }
    }

}
