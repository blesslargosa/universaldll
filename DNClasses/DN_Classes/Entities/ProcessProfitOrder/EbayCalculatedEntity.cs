﻿using eBay.Service.Core.Soap;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DN_Classes.Entities.ProcessProfitOrder
{
    [BsonIgnoreExtraElements]
    public class EbayCalculatedEntity
    {
        public ObjectId _id { get; set; }
        public string Agentname { get; set; }
        public string SellerSku { get; set; }
        public string SupplierName { get; set; }
        public string Ean { get; set; }
        public string ArticleNumber { get; set; }
        public int VE { get; set; }
        // public DateTime ProcessedDate { get; set; }
        public DateTime PurchasedDate { get; set; }
        public BsonDocument plus { get; set; }

        public PlatformEntity Platform { get; set; }
        public IncomeEntity Income { get; set; }
        public ItemEntity Item { get; set; }
        public TaxEntity Tax { get; set; }
        public ShippingEntity Shipping { get; set; }
        public PackingEntity Packing { get; set; }
        //public OrderType EbayOrder { get; set; }
		public EbayOrderType EbayOrder { get; set; }
    }
}
