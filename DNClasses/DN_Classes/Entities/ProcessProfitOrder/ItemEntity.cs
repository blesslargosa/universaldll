﻿using DN_Classes;
using DN_Classes.Conditions;
using DN_Classes.Products;
using MongoDB.Bson.Serialization.Attributes;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace DN_Classes.Entities
{
    [BsonIgnoreExtraElements]
    public class ItemEntity
    {
        public double NewBuyPrice { get; set; }
        public double DiscountedBuyprice { get; set; }
        public int Stock { get; set; }
        public double RiskCost { get; set; }
        public double Profit { get; set; }
        public double profitOverwrite { get; set; }
        public SimpleSize SimpleSize { get; set; }

      
    }
}
