﻿
using DN_Classes.Products;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DN_Classes.Entities
{
    [BsonIgnoreExtraElements]
    public class PackingEntity
    {
        public int lenght;
        public string name;
        public double netPrice;
        public double price;
        public int weight;
        public int width;

    }
}
