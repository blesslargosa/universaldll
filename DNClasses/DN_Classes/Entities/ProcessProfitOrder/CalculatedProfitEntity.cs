﻿using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MongoDB.Bson.Serialization.Attributes;
using System.Text.RegularExpressions;
using System.Globalization;
using DN_Classes.Amazon.Orders;
using DN_Classes.Conditions;
using DN_Classes.Products;
using eBay.Service.Core.Soap;
using DN_Classes.Entities.ProcessProfitOrder;



namespace DN_Classes.Entities
{
    [BsonIgnoreExtraElements]
    public class CalculatedProfitEntity
    {
        public ObjectId _id { get; set; }
        public string Agentname { get; set; }
        public string SellerSku { get; set; }
        public string SupplierName { get; set; }
        public string Ean { get; set; }
        public string ArticleNumber { get; set; }
        public int VE { get; set; }
       // public DateTime ProcessedDate { get; set; }
        public DateTime PurchasedDate { get; set; }
        public BsonDocument plus { get; set; }
        public BsonDocument plus2 { get; set; }

        public PlatformEntity Platform { get; set; }
        public IncomeEntity Income { get; set; }
        public ItemEntity Item { get; set; }
        public TaxEntity Tax { get; set; }
        public ShippingEntity Shipping { get; set; }
        public PackingEntity Packing { get; set; }
        public AmazonOrder AmazonOrder { get; set; }
        public EbayOrderType EbayOrder { get; set; }
        //public OrderType EbayOrder2 { get; set; }
        
    }
}
