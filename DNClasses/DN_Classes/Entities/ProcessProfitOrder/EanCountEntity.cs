﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DN_Classes.Entities.ProcessProfitOrder
{
    public class EanCountEntity
    {
        public string ean { get; set; }
        public string title { get; set; }
        public string supplier { get; set; }
    }

    public class EanCountSupplierEntity {

        public string ean { get; set; }
        public string title { get; set; }
        public int count { get; set; }
        public List<SupplierCountEntity> suppliers { get; set; }
    
    }

    public class SupplierCountEntity {

        public string suppliername { get; set; }
        public int count { get; set; }
        public int inStock { get; set; }
    
    }
}
