﻿using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DN_Classes.Entities
{
    [BsonIgnoreExtraElements]
    public class ShippingEntity
    {
        public string ShippingName { get; set; }
        public double ShippingCosts { get; set; }
        public double PackingCosts { get; set; }
        public double HandlingCosts { get; set; }
   }
}
