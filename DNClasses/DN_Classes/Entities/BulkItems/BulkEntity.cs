﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DN_Classes.Entities.BulkItems
{
    [BsonIgnoreExtraElements]
    public class BulkEntity
    {
        public ObjectId id { get; set; }
        public int bulkNumber { get; set; }
        public DateTime createdDate { get; set; }
        public double amountPaid { get; set; }
        public DateTime startDate { get; set; }
        public string name { get; set; }
    }
}
