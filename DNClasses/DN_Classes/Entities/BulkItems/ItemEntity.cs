﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DN_Classes.Entities.BulkItems
{
    [BsonIgnoreExtraElements]
    public class ItemEntity
    {
        public ObjectId id { get; set; }
        public string ean { get; set; }
        public int bulkNumber { get; set; }
        public double buyprice { get; set; }
        public int quantity { get; set; }
        public DateTime soldDate { get; set; }
        public string title { get; set; }
        public BsonDocument plus { get; set; }
        public string suppliername { get; set; }
    }
}
