﻿using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DN_Classes.Entities.Seller
{
    public class SellerEntity
    {
        public ObjectId id { get; set; }
        public Details details { get; set; }
        public BsonDocument plus { get; set; }
    }

    public class Details
    {
        public string sellerID { get; set; }
        public string amazonLink { get; set; }
        public string businessName { get; set; }
        public string typeOfBusiness { get; set; }
        public string tradeRegisterNumber { get; set; }
        public string ustID { get; set; }
        public string companyRepresentatives { get; set; }
        public string phone { get; set; }
        public string customerServiceAddress { get; set; }
        public string businessAddress { get; set; }
        public BsonArray email { get; set; }
        /// <summary>
        /// within last 30 days
        /// </summary>
        public FeedBack feedbackHistory { get; set; }
    }

    public class FeedBack
    {
        /// <summary>
        /// 0 - 100% and rating stars 5&4
        /// </summary>
        public float positive { get; set; }
        /// <summary>
        ///  0 - 100% and rating stars 3
        /// </summary>
        public float neutral { get; set; }
        /// <summary>
        /// 0 - 100% and rating stars 1&2
        /// </summary>
        public float negative { get; set; } 
        /// <summary>
        /// number => 40
        /// </summary>
        public int number { get; set; }
        /// <summary>
        /// within last 10 days if the feed back history satisfies positive >= 95% and number > 40
        /// </summary>
        public string negativeRatings { get; set; }
    }

}
