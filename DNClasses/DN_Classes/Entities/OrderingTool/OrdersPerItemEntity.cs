﻿using DN_Classes.Entities;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DN_Classes.Entities.OrderingTool
{
    [BsonIgnoreExtraElements]
   public  class OrdersPerItemEntity
    {
       [BsonId]
        public string id { get; set; }
        public List<CalculatedProfitEntity> singleorders { get; set; }
        public int wareHouseStock { get; set; }
        public int packingUnitToOrder { get; set; }
        
        
    }
}
