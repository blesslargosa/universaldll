﻿using DN_Classes.Products;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DN_Classes.Entities.OrderingTool
{
    [BsonIgnoreExtraElements]
    public class JtlEntity
    {
        public ObjectId id { get; set; }
        public Product product { get; set; }
        public BsonDocument plus { get; set; }
    }
}