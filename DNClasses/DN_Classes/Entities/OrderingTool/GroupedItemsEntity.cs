﻿
using DN_Classes.Entities;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DN_Classes.Entities.OrderingTool
{
    [BsonIgnoreExtraElements]
    public class GroupedItemsEntity
    {
        public string ean { get; set; }
        public string supplier { get; set; }
        public int countorders { get; set; }
        public int itemsPerPack { get; set; }
        public double buyprice { get; set; }
        public double totalVePrice { get; set; }
        public List<CalculatedProfitEntity> singleorders { get; set; }

    }
}