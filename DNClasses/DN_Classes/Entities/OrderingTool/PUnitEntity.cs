﻿using DN_Classes.Amazon.Orders;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;

using System.Linq;
using System.Web;

namespace DN_Classes.Entities.OrderingTool
{
    [BsonIgnoreExtraElements]
    public class PUnitEntity
    {

        
        public string suppliername { get; set; }
        public string ean { get; set; }
        public string artnr { get; set; }
        public int itemsPerVe { get; set; }
        public int countOrders { get; set; }
        public int itemsInStock { get; set; }
        public int itemsToOrder { get; set; }
        public int totalVeToOrder { get; set; }

        public bool ordered { get; set; }
      
        public DateTime orderedDate { get; set; }

        public List<AmazonOrder> amazonOrderID { get; set; }
    }

   
    
}