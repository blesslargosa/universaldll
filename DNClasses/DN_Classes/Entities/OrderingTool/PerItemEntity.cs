﻿using DN_Classes.Entities;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DN_Classes.Entities.OrderingTool
{
   public class PerItemEntity
    {
       [BsonId]
        public DateTime id { get; set; }
       public CalculatedProfitEntity singleorder { get; set; }
    }
}
