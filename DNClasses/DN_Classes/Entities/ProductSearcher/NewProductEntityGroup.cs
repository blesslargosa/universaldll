﻿using System.Collections.Generic;

namespace DN_Classes.Entities.ProductSearcher {
	public class NewProductEntityGroup {
		public string monthyear { get; set; }
		public List<string> asins { get; set; }
		public List<string> eans { get; set; }
		public List<string> titles { get; set; }
	}
}
