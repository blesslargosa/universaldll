﻿using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DN_Classes.Entities.ProductSearcher
{
    public class NewProductINCEntity
    {

        public ObjectId id { get; set; }
        public Product product { get; set; }
        public BsonDocument plus { get; set; }


       
    }

    public class Product
    {
        public string artnr { get; set; }
        public string ean { get; set; }
        public double price { get; set; }
        public int vpe { get; set; }
        public string title { get; set; }
    }
}
