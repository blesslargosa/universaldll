﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;

namespace DN_Classes.Entities.ProductSearcher {
	[BsonIgnoreExtraElements]
	public class NewProductEntity {
		[BsonId]
		public string asin { get; set; }
		public DateTime datefound { get; set; }
		public string ean { get; set; }
		public string title { get; set; }
	}
}
