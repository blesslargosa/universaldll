﻿using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace DN_Classes.Amazon.Categories
{
    public class CategoryDB
    {
        private static MongoClient Client;
        private static MongoServer Server;
        private static MongoDatabase PDatabase;

        private static MongoCollection<CategoryEntity> mongoCollection;

        public CategoryDB()
        {
            Client = new MongoClient(@"mongodb://client148:client148devnetworks@136.243.44.111/AmazonAsinCategories");
            Server = Client.GetServer();
            PDatabase = Server.GetDatabase("AmazonAsinCategories");
            mongoCollection = PDatabase.GetCollection<CategoryEntity>("categories");
        }

        public CategoryDB(string server)
        {
            Client = new MongoClient(@"mongodb://client148:client148devnetworks@" + server + "/AmazonAsinCategories");
            Server = Client.GetServer();
            PDatabase = Server.GetDatabase("AmazonAsinCategories");
            mongoCollection = PDatabase.GetCollection<CategoryEntity>("categories");
        }

        public void SaveEntity(List<string> asins, string category, string productType, string ean)
        {
            foreach (string asin in asins)
            {
                if (mongoCollection.Find(Query.EQ("asin", asin)).Count() == 0 && asin!="")
                {
                    mongoCollection.Save(new CategoryEntity { asin = asin, ean = ean, category = category, type = productType, plus = null });
                }
            }
        }

        public List<CategoryEntity> FindAllEntities()
        {
            return mongoCollection.FindAll().ToList();
        }

        public CategoryEntity FindOne(string asin)
        {
            return mongoCollection.FindOne(Query.EQ("_id", asin));
        }

        public bool isAlreadyListed(string asinean)
        {
            return asinean.Length == 13
                ? mongoCollection.Find(Query.EQ("ean", asinean)).Count() > 0
                : mongoCollection.Find(Query.EQ("_id", asinean)).Count() > 0;
        }


        public bool HasRank(string ean)
        {
            CategoryEntity entity = mongoCollection.FindOne(Query.And(
                                                                        Query.EQ("_id", ean),
                                                                        Query.Exists("plus.rank")));
            if (entity == null)
            {
                return false;
            }
            else
            {
                return true;
            }

        }

        public List<string> GetBlankCategories()
        {
            return mongoCollection.Find(Query.EQ("category", "")).ToList().Where(x => GetEanType(x.ean)).ToList().Select(x => x.ean).ToList();
        }

        public bool GetEanType(string ean)
        {
            return Regex.Match(ean, @"\d{13}").Success;
        }
        public void SaveEntity(CategoryEntity newEntity, string rank)
        {
            CategoryEntity entity = mongoCollection.FindOne(Query.EQ("_id", newEntity.ean));
            if (entity != null)
            {
                entity.category = newEntity.category;

                BsonDocument plus = entity.plus;
                try
                {
                    plus["rank"] = rank;
                }
                catch
                {
                    plus = new BsonDocument();
                    plus.Add("rank", rank);
                }
                entity.plus = plus;
                mongoCollection.Save(entity);

            }
            else
            {
                BsonDocument plus = new BsonDocument();
                plus.Add("rank", rank);
                newEntity.plus = plus;
                mongoCollection.Save(newEntity);

            }
        }

    }
}
