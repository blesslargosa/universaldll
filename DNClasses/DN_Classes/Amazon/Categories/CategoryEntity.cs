﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DN_Classes.Amazon.Categories
{
    [BsonIgnoreExtraElements]
    public class CategoryEntity
    {
        [BsonId]
        public string asin { get; set; }
        public string ean { get; set; }
        public string category { get; set; }
        public string type { get; set; }
        public BsonDocument plus { get; set; }

    }
}
