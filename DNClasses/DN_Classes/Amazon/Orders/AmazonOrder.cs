﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DN_Classes.Amazon.Orders
{
    [BsonIgnoreExtraElements]
    public class AmazonOrder : ICloneable
    {
        public string AmazonOrderId { get; set; }
        public string PurchaseDate { get; set; }
        public string LastUpdateDate { get; set; }
        public string OrderStatus { get; set; }
        public string FulfillmentChannel { get; set; }
        public string SalesChannel { get; set; }
        public string ShipServiceLevel { get; set; }
        public ShippingAddress ShippingAddress { get; set; }
        public OrderTotal OrderTotal { get; set; }
        public string NumberOfItemsShipped { get; set; }
        public string NumberOfItemsUnshipped { get; set; }
        public string PaymentMethod { get; set; }
        public string MarketplaceId { get; set; }
        public string BuyerEmail { get; set; }
        public string BuyerName { get; set; }
        public string ShipmentServiceLevelCategory { get; set; }
        public string ShippedByAmazonTFM { get; set; }
        public string OrderType { get; set; }
        public string EarliestShipDate { get; set; }
        public string LatestShipDate { get; set; }
        public BsonValue ASIN { get; set; }
        public BsonValue SellerSKU { get; set; }
        public BsonValue OrderItemId { get; set; }
        public BsonValue Title { get; set; }
        public BsonValue QuantityOrdered { get; set; }
        public BsonValue QuantityShipped { get; set; }
        public BsonValue ItemPrice { get; set; }
        public BsonValue ShippingPrice { get; set; }
        public BsonValue GiftWrapPrice { get; set; }
        public BsonValue ItemTax { get; set; }
        public BsonValue ShippingTax { get; set; }
        public BsonValue GiftWrapTax { get; set; }
        public BsonValue ShippingDiscount { get; set; }
        public BsonValue PromotionDiscount { get; set; }
        public BsonValue ConditionNote { get; set; }
        public BsonValue ConditionId { get; set; }
        public BsonValue ConditionSubtypeId { get; set; }
        public string AgentId { get; set; }

        public double GetItemPrice()
        {
            return ItemPrice.ToDouble();
        }

        public object Clone()
        {
            return this.MemberwiseClone();
        }
    }
}
