﻿using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DN_Classes.Amazon.Orders
{
    [BsonIgnoreExtraElements]
    public class GiftWrapPrice
    {
        public string CurrencyCode { get; set; }
        public string Amount { get; set; }
    }
}
