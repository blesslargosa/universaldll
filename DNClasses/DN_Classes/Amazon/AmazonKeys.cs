﻿using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DN_Classes.Amazon
{
    [BsonIgnoreExtraElements]
    public class AmazonKeys
    {
        public string accesskeyid { get; set; }
        public string secretaccesskeyid { get; set; }
        public string merchantid { get; set; }
        public string marketplaceid { get; set; }
    }
}
