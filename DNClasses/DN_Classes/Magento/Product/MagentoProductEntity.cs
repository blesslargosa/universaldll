﻿using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DN_Classes.Magento.Product
{
    public class MagentoProductEntity
    {
        public ObjectId id { get; set; }
        public MagentoProduct product { get; set; }
        public BsonDocument plus { get; set; }

        public string GetStringCategoriesBySku(string sku)
        {
            string value = "";
            foreach (var entity in this.product.categories)
            {
                value += entity.id;
                value += ",";
            }
            try
            {
                return value.Substring(0, value.Length - 1);
            }
            catch { return ""; }
        }
    }

    public class MagentoProduct {
        public string ean { get; set; }
        public string sku { get; set; }
        public List<string> shops { get; set; }
        public List<ShopCategory> categories { get; set; }
    }

  
}
