﻿using MongoDB.Driver;
using MongoDB.Driver.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DN_Classes.Magento.Product
{
    public class MagentoProductDB
    {
        private MongoCollection<MagentoProductEntity> magentoCollection;
        public MagentoProductDB()
        {
            magentoCollection = new MongoClient(@"mongodb://client148:client148devnetworks@136.243.44.111/MagentoCollection")
                            .GetServer().GetDatabase("MagentoCollection")
                            .GetCollection<MagentoProductEntity>("magentoProducts");

        }

        public MagentoProductEntity GetEntityBySku(string sku)
        {
            return magentoCollection.FindOne(Query.EQ("product.sku", sku));
        }
        public List<MagentoProductEntity> GetAll()
        {
            return magentoCollection.FindAll().ToList();
        }

        public List<ShopCategory> GetCategoriesBySku(string sku)
        {
            return magentoCollection.FindOne(Query.EQ("product.sku", sku)).product.categories;
        }

     
        public void UpdateProductEntity(MagentoProductEntity newEntity) {
            MagentoProductEntity oldEntity = this.GetEntityBySku(newEntity.product.sku);
            if (oldEntity == null)
                magentoCollection.Save(newEntity);
            else {
                oldEntity.product.ean  = newEntity.product.ean;
                oldEntity.product.sku = newEntity.product.sku;
                oldEntity.product.shops  = newEntity.product.shops;
                oldEntity.product.categories = newEntity.product.categories;
                magentoCollection.Save(oldEntity);
            }
        }

    }
}
