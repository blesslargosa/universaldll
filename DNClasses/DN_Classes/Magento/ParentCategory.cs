﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DN_Classes.Magento
{
    public class ParentCategory:ShopCategory
    {
        public List<SubCategory> subcategory { get; set; }
        
        public ParentCategory(int id, string name) : base(id, name) { 
        }
        

    }
}
