﻿using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DN_Classes.Magento
{
    public class MagentoShop
    {
        public ObjectId id { get; set; }
        public string shopName { get; set; }
        public string url { get; set; }
        public List<ShopCategory> categories { get; set; }

    }
}
