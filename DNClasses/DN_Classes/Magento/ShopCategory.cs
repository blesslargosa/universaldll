﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DN_Classes.Magento
{
    public class ShopCategory
    {
        public int id { get; set; }
        public string name { get; set; }

        public ShopCategory(int id, string name) {
            this.id = id;
            this.name = name;
        }

    }
}
