﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DN_Classes.Magento
{
    public class SubCategory : ShopCategory
    {
         public int id { get; set; }
         public ParentCategory parent { get; set; }
         public string name { get; set; }

         public SubCategory(int id, string name)
             : base(id, name)
         {
         }
    }
}
