﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DN_Classes.Magento.Interfaces
{
    public interface IMagento
    {
        MagentoShop GetShop(string shopName);
        void AddShop(MagentoShop newShop);
        List<ShopCategory> GatCategories(string shop, List<string> catNames);
    }
}
