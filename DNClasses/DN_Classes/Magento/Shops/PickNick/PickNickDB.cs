﻿using MongoDB.Driver;
using MongoDB.Driver.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DN_Classes.Magento.Shops.PickNick
{
    public class PickNickDB
    {

        private MongoCollection<MagentoShop > magentoCollection = new MongoClient(@"mongodb://client148:client148devnetworks@148.251.0.235/MagentoCollection")
                                                                         .GetServer().GetDatabase("magentocollection")
                                                                         .GetCollection<MagentoShop >("magentocollection");

        public PickNickDB() { }

        public MagentoShop GetShop(string shopName) { 
            return magentoCollection.FindOne(Query.EQ("shopName", shopName));
        }

        public void AddShop(MagentoShop newShop) {
            if (this.GetShop(newShop.shopName) == null)
            {
                magentoCollection.Save(newShop);
            }
        }
    }
}
