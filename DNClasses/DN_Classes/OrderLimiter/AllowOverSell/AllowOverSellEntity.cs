﻿using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DN_Classes.OrderLimiter.AllowOverSell
{
    public class AllowOverSellEntity
    {
        public ObjectId id { get; set; }
        public string sku { get; set; }
        public string ean { get; set; }
        public string supplier { get; set; }
    }
}
