﻿using MongoDB.Driver;
using MongoDB.Driver.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DN_Classes.OrderLimiter.AllowOverSell
{
    public class AllowOverSellDB
    {
        MongoCollection<AllowOverSellEntity> collection;

        public AllowOverSellDB()
        {
             collection = new MongoClient(@"mongodb://client148:client148devnetworks@136.243.44.111/SalesLimiter")
                                              .GetServer().GetDatabase("SalesLimiter") 
                                              .GetCollection<AllowOverSellEntity>("AllowedOversell");
        }

        public List<AllowOverSellEntity> GetAllAllowedBySupplier(string supplierName) {
            return collection.Find(Query.EQ("supplier", supplierName.ToUpper())).ToList();
        }

        public List<AllowOverSellEntity> GetAllAllowed()
        {
            return collection.FindAll().ToList();
        }
        
        public bool EanIsAllowedToOverSell(string ean) {
            return collection.FindOne(Query.EQ("ean", ean))==null? false:true;
        }

        public bool SkuIsAllowedToOverSell(string sku)
        {
            return collection.FindOne(Query.EQ("sku", sku)) == null ? false : true;
        }
    }
}
