﻿using MongoDB.Driver;
using MongoDB.Driver.Builders;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DN_Classes.OrderLimiter.SupplierOrderLimiter
{
    public class SalesLimiterRulesDB
    {

       private MongoCollection<SalesLimiterRulesEntity> collection;

        public SalesLimiterRulesDB()
        {
            collection = new MongoClient(@"mongodb://client148:client148devnetworks@136.243.44.111/SalesLimiter")
                                             .GetServer().GetDatabase("SalesLimiter")
                                             .GetCollection<SalesLimiterRulesEntity>("SalesLimiterRules");
        }

        public List<SalesLimiterRulesEntity> GetSalesLimiterBySupplier(string supplierName)
        {
            //query by UPPERCASE
            var salesLimiterRules = collection.Find(Query.EQ("supplier", supplierName.ToUpper()));
            //query by lowercase
            if (salesLimiterRules == null || salesLimiterRules.Count() == 0)
            {
                salesLimiterRules = collection.Find(Query.EQ("supplier", supplierName.ToLower()));
            }
            //query by Camelcase
            else if (salesLimiterRules == null || salesLimiterRules.Count() == 0)
            {
                salesLimiterRules = collection.Find(Query.EQ("supplier", CultureInfo.CurrentCulture.TextInfo.ToTitleCase(supplierName.Trim())));
            }

            return salesLimiterRules.ToList();
        }

    }
}
