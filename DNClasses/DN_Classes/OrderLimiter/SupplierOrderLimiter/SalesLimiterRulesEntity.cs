﻿using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DN_Classes.OrderLimiter.SupplierOrderLimiter
{
public class SalesLimiterRulesEntity
    {
        public ObjectId id { get; set; }
        public string supplier { get; set; }
        public int days { get; set; }
        public int maxItem { get; set; }
    }
}
