﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DN_Classes.DBCollection.SingleCalculatedOrders;
using DN_Classes.Entities;
using MongoDB.Driver.Builders;
using MongoDB.Bson;
using MongoDB.Driver;
using DN_Classes.OrderLimiter.AllowOverSell;
using DN_Classes.OrderLimiter.SupplierOrderLimiter;
using System.Text.RegularExpressions;
namespace DN_Classes.OrderLimiter
{
    public class SalesLimiter : ISalesLimiter
    {
        private List<SupplierOrderGroup> singleOrders = new List<SupplierOrderGroup>();
        private SalesLimiterRulesDB salesLimiterDB = new SalesLimiterRulesDB();
        private List<SalesLimiterRulesEntity> salesLimiterRulesEntities;
        private AllowOverSellDB allowedOverSellDB = new AllowOverSellDB();

        private void ProcessSalesLimit(string SupplierName)
        {
            salesLimiterRulesEntities = salesLimiterDB.GetSalesLimiterBySupplier(SupplierName);
            foreach (var salesLimiterRulesEntity in salesLimiterRulesEntities)
            {
                int date = -30;
                try { date = salesLimiterRulesEntity.days * -1; }
                catch { }

                var match = new BsonDocument {
                                { 
                                        "$match", new BsonDocument 
                                        {
                                                {
                                                    "PurchasedDate", 
                                                        new BsonDocument {
                                                                {
                                                                    "$gte", DateTime.UtcNow.AddDays(date)
                                                                }
                                                         }
                                                },
                                                {
                                                    "$or" , new BsonArray{
                                                        new BsonDocument
                                                        {   
                                                            {
                                                                "plus.dVersandt", new BsonDocument{ 
                                                                    { "$exists", false}
                                                                }
                                                            }
                                                        },
                                                        new BsonDocument
                                                        {   
                                                            {
                                                                "plus.dVersandt", ""
                                                            }
                                                        }
                                                    }
                                                },
                                                {"SupplierName",SupplierName}

                                        }
                                }
               };
                var group = new BsonDocument { 
                    { "$group", 
                        new BsonDocument { 
                               new BsonDocument{ 
                                                 {"_id","$SellerSku"} 
                               }, 
                               { 
                                    "count", new BsonDocument{ 
                                        { "$sum", 1} 
                                     } 
                               }
                            } 
                  } 
                };
                AggregateArgs aggArgs = new AggregateArgs();
                var pipeline = new[] { match, group };
                aggArgs.Pipeline = pipeline;
                var results = CalculatedDBCollection.SingleCalculatedCollection.Aggregate(aggArgs);
                foreach (var result in results)
                {
                    SupplierOrderGroup newGroup = new SupplierOrderGroup
                    {
                        sku = result["_id"].ToString(),
                        count = result["count"].AsInt32,
                        ean = new Regex(@"\d{13}").Match(result["_id"].ToString()).Value
                    };
                    singleOrders.Add(newGroup);
                }
            }
        }

        public List<string> BlockedList(string SupplierName)
        {
            ProcessSalesLimit(SupplierName);

            List<string> temp = new List<string>();
            foreach (var singleOrder in singleOrders)
            {
                foreach (var salesLimiterRulesEntity in salesLimiterRulesEntities)
                {
                    if (salesLimiterRulesEntity != null)
                    {
                     //   Console.WriteLine(salesLimiterRulesEntity.supplier + "\t" + salesLimiterRulesEntity.maxItem);
                        if (!allowedOverSellDB.SkuIsAllowedToOverSell(singleOrder.sku) && singleOrder.count >= salesLimiterRulesEntity.maxItem)
                        {
                            temp.Add(singleOrder.ean);
                        }
                    }
                }

            }
            return temp.Distinct().ToList();
        }

        public Dictionary<string, string> BlockedListWithSKU(string SupplierName)
        {
            ProcessSalesLimit(SupplierName);

            Dictionary<string, string> blockedList = new Dictionary<string, string>();

            foreach (var singleOrder in singleOrders)
            {
                foreach (var salesLimiterRulesEntity in salesLimiterRulesEntities)
                {
                    if (salesLimiterRulesEntity != null)
                    {
                    //    Console.WriteLine(salesLimiterRulesEntity.id);
                        if (!allowedOverSellDB.SkuIsAllowedToOverSell(singleOrder.sku) && singleOrder.count >= salesLimiterRulesEntity.maxItem)
                        {
                            try
                            {
                                blockedList.Add(singleOrder.sku, singleOrder.ean);
                            }
                            catch { }
                            //temp.Add(singleOrder.ean);
                        }
                    }
                }

            }
            return blockedList;
        }


    }
}


