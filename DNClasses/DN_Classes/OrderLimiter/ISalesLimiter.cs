﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DN_Classes.OrderLimiter
{
    public interface ISalesLimiter
    {
        List<string> BlockedList(string SupplierName);

        Dictionary<string, string> BlockedListWithSKU(string SupplierName);
    }
}
