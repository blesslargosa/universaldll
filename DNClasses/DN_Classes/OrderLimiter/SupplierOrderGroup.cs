﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DN_Classes.OrderLimiter
{
    public class SupplierOrderGroup
    {
        public string ean { get; set; }
        public int count { get; set; }
        public string sku { get; set; }
    }
}
