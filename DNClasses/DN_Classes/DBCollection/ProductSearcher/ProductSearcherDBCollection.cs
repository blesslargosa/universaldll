﻿using MongoDB.Driver;
using DN_Classes.Entities.ProductSearcher;

namespace DN_Classes.DBCollection.ProductSearcher {
	public static class ProductSearcherDBCollection {
		private static string ConnectionString = "mongodb://steffennordquist:adminsteffen148devnetworks@136.243.44.111:27017/";
		private static string AuthSource = "?authSource=admin";


		private static MongoCollection<T> GetCollection<T>(string dbName, string collectionName) {
			return new MongoClient(@"mongodb://steffennordquist:adminsteffen148devnetworks@136.243.44.111:27017/" + dbName + "?authSource=admin").GetServer()
				.GetDatabase(dbName).GetCollection<T>(collectionName);
		}

        public static MongoCollection<NewProductINCEntity> IncompleteDetails = GetCollection<NewProductINCEntity>("NewProducts", "IncompleteDetails2");

        public static MongoCollection<NewProductEntity> NewProducts_Wuerth = GetCollection<NewProductEntity>("NewProducts", "wuerth");

        public static MongoCollection<NewProductEntity> GetDeletedWuerthFound = GetCollection<NewProductEntity>("NewProducts", "deletedFoundWuerthItems");
	}
}
