﻿using DN_Classes.Entities.AsinMultiplier;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DN_Classes.DBCollection.AsinMultiplier
{
    public class AsinMultiplierDBCollection
    {
        public static MongoCollection<AsinMultiplierEntity> AsinMultiplierCollection = new MongoClient(@"mongodb://client148:client148devnetworks@136.243.44.111/AsinMultiplier")
                                                                             .GetServer().GetDatabase("AsinMultiplier")
                                                                             .GetCollection<AsinMultiplierEntity>("asinmultiplier");
    }
}
