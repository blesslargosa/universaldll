﻿using DN_Classes.Entities.ProductTerminator;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DN_Classes.DBCollection.ProductTerminator
{
    class ProductTerminatorDBCollection
    {
        public static MongoCollection<ProductTerminatorEntity> ProductTerminatorCollection = new MongoClient(@"mongodb://client148:client148devnetworks@136.243.44.111/FalseProducts")
                                                                             .GetServer().GetDatabase("FalseProducts")
                                                                             .GetCollection<ProductTerminatorEntity>("falseproducts");
    }
}
