﻿using DN_Classes.Entities;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DN_Classes.DBCollection.FalseProducts
{
    public class FalseProductsDBCollection
    {
        public MongoCollection<FalseProductsEntity> FalseProductsCollection = new MongoClient(@"mongodb://client148:client148devnetworks@136.243.44.111/FalseProducts")
                                                                             .GetServer().GetDatabase("FalseProducts")
                                                                             .GetCollection<FalseProductsEntity>("falseproducts");
    }
}
