﻿using DN_Classes.Entities.AsinMultiplier;
using DN_Classes.Entities.Supplier;
using DN_Classes.Entities.Warehouse;
using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace DN_Classes.DBCollection.Supplier
{
    public class FakeSupplierDBCollection
    {
        public MongoCollection<FakeSupplierEntity> FakeSupplierCollection = new MongoClient(@"mongodb://client148:client148devnetworks@136.243.44.111/PD_Fake")
                                                                             .GetServer().GetDatabase("PD_Fake")
                                                                             .GetCollection<FakeSupplierEntity>("fake");
        public MongoCollection<JtlEntity> jtlcollection = new MongoClient(@"mongodb://client148:client148devnetworks@136.243.44.111/PD_JTL")
                                                                             .GetServer().GetDatabase("PD_JTL")
                                                                             .GetCollection<JtlEntity>("jtl");

        public void UpdateForMultipliers(string ean, /*List<AsinMultiplierEntity>*/string asinmultipliers)
        {
            FakeSupplierDBCollection fakeDB = new FakeSupplierDBCollection();
            FakeSupplierEntity fe = fakeDB.FakeSupplierCollection.FindOne(Query.EQ("product.ean", ean));

            if (fe != null)
            {
                
                //BsonDocument bsonAsinMultiplier;
                BsonArray arrayAsins = new BsonArray();
                BsonArray arrayAsinMultipliers = new BsonArray();
                
                var feMultipliers = fe.plus["multipliers"].AsBsonArray;

                foreach (var v in IdentifyAsinMultiplier(asinmultipliers))
                {
                    arrayAsins.Add(v.Key);                               
                }


                foreach (var v in IdentifyAsinMultiplier(asinmultipliers))
                {
                    arrayAsinMultipliers.Add(
                        new BsonDocument()
                        {
                             {"asin", v.Key},
                             {"multiplier", v.Value},
                        });

                }

                fakeDB.FakeSupplierCollection.Update(Query.EQ("product.ean", fe.product.ean),
                Update.Set("plus.asins", arrayAsins)
                      .Set("plus.multipliers", arrayAsinMultipliers));
            }

        }

        public Dictionary<string, int> IdentifyAsinMultiplier(string asinMultipliers)
        {
            Dictionary<string, int> dicAsinMultiplier = new Dictionary<string, int>();
            string[] items = asinMultipliers.Split('\n', '\r');

            string pattern = @"\b[a-zA-Z\d]{10}[,][0-9]{1,5}\b";
            foreach (var item in items)
            {
                var matches = Regex.Matches(item, pattern);
                var am = matches.Cast<Match>().Select(match => match.Value).ToList();
                foreach (var a in am)
                {
                    string[] asinmultiplier = a.Split(',');
                    dicAsinMultiplier.Add(asinmultiplier[0], Convert.ToInt32(asinmultiplier[1]));
                }
            }

            return dicAsinMultiplier;
        }
    }

}
