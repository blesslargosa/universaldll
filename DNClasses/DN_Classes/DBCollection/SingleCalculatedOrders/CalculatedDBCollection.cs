﻿using DN_Classes.Entities;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DN_Classes.DBCollection.SingleCalculatedOrders
{
    public class CalculatedDBCollection
    {
        public static MongoCollection<CalculatedProfitEntity> SingleCalculatedCollection = new MongoClient(@"mongodb://client148:client148devnetworks@136.243.44.111/CalculatedProfit")
                                                                             .GetServer().GetDatabase("CalculatedProfit") 
                                                                             .GetCollection<CalculatedProfitEntity>("SingleCalculatedItem");


        public static MongoCollection<CalculatedProfitEntity> EbayCalculatedCollection = new MongoClient(@"mongodb://client148:client148devnetworks@136.243.44.111/CalculatedProfit")
                                                                            .GetServer().GetDatabase("CalculatedProfit")
                                                                            .GetCollection<CalculatedProfitEntity>("EbaySingleCalculatedItemV2");

    }
}
