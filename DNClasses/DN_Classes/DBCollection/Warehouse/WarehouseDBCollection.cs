﻿using DN_Classes.Agent;
using DN_Classes.Entities.Warehouse;
using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DN_Classes.Queries
{
    public class WarehouseDBCollection
    {
        public static MongoCollection<JtlEntity> JtlCollection = new MongoClient(@"mongodb://client148:client148devnetworks@136.243.44.111/PD_JTL")
                                                                              .GetServer().GetDatabase("PD_JTL")   
                                                                              .GetCollection<JtlEntity>("jtl");

        public static MongoCollection<JTLOrderEntity> JtlOrdersCollection = new MongoClient(@"mongodb://client148:client148devnetworks@136.243.44.111/JTLOrders")
                                                                              .GetServer().GetDatabase("JTLOrders")
                                                                              .GetCollection<JTLOrderEntity>("jtlorders");

         public static MongoCollection<JTLOrderEntity> JtlOrdersUnshippedCollection = new MongoClient(@"mongodb://client148:client148devnetworks@136.243.44.111/JTLOrders")
                                                                               .GetServer().GetDatabase("JTLOrders")
                                                                               .GetCollection<JTLOrderEntity>("jtlordersUnshipped");

         public static MongoCollection<AgentEntity> AgentsCollection = new MongoClient(@"mongodb://client148:client148devnetworks@136.243.44.111/Agents")
                                                                                 .GetServer().GetDatabase("Agents")
                                                                                 .GetCollection<AgentEntity>("agents");
    }

   
}
