﻿using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DN_Classes.DBCollection.Orders
{
    public class OrdersDBCollection
    {
        public MongoCollection<BsonDocument> AmazonOrdersCollection = new MongoClient(@"mongodb://client148:client148devnetworks@136.243.44.111/Orders")
                                                                              .GetServer().GetDatabase("Orders")
                                                                              .GetCollection<BsonDocument>("SingleOrderItems");


        public MongoCollection<BsonDocument> EbayOrdersCollection = new MongoClient(@"mongodb://client148:client148devnetworks@136.243.44.111/Orders")
                                                                             .GetServer().GetDatabase("Orders")
                                                                             .GetCollection<BsonDocument>("EbaySingleOrderItems");

        public MongoCollection<BsonDocument> SingleOrderItems = new MongoClient(@"mongodb://client148:client148devnetworks@136.243.44.111/Orders")
                                                                             .GetServer().GetDatabase("Orders")
                                                                             .GetCollection<BsonDocument>("SingleOrderItems");

    }
}
