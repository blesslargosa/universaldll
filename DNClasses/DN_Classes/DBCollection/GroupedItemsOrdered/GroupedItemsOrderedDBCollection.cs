﻿using DN_Classes.Entities;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DN_Classes.DBCollection.GroupedItemsOrdered
{
    public class GroupedItemsOrderedDBCollection
    {
        public static MongoCollection<CalculatedProfitEntity> GroupedItemsOrdered = new MongoClient(@"mongodb://client148:client148devnetworks@136.243.44.111/CalculatedProfit")
                                                                             .GetServer().GetDatabase("CalculatedProfit")
                                                                             .GetCollection<CalculatedProfitEntity>("GroupedItemsOrdered");
    }
}
