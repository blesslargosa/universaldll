﻿using DN_Classes.Entities;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DN_Classes.DBCollection.ReOrders
{
    public class ReOrderDBCollection
    {
        public static MongoCollection<CalculatedProfitEntity> ReOrderCollection = new MongoClient(@"mongodb://client148:client148devnetworks@148.251.0.235/CalculatedProfit")
                                                                             .GetServer().GetDatabase("CalculatedProfit")
                                                                             .GetCollection<CalculatedProfitEntity>("ReOrders");
   
    }
}
