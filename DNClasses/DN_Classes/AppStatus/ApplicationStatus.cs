﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.Builders;
using MongoDB.Driver.GridFS;
using MongoDB.Driver.Linq;
using System.Diagnostics;
using DN_Classes.Entities;

namespace DN_Classes.AppStatus
{
    public class ApplicationStatus : IDisposable
    {
        private Stopwatch _stopWatch = new Stopwatch();
        private string _appName = "";
        private List<string> _messageLines = new List<string>();
        private bool _success = false;

        private string _machineName = "";

        private string _machineGlobalIP = "";

        /// <summary>
        /// This will Initialize the Appstaus and starts the time measurement
        /// </summary>
        /// <param name="appName"></param>
        public ApplicationStatus(string appName)
        {
            _appName = appName;

            var machine = new Machine();

            _machineName = machine.GetName();

            _machineGlobalIP = machine.GetGlobalIP();

            _appName += "_" + _machineName;
            _appName += "_" + _machineGlobalIP;

            _stopWatch.Start(); 
        }
        
        /// <summary>
        /// Adds a messageLine to the status output
        /// </summary>
        /// <param name="messageLine"></param>
        public void AddMessagLine(string messageLine)
        {
            _messageLines.Add(messageLine);
        }

        public void Successful()
        {
            _success = true; 
        }

        private void Save()
        {
            Console.WriteLine("Success: " + _success);
            string message = string.Join(Environment.NewLine,_messageLines);
            string executionTimeSpan = GetElapsedTimeString();

            var collection = GetCollection();

            if (collection.Find(Query.EQ("AppName", _appName)).Count() > 0)
            {
                UpdateBuilder update = Update.Set("RunningTime", DateTime.Now)
                    .Set("ExecutionTime", executionTimeSpan)
                    .Set("Status",message)
                    .Set("Success",_success)
                    .Set("ComputerName", _machineName)
                    .Set("ComputerGlobalIP", _machineGlobalIP);
                collection.Update(Query.EQ("AppName", _appName), update);
            }
            else
            {
                collection.Insert(new ApplicationStatusEntity { AppName = _appName, RunningTime = DateTime.Now, ExecutionTime = executionTimeSpan, Status = message, Success = _success, ComputerName = _machineName, ComputerGlobalIP = _machineGlobalIP });
            }
        }
        

        public List<ApplicationStatusEntity> GetApplicationsOnDB()
        {
            return GetCollection().FindAll().ToList();
        }

        public void Dispose()
        {
            _stopWatch.Stop();
            Save();
        }

        #region private methods
        private static MongoCollection<ApplicationStatusEntity> GetCollection()
        {
            return new MongoClient(@"mongodb://client148:client148devnetworks@136.243.44.111/ApplicationDatabase").GetServer().GetDatabase("ApplicationDatabase").GetCollection<ApplicationStatusEntity>("applications");
        }

        private string GetElapsedTimeString()
        {
            TimeSpan elapsedTime = _stopWatch.Elapsed;

            string executionTimeSpan = elapsedTime.Days + "d : " + elapsedTime.Hours + "h : " + elapsedTime.Minutes + "m : " + elapsedTime.Seconds + "s : " + elapsedTime.Milliseconds + "ms";
            return executionTimeSpan;
        }
        #endregion
        #region Obsolete methods
        [Obsolete("Use C# \"using\" statement")]
        public void Start()
        {
            _stopWatch = new Stopwatch();
            _stopWatch.Start();
        }

        [Obsolete("Use C# \"using\" statement")]
        public void Stop()
        {
            _stopWatch.Stop();
            Save();
        }

        [Obsolete("Use AddMessagLine instead")]
        public void Stop(string message)
        {
            _stopWatch.Stop();
            AddMessagLine(message);
            Save();
        }
        #endregion
    }
}

