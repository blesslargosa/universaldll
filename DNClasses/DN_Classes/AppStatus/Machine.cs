﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace DN_Classes.AppStatus
{
    public class Machine : IMachine
    {
        public string GetName()
        {
            try
            {
                return System.Environment.MachineName;
            }
            catch { return ""; }
        }

        public string GetGlobalIP()
        {
            try
            {
                //string externalip = new WebClient().DownloadString("http://icanhazip.com");
                string externalip = new WebClient().DownloadString("http://checkip.amazonaws.com/");
                return externalip.Trim();
            }
            catch { return ""; }

        }
    }
}
