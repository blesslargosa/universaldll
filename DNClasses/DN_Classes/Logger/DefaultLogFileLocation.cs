﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DN_Classes.Logger
{
    public class DefaultLogFileLocation : ILogFileLocation
    {
        public string LogDirectory { get; set; }
        public string ToolName { get; set; }
        public string SubToolName { get; set; }
    }
}
