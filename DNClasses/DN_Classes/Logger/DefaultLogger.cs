﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DN_Classes.Logger.DbPersistent;

namespace DN_Classes.Logger
{
    public class DefaultLogger : ILogger
    {

        //private readonly string subToolName;
        //public string SubToolName
        //{
        //    get { return subToolName; }
        //}

        //private readonly string toolName;
        //public string ToolName
        //{
        //    get { return toolName; }
        //}

        //private readonly string logDirectory;
        //public string LogDirectory
        //{
        //    get { return logDirectory; }
        //}

        private StringBuilder _infoLogsContainer;
        public StringBuilder InfoLogsContainer
        {
            get { return _infoLogsContainer; }
        }

        private StringBuilder _warningLogsContainer;
        public StringBuilder WarningLogsContainer
        {
            get { return _warningLogsContainer; }
        }

        private StringBuilder _errorLogsContainer;
        public StringBuilder ErrorLogsContainer
        {
            get { return _errorLogsContainer; }
        }

        object addingLogLock = new object();
        object writingLogsToFileLock = new object();

        private readonly ILogUploader _logUploader;
        private readonly ILogFileLocation _logFileLocation;
        private readonly ILogDbPersistent _logDbPersistent;

        public DefaultLogger(ILogFileLocation logFileLocation, ILogUploader logUploader, ILogDbPersistent logDbPersistent)
        {
            AppLogger.SetLogger(this);

            this._logUploader = logUploader;
            this._logFileLocation = logFileLocation;
            this._logDbPersistent = logDbPersistent;

            if (string.IsNullOrEmpty(logFileLocation.SubToolName))
            {
                throw new NullReferenceException("ILogFileLocation.SubToolName must be set");
            }

            if (string.IsNullOrEmpty(logFileLocation.ToolName))
            {
                throw new NullReferenceException("ILogFileLocation.ToolName must be set");
            }

            if (string.IsNullOrEmpty(logFileLocation.LogDirectory))
            {
                throw new NullReferenceException("ILogFileLocation.LogDirectory must be set");
            }

            if (!Directory.Exists(logFileLocation.LogDirectory))
            {
                Directory.CreateDirectory(logFileLocation.LogDirectory);
            }

            //this.logDirectory = logFileLocation.LogDirectory;
            //this.toolName = logFileLocation.ToolName;
            //this.subToolName = logFileLocation.SubToolName;

            _infoLogsContainer = new StringBuilder();
            _infoLogsContainer.AppendLine(DateTime.Now.ToString("dd MMM yyyy hh:mm:ss"));

            _warningLogsContainer = new StringBuilder();
            _warningLogsContainer.AppendLine(DateTime.Now.ToString("dd MMM yyyy hh:mm:ss"));

            _errorLogsContainer = new StringBuilder();
            _errorLogsContainer.AppendLine(DateTime.Now.ToString("dd MMM yyyy hh:mm:ss"));
        }

        public void Info(string log)
        {
            lock (addingLogLock)
            {
                log = DateTime.Now.ToString("hh:mm:ss") + " " + log;
                _infoLogsContainer.AppendLine(log);
                Console.WriteLine(log);
            }
        }

        public void Warn(string log)
        {
            lock (addingLogLock)
            {
                log = DateTime.Now.ToString("hh:mm:ss") + " " + log;
                _warningLogsContainer.AppendLine(log);
                Console.WriteLine(log);
            }
        }

        public void Error(string log)
        {
            lock (addingLogLock)
            {
                log = DateTime.Now.ToString("hh:mm:ss") + " " + log;
                _errorLogsContainer.AppendLine(log);
                Console.WriteLine(log);
            }
        }

        public void Dispose()
        {
            lock (writingLogsToFileLock)
            {
                string infoLogFileName = DateTime.Now.Ticks + "InfoLogs.txt";
                File.WriteAllText(_logFileLocation.LogDirectory + @"\" + infoLogFileName, _infoLogsContainer.ToString());

                string warningLogFileName = DateTime.Now.Ticks + "WarningLogs.txt";
                File.WriteAllText(_logFileLocation.LogDirectory + @"\" + warningLogFileName, _warningLogsContainer.ToString());

                string errorLogFileName = DateTime.Now.Ticks + "ErrorLogs.txt";
                File.WriteAllText(_logFileLocation.LogDirectory + @"\" + errorLogFileName, _errorLogsContainer.ToString());

                _logUploader.UploadLog(infoLogFileName);
                _logUploader.UploadLog(warningLogFileName);
                _logUploader.UploadLog(errorLogFileName);

                _logDbPersistent.Save(_logFileLocation.ToolName, infoLogFileName);
                _logDbPersistent.Save(_logFileLocation.ToolName, warningLogFileName);
                _logDbPersistent.Save(_logFileLocation.ToolName, errorLogFileName);
            }

            
        }

    }
}
