﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DN_Classes.Logger.DbPersistent;
using DN_Classes.Logger.DbPersistent.DbFactory;
using Microsoft.Practices.Unity;

namespace DN_Classes.Logger
{
    public class LoggingConfig
    {
        private readonly IUnityContainer _container;

        public LoggingConfig()
        {
            _container = new UnityContainer();

            RegisterRequirements();
        }

        private void RegisterRequirements()
        {
            _container.RegisterType<ILogUploader, LogUploader>();
            _container.RegisterType<ILogDbFactory, DefaultLogDbFactory>();
            _container.RegisterType<ILogDbPersistent, NullLogDbPersistent>();
            _container.RegisterType<ILogger, DefaultLogger>();
        }

        public T Resolve<T>()
        {
            return _container.Resolve<T>();
        }

        public void Register<T>(T registree)
        {
            _container.RegisterInstance(registree);
        }
    }
}
