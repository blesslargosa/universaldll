﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DN.DataAccess.ConnectionFactory;
using DN.DataAccess.ConnectionFactory.Mongo;

namespace DN_Classes.Logger.DbPersistent.DbFactory
{
    public class DefaultLogDbFactory : ILogDbFactory
    {
        private readonly IDbConnectionFactory _connectionFactory;

        public DefaultLogDbFactory()
        {
            IConnectionProperties connectionProperties = new MongoConnectionProperties();
            connectionProperties.SetProperty("connectionstring", "mongodb://client148:client148devnetworks@136.243.44.111/Logging");
            connectionProperties.SetProperty("databasename", "Logging");
            connectionProperties.SetProperty("collectionname", "logs");

            _connectionFactory = new MongoConnectionFactory(connectionProperties);
        }

        public IDbConnection LogDbConnection()
        {
            return this._connectionFactory.CreateConnection();
        }
    }
}
