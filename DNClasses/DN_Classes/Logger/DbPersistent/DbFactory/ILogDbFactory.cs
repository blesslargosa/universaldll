﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DN.DataAccess.ConnectionFactory;

namespace DN_Classes.Logger.DbPersistent.DbFactory
{
    public interface ILogDbFactory
    {
        IDbConnection LogDbConnection();
    }
}
