﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DN_Classes.Logger.DbPersistent
{
    public interface ILogDbPersistent
    {
        void Save(string toolName, string logFileName);
    }
}
