﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DN.DataAccess.ConnectionFactory;
using DN_Classes.Logger.DbPersistent.DbFactory;
using MongoDB.Bson;

namespace DN_Classes.Logger.DbPersistent
{
    public class LogDbPersistent : ILogDbPersistent
    {
        private readonly IDbConnection _dbConnection;

        public LogDbPersistent(ILogDbFactory logDbFactory)
        {
            this._dbConnection = logDbFactory.LogDbConnection();
        }

        public void Save(string toolName, string logFileName)
        {
            var toolDbRecord = _dbConnection.DataAccess.Queries.FindByKey<BsonDocument>("_id", toolName);

            if (toolDbRecord == null)
            {
                BsonDocument toolDbRecordToPersists = new BsonDocument
                {
                    {"_id", toolName}
                };

                _dbConnection.DataAccess.Commands.Insert(toolDbRecordToPersists);
            }

            _dbConnection.DataAccess.Commands.AddToSet("{_id:\"" + toolName + "\"}", "logs", logFileName);
        }
    }
}
