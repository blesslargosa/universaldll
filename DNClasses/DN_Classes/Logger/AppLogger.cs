﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DN_Classes.Logger
{
    public static class AppLogger
    {
        private static ILogger logger = null;

        public static void Info(string log)
        {
            if (logger == null)
            {
                throw new NullReferenceException("'logger' of type ILogger must be set");
            }

            logger.Info(log);
        }

        public static void Warn(string log)
        {
            if (logger == null)
            {
                throw new NullReferenceException("'logger' of type ILogger must be set");
            }

            logger.Warn(log);
        }

        public static void Error(string log)
        {
            if (logger == null)
            {
                throw new NullReferenceException("'logger' of type ILogger must be set");
            }

            logger.Error(log);
        }

        public static void SetLogger(ILogger _logger)
        {
            logger = _logger;
        }

        public static ILogger Logger() { return logger;}
    }
}
