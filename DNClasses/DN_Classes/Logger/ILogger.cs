﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DN_Classes.Logger
{
    public interface ILogger : IDisposable
    {
        //string LogDirectory { get; }
        //string ToolName { get; }
        //string SubToolName { get; }

        StringBuilder InfoLogsContainer { get; }

        void Info(string log);

        StringBuilder WarningLogsContainer { get; }

        void Warn(string log);

        StringBuilder ErrorLogsContainer { get; }

        void Error(string log);

        

    }
}
