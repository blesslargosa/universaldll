﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace DN_Classes.Logger
{
    public class LogUploader : ILogUploader
    {
        private readonly ILogFileLocation _logFileLocation;

        public LogUploader(ILogFileLocation logFileLocation)
        {
            _logFileLocation = logFileLocation;

            CreateUploadDirectory("ftp://148.251.0.235:2016/" + _logFileLocation.ToolName);
            CreateUploadDirectory("ftp://148.251.0.235:2016/" + _logFileLocation.ToolName + "/" + _logFileLocation.SubToolName);
        }

        public void UploadLog(string filename)
        {
            //Uploading logs

            FtpWebRequest uploadFtpFileRequest = (FtpWebRequest)WebRequest.Create("ftp://148.251.0.235:2016/" +_logFileLocation.ToolName +"/" +_logFileLocation.SubToolName + "/" +filename);
            uploadFtpFileRequest.Method = WebRequestMethods.Ftp.UploadFile;

            StreamReader sourceStream = new StreamReader(_logFileLocation.LogDirectory + @"\" + filename);
            byte[] fileContents = Encoding.UTF8.GetBytes(sourceStream.ReadToEnd());
            sourceStream.Close();
            uploadFtpFileRequest.ContentLength = fileContents.Length;

            Stream requestStream = uploadFtpFileRequest.GetRequestStream();
            requestStream.Write(fileContents, 0, fileContents.Length);
            requestStream.Close();

            FtpWebResponse response = (FtpWebResponse)uploadFtpFileRequest.GetResponse();

            Console.WriteLine("Upload File Complete, status {0}", response.StatusDescription);

            response.Close();
        }

        private void CreateUploadDirectory(string uri)
        {
            FtpWebRequest createFtpDirRequest = (FtpWebRequest)FtpWebRequest.Create(uri);

            createFtpDirRequest.UsePassive = true;
            createFtpDirRequest.UseBinary = true;
            createFtpDirRequest.KeepAlive = false;

            createFtpDirRequest.Method = WebRequestMethods.Ftp.MakeDirectory;

            try
            {
                using (var resp = (FtpWebResponse)createFtpDirRequest.GetResponse())
                {
                    Console.WriteLine(resp.StatusCode);
                }
            }

            catch (Exception ex)
            {
                Console.WriteLine("Ftp Directory Creation error : " + ex.Message);
            }
        }


    }
}

