﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DN_Classes.Logger
{
    public interface ILogFileLocation
    {
        string LogDirectory { get; set; }
        string ToolName { get; set; }
        string SubToolName { get; set; }
    }
}
