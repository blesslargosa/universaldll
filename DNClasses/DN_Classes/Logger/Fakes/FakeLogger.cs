﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DN_Classes.Logger.Fakes
{
    public class FakeLogger : ILogger
    {
        public string LogDirectory
        {
            get { return ""; }
        }

        public string LogFileName
        {
            get { return ""; }
        }

        public StringBuilder LogsContainer
        {
            get { return new StringBuilder();}
        }

        public void Log(string log)
        {
            
        }

        public void Dispose()
        {
            
        }


        public StringBuilder InfoLogsContainer
        {
            get { throw new NotImplementedException(); }
        }

        public void Info(string log)
        {
        }

        public StringBuilder WarningLogsContainer
        {
            get { throw new NotImplementedException(); }
        }

        public void Warn(string log)
        {
        }

        public StringBuilder ErrorLogsContainer
        {
            get { throw new NotImplementedException(); }
        }

        public void Error(string log)
        {
        }

        public string ToolName
        {
            get { throw new NotImplementedException(); }
        }

        public string SubToolName
        {
            get { throw new NotImplementedException(); }
        }
    }
}
