﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using DN_Classes.Rules.SellingRules;
using MongoDB.Bson;

namespace DN_Classes.Tests.Rules.SellingRules
{
    [TestClass]
    public class DefaultProductSellingTests
    {
        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void IsSellingReady_WithNullParam_ThrowsArgumentNullException()
        {
            //Arrange
            IProductSelling defaultProductSelling = new DefaultProductSelling();

            //Act
            defaultProductSelling.IsSellingReady<string>(null);

            //Assert is done by ExpectedException attribute
        }

        [TestMethod]
        public void IsSellingReady_WithValidParam_NoException()
        {
            //Arrange
            IProductSelling defaultProductSelling = new DefaultProductSelling();

            try
            {
                //Act
                defaultProductSelling.IsSellingReady<string>("");
            }
            catch
            {
                //Assert
                Assert.Fail();
            }
        }

        [TestMethod]
        public void IsSellingReady_WithSetParam_ReturnsTrue()
        {
            //Arrange
            IProductSelling defaultProductSelling = new DefaultProductSelling();
            BsonDocument plus = new BsonDocument
            {
                {"startselling", "02-01-2016"}
            };

            //Act
            bool isSellingReady = defaultProductSelling.IsSellingReady<BsonDocument>(plus);

            //Assert 
            Assert.IsTrue(isSellingReady);
        }

        [TestMethod]
        public void IsSellingReady_WithSetParam_ReturnsFalse()
        {
            //Arrange
            IProductSelling defaultProductSelling = new DefaultProductSelling();
            BsonDocument plus = new BsonDocument
            {
                {"startselling", "25-01-2016"}
            };

            //Act
            bool isSellingReady = defaultProductSelling.IsSellingReady<BsonDocument>(plus);

            //Assert 
            Assert.IsFalse(isSellingReady);
        }
    }
}
