﻿using DN_Classes.OrderLimiter;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DN_Classes.Tests.OrderLimiter
{
    [TestClass]
    public class SalesLimiterTests
    {
        [TestMethod]
        public void BlockedList_WithUnknownConstructorParam_ReturnsEmptyList()
        {
            //Arrange
            ISalesLimiter salesLimiter = new SalesLimiter();

            //Act
            var blockedList = salesLimiter.BlockedList("unknownsupplier");

            //Assert
            Assert.IsNotNull(blockedList);
        }

        [TestMethod]
        public void BlockedListWithSKU_WithUnknownConstructorParam_ReturnsEmptyList()
        {
            //Arrange
            ISalesLimiter salesLimiter = new SalesLimiter();

            //Act
            var blockedListWithSKU = salesLimiter.BlockedListWithSKU("unknownsupplier");

            //Assert
            Assert.IsNotNull(blockedListWithSKU);
        }
    }
}
