﻿using DN_Classes.AppStatus;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace DN_Classes.Tests.AppStatus
{
    [TestClass]
    public class MachineTests
    {
        [TestMethod]
        public void GetName_NameExpected()
        {
            var machine = new Machine();

            string name = machine.GetName();

            Assert.IsTrue(name != "");
        }

        [TestMethod]
        public void GetGlobalIP_IPExpected()
        {
            var machine = new Machine();

            string ip = machine.GetGlobalIP();

            Assert.IsTrue(ip != "");
        }
    }
}
