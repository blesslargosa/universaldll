﻿using DN_Classes.DBCollection.Orders;
using DN_Classes.DBCollection.Supplier;
using MongoDB.Driver.Builders;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace DN_Classes.Finder
{
    public static class PriorityRepricingAsinFinder
    {
        public static HashSet<string> FindActive()
        {
            HashSet<string> activeAsins = new HashSet<string>();

            List<string> activeSuppliersTransferFileUrls = new SupplierDBCollection().SupplierCollection.Find(Query.EQ("active", true)).Where(x => !String.IsNullOrEmpty(x.GetTransferFile())).Select(x => x.GetTransferFile()).ToList();

            Regex asinRegex = new Regex("\\b[A-Z0-9]{10}\\b");

            ParallelOptions parallelOptions = new ParallelOptions { MaxDegreeOfParallelism = 8 };
            Parallel.ForEach(activeSuppliersTransferFileUrls, parallelOptions, activeSuppliersTransferFileUrl =>
            {

                string transferFileContent = "";

                try
                {
                    transferFileContent = new WebClient().DownloadString(activeSuppliersTransferFileUrl);
                }
                catch { }

                List<string> transferFileLines = new List<string>();

                if (transferFileContent != "")
                {
                    transferFileLines = transferFileContent.Split(new string[] { "\r\n" }, StringSplitOptions.None).ToList();
                }

                foreach (var x in transferFileLines)
                {
                    MatchCollection matches = asinRegex.Matches(x);
                    foreach (var match in matches)
                    {
                        activeAsins.Add(match.ToString());
                    }
                }

            });

            return activeAsins;
        }


        public static HashSet<string> FindSold(TimeSpan time)
        {
            HashSet<string> soldAsins = new HashSet<string>();

            DateTime dateBack = DateTime.Now;

            dateBack = dateBack - time;

            var singleOrderItemDocuments = new OrdersDBCollection().SingleOrderItems
                .Find(Query.Exists("_id")).SetFields("Order.ASIN", "Order.PurchaseDate");

            ParallelOptions pOptions = new ParallelOptions { MaxDegreeOfParallelism = 8 };
            Parallel.ForEach(singleOrderItemDocuments, pOptions, document =>
            {
                DateTime purchasedDate = DateTime.MinValue;
                try
                {
                    purchasedDate = DateTime.Parse(document["Order"]["PurchaseDate"].AsString, CultureInfo.InvariantCulture);
                }
                catch (Exception ex)
                {
                    purchasedDate = DateTime.ParseExact(document["Order"]["PurchaseDate"].AsString, "dd.MM.yyyy HH:mm:ss", CultureInfo.InvariantCulture);
                }

                if (purchasedDate >= dateBack)
                {
                    if (document["Order"]["ASIN"].IsBsonArray)
                    {
                        foreach (var asinDoc in document["Order"]["ASIN"].AsBsonArray)
                        {
                            soldAsins.Add(asinDoc.AsString);
                        }
                    }
                    else
                    {
                        soldAsins.Add(document["Order"]["ASIN"].AsString);
                    }
                }
            });

            #region not parallel
            //foreach (var document in singleOrderItemDocuments)
            //{
            //    DateTime purchasedDate = DateTime.MinValue;
            //    try
            //    {
            //        purchasedDate = DateTime.Parse(document["Order"]["PurchaseDate"].AsString, CultureInfo.InvariantCulture);
            //    }
            //    catch (Exception ex)
            //    {
            //        purchasedDate = DateTime.ParseExact(document["Order"]["PurchaseDate"].AsString, "dd.MM.yyyy HH:mm:ss", CultureInfo.InvariantCulture);
            //    }

            //    if (purchasedDate >= dateBack)
            //    {
            //        if (document["Order"]["ASIN"].IsBsonArray)
            //        {
            //            foreach (var asinDoc in document["Order"]["ASIN"].AsBsonArray)
            //            {
            //                soldAsins.Add(asinDoc.AsString);
            //            }
            //        }
            //        else
            //        {
            //            soldAsins.Add(document["Order"]["ASIN"].AsString);
            //        }
            //    }
            //}
            #endregion

            return soldAsins;
        }
    }
}
