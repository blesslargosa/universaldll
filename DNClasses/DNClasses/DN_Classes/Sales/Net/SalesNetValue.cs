﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DN_Classes.Sales.Net
{
    public class SalesNetValue : INetValue
    {
        public double GetNetValue(double grossValue, int taxRate)
        {
            return Math.Round(grossValue / ((100 + taxRate)/100));
        }
    }
}
