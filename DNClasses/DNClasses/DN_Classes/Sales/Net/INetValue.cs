﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DN_Classes.Sales.Net
{
    public interface INetValue
    {
        double GetNetValue(double grossValue, int taxRate);
    }
}
