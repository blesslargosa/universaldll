﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
namespace DN.DataAccess.ConfigurationData
{
    public class AppConfigurationManager : IConfigurationManager
    {
        private readonly Configuration _config;

        public AppConfigurationManager(IConfigurationSource configurationSource)
        {
            configurationSource.SetConfigurationFile();

            this._config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
        }

        public string GetValue(string name)
        {
            //return ConfigurationManager.AppSettings[name];
            return _config.AppSettings.Settings[name].Value;
        }


        public void SetValue(string name, string value)
        {
            //ConfigurationManager.AppSettings[name] = value;
            _config.AppSettings.Settings[name].Value = value;
        }
    }
}
