﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace DN.DataAccess.ConfigurationData
{
    /// <summary>
    /// Manages the configuration file source
    /// </summary>
    public class ConfigurationSource : IConfigurationSource
    {
        /// <summary>
        /// configuration file path
        /// </summary>
        private readonly string _configFullPath;

        /// <summary>
        /// class constructor
        /// </summary>
        /// <param name="configFullPath">configuration file path</param>
        public ConfigurationSource(string configFullPath)
        {
            this._configFullPath = configFullPath;
        }

        /// <summary>
        /// setting system's config file
        /// </summary>
        public void SetConfigurationFile()
        {
            AppDomain.CurrentDomain.SetData("APP_CONFIG_FILE", _configFullPath);
            
            ResetConfigMechanism();
        }

        /// <summary>
        /// There exists a class ClientConfigPaths that caches the paths. 
        /// So, even after changing the path with SetData, it is not re-read, because there already exist cached values. 
        /// The solution is to remove these
        /// http://stackoverflow.com/questions/6150644/change-default-app-config-at-runtime
        /// </summary>
        private void ResetConfigMechanism()
        {
            typeof(ConfigurationManager)
                .GetField("s_initState", BindingFlags.NonPublic | 
                                         BindingFlags.Static)
                .SetValue(null, 0);

            typeof(ConfigurationManager)
                .GetField("s_configSystem", BindingFlags.NonPublic | 
                                            BindingFlags.Static)
                .SetValue(null, null);

            typeof(ConfigurationManager)
                .Assembly.GetTypes()
                .Where(x => x.FullName == 
                            "System.Configuration.ClientConfigPaths")
                .First()
                .GetField("s_current", BindingFlags.NonPublic | 
                                       BindingFlags.Static)
                .SetValue(null, null);
        }
    }
}
