﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DN.DataAccess.DataCommand
{
    public interface IDataCommand
    {
        /// <summary>
        /// updates an item
        /// </summary>
        /// <param name="queryKey"></param>
        /// <param name="queryValue"></param>
        /// <param name="updateField"></param>
        /// <param name="updateValue"></param>
        /// <returns></returns>
        bool Update<TValue>(string queryKey, string queryValue, string updateField, TValue updateValue, bool multi = false, bool upsert = false);

        /// <summary>
        /// updates an item
        /// </summary>
        /// <param name="query"></param>
        /// <param name="updateField"></param>
        /// <param name="updateValue"></param>
        /// <returns></returns>
        bool Update<TValue>(string query, string updateField, TValue updateValue, bool multi = false, bool upsert = false);

        /// <summary>
        /// adds value to an array
        /// </summary>
        /// <typeparam name="TValue"></typeparam>
        /// <param name="query"></param>
        /// <param name="arrayField"></param>
        /// <param name="arrayValue"></param>
        /// <param name="multi"></param>
        /// <param name="upsert"></param>
        /// <returns></returns>
        bool AddToSet<TValue>(string query, string arrayField, TValue arrayValue, bool multi = false, bool upsert = false);

        /// <summary>
        /// removes value to an array
        /// </summary>
        /// <typeparam name="TValue"></typeparam>
        /// <param name="query"></param>
        /// <param name="arrayField"></param>
        /// <param name="arrayValue"></param>
        /// <param name="multi"></param>
        /// <param name="upsert"></param>
        /// <returns></returns>
        bool Pull<TValue>(string query, string arrayField, TValue arrayValue, bool multi = false, bool upsert = false);

        /// <summary>
        /// adds value to an array
        /// </summary>
        /// <typeparam name="TValue"></typeparam>
        /// <param name="query"></param>
        /// <param name="arrayField"></param>
        /// <param name="arrayValue"></param>
        /// <param name="multi"></param>
        /// <param name="upsert"></param>
        /// <returns></returns>
        bool Push<TValue>(string query, string arrayField, TValue arrayValue, bool multi = false, bool upsert = false);

        /// <summary>
        /// deletes an item
        /// </summary>
        /// <param name="key"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        bool Delete(string key, string value);

        /// <summary>
        /// deletes an item
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        bool Delete(string query);

        /// <summary>
        /// inserts generic object
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="obj"></param>
        /// <returns></returns>
        bool Insert<T>(T obj);
    }
}
