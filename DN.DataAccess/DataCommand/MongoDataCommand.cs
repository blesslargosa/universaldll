﻿using DN.DataAccess.DataCommand;
using DN.DataAccess.DataQuery;
using MongoDB.Bson;
using MongoDB.Bson.Serialization;
using MongoDB.Driver;
using MongoDB.Driver.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DN.DataAccess.DataCommand
{
    public class MongoDataCommand: IDataCommand
    {
        private readonly MongoCollection<BsonDocument> collection;

        public MongoDataCommand(MongoCollection<BsonDocument> collection)
        {
            this.collection = collection;
        }

        public bool Update<TValue>(string queryKey, string queryValue, string updateField, TValue updateValue, bool multi = false, bool upsert = false)
        {

            WriteConcernResult updateRequest = new WriteConcernResult(new BsonDocument());

            BsonValue bsonValue = null;

            try
            {
                bsonValue = BsonValue.Create(updateValue);
            }
            catch
            {
            }

            if (bsonValue == null)
            {
                try
                {
                    bsonValue = BsonSerializer.Deserialize<BsonDocument>(updateValue.ToJson());
                }
                catch { }
            }

            if (multi)
            {
                updateRequest = collection.Update(Query.EQ(queryKey, queryValue), MongoDB.Driver.Builders.Update.Set(updateField, bsonValue), UpdateFlags.Multi);
            }
            else if (upsert)
            {
                updateRequest = collection.Update(Query.EQ(queryKey, queryValue), MongoDB.Driver.Builders.Update.Set(updateField, bsonValue), UpdateFlags.Upsert);
            }
            else
            {
                updateRequest = collection.Update(Query.EQ(queryKey, queryValue), MongoDB.Driver.Builders.Update.Set(updateField, bsonValue), UpdateFlags.None);
            }

            return updateRequest.UpdatedExisting;
        }

        public bool Update<TValue>(string query, string updateField, TValue updateValue, bool multi = false, bool upsert = false)
        {
            WriteConcernResult updateRequest = new WriteConcernResult(new BsonDocument());

            BsonValue bsonValue = null;

            try
            {
                bsonValue = BsonValue.Create(updateValue);
            }
            catch
            {
            }

            if (bsonValue == null)
            {
                try
                {
                    bsonValue = BsonSerializer.Deserialize<BsonDocument>(updateValue.ToJson());
                }
                catch { }
            }

            BsonDocument document = BsonSerializer.Deserialize<BsonDocument>(query);
            QueryDocument queryDocument = new QueryDocument(document);

            if (multi)
            {
                updateRequest = collection.Update(queryDocument, MongoDB.Driver.Builders.Update.Set(updateField, bsonValue), UpdateFlags.Multi);
            }
            else if (upsert)
            {
                updateRequest = collection.Update(queryDocument, MongoDB.Driver.Builders.Update.Set(updateField, bsonValue), UpdateFlags.Upsert);
            }
            else
            {
                updateRequest = collection.Update(queryDocument, MongoDB.Driver.Builders.Update.Set(updateField, bsonValue), UpdateFlags.None);
            }

            return updateRequest.UpdatedExisting;
        }

        public bool AddToSet<TValue>(string query, string arrayField, TValue arrayValue, bool multi = false, bool upsert = false)
        {
            WriteConcernResult updateRequest = new WriteConcernResult(new BsonDocument());

            BsonValue bsonValue = null;

            try
            {
                bsonValue = BsonValue.Create(arrayValue);
            }
            catch
            {
            }

            if (bsonValue == null)
            {
                try
                {
                    bsonValue = BsonSerializer.Deserialize<BsonDocument>(arrayValue.ToJson());
                }
                catch { }
            }

            BsonDocument document = BsonSerializer.Deserialize<BsonDocument>(query);
            QueryDocument queryDocument = new QueryDocument(document);

            UpdateFlags updateFlag = UpdateFlags.None;

            if (multi)
            {
                updateFlag = UpdateFlags.Multi;
            }
            else if (upsert)
            {
                updateFlag = UpdateFlags.Upsert;
            }

            updateRequest = collection.Update(queryDocument, MongoDB.Driver.Builders.Update.AddToSet(arrayField, bsonValue), updateFlag);

            return updateRequest.UpdatedExisting;
        }

        public bool Pull<TValue>(string query, string arrayField, TValue arrayValue, bool multi = false, bool upsert = false)
        {
            WriteConcernResult updateRequest = new WriteConcernResult(new BsonDocument());

            BsonValue bsonValue = null;

            try
            {
                bsonValue = BsonValue.Create(arrayValue);
            }
            catch
            {
            }

            if (bsonValue == null)
            {
                try
                {
                    bsonValue = BsonSerializer.Deserialize<BsonDocument>(arrayValue.ToJson());
                }
                catch { }
            }

            BsonDocument document = BsonSerializer.Deserialize<BsonDocument>(query);
            QueryDocument queryDocument = new QueryDocument(document);

            UpdateFlags updateFlag = UpdateFlags.None;

            if (multi)
            {
                updateFlag = UpdateFlags.Multi;
            }
            else if (upsert)
            {
                updateFlag = UpdateFlags.Upsert;
            }

            updateRequest = collection.Update(queryDocument, MongoDB.Driver.Builders.Update.Pull(arrayField, bsonValue), updateFlag);

            return updateRequest.UpdatedExisting;
        }

        public bool Push<TValue>(string query, string arrayField, TValue arrayValue, bool multi = false, bool upsert = false)
        {
            WriteConcernResult updateRequest = new WriteConcernResult(new BsonDocument());

            BsonValue bsonValue = null;

            try
            {
                bsonValue = BsonValue.Create(arrayValue);
            }
            catch
            {
            }

            if (bsonValue == null)
            {
                try
                {
                    bsonValue = BsonSerializer.Deserialize<BsonDocument>(arrayValue.ToJson());
                }
                catch { }
            }

            BsonDocument document = BsonSerializer.Deserialize<BsonDocument>(query);
            QueryDocument queryDocument = new QueryDocument(document);

            UpdateFlags updateFlag = UpdateFlags.None;

            if (multi)
            {
                updateFlag = UpdateFlags.Multi;
            }
            else if (upsert)
            {
                updateFlag = UpdateFlags.Upsert;
            }

            updateRequest = collection.Update(queryDocument, MongoDB.Driver.Builders.Update.Push(arrayField, bsonValue), updateFlag);

            return updateRequest.UpdatedExisting;
        }

        public bool Delete(string key, string value)
        {
            var deleteRequest = collection.Remove(Query.EQ(key, value));

            return !deleteRequest.HasLastErrorMessage;
        }

        public bool Delete(string query)
        {
            BsonDocument document = BsonSerializer.Deserialize<BsonDocument>(query);
            QueryDocument queryDocument = new QueryDocument(document);

            var deleteRequest = collection.Remove(queryDocument);

            return !deleteRequest.HasLastErrorMessage;
        }

        /// <summary>
        /// tested => will throw an error when passing JObject type
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="obj"></param>
        /// <returns></returns>
        public bool Insert<T>(T obj)
        {
            BsonDocument document = new BsonDocument();

            //string type
            if (typeof(T) == typeof(string))
            {
                document = BsonSerializer.Deserialize<BsonDocument>(obj as string);
            }
            else
            {
                document = BsonSerializer.Deserialize<BsonDocument>(obj.ToJson());
            }

            var insertRequest = collection.Insert(document);

            return !insertRequest.HasLastErrorMessage;
        }
    }
}
