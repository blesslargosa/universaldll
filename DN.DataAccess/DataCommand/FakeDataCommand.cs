﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DN.DataAccess.DataCommand
{
    public class FakeDataCommand : IDataCommand
    {
        public bool Update<TValue>(string queryKey, string queryValue, string updateField, TValue updateValue, bool multi = false, bool upsert = false)
        {
            return true;
        }

        public bool Update<TValue>(string query, string updateField, TValue updateValue, bool multi = false, bool upsert = false)
        {
            return true;
        }

        public bool AddToSet<TValue>(string query, string arrayField, TValue arrayValue, bool multi = false, bool upsert = false)
        {
            return true;
        }

        public bool Pull<TValue>(string query, string arrayField, TValue arrayValue, bool multi = false, bool upsert = false)
        {
            return true;
        }

        public bool Push<TValue>(string query, string arrayField, TValue arrayValue, bool multi = false, bool upsert = false)
        {
            return true;
        }

        public bool Delete(string key, string value)
        {
            return true;
        }

        public bool Delete(string query)
        {
            return true;
        }

        public bool Insert<T>(T obj)
        {
            return true;
        }
    }
}
