﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DN.DataAccess.DataCommand;
using DN.DataAccess.DataQuery;

namespace DN.DataAccess
{
    public class FakeDataAccess : IDataAccess
    {
        public DataCommand.IDataCommand Commands
        {
            get { return new FakeDataCommand();}
        }

        public DataQuery.IDataQuery Queries
        {
            get { return new FakeDataQuery();}
        }
    }
}
