﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DN.DataAccess.ConnectionFactory.Fakes
{
    public class FakeConnectionFactory : IDbConnectionFactory
    {
        public IDbConnection CreateConnection()
        {
            return new FakeDbConnection();
        }

        public IConnectionProperties ConnectionProperties
        {
            get { return new FakeConnectionProperties(); }
        }
    }
}
