﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DN.DataAccess.ConnectionFactory.Fakes
{
    public class FakeDbConnection : IDbConnection
    {
        public IDataAccess DataAccess
        {
            get { return new FakeDataAccess(); }
        }
    }
}
