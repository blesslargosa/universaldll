﻿using DN.DataAccess.ConfigurationData;
using DN.DataAccess.ConnectionFactory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DN.DataAccess.ConnectionFactory.Mongo
{
    public class MongoConnectionProperties : IConnectionProperties
    {
        private Dictionary<string, string> properties;

        public MongoConnectionProperties()
        {
            properties = new Dictionary<string, string>();
        }

        public string GetProperty(string name)
        {
            string propertyValue = null;

            if (properties.ContainsKey(name)) 
            {
                propertyValue = properties[name];
            }

            return propertyValue;
        }

        public void SetProperty(string name, string value)
        {
            if (this.properties.ContainsKey(name))
            {
                this.properties[name] = value;
            }
            else
            {
                this.properties.Add(name, value);
            }
        }
    }
}
