﻿using DN.DataAccess.ConnectionFactory;
using DN.DataAccess.DataCommand;
using DN.DataAccess.DataQuery;
using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DN.DataAccess.ConnectionFactory.Mongo
{
    public class MongoConnectionFactory : IDbConnectionFactory
    {
        private readonly IConnectionProperties _connectionProperties;

        public MongoConnectionFactory(IConnectionProperties connectionProperties)
        {
            this._connectionProperties = connectionProperties;
        }

        public IDbConnection CreateConnection()
        {   
            string connectionString = _connectionProperties.GetProperty("connectionstring");
            string databaseName = _connectionProperties.GetProperty("databasename");
            string collectionName = _connectionProperties.GetProperty("collectionname");
            
            var collection = new MongoClient(connectionString)
                .GetServer()
                .GetDatabase(databaseName)
                .GetCollection<BsonDocument>(collectionName);

            IDataQuery dataQuery = new MongoDataQuery(collection);
            IDataCommand dataCommand = new MongoDataCommand(collection);

            IDataAccess mongoDataAccess = new MongoDataAccess(dataCommand, dataQuery);

            return new MongoConnection(mongoDataAccess);
        }



        public IConnectionProperties ConnectionProperties
        {
            get { return this._connectionProperties; }
        }
    }
}
