﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DN.DataAccess.ConnectionFactory
{
    public interface IConnectionProperties
    {
        string GetProperty(string name);

        void SetProperty(string name, string value);
    }
}
