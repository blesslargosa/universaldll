﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DN.DataAccess.DataQuery
{
    public class FakeDataQuery : IDataQuery
    {
        public T FindByKey<T>(string key, string value)
        {
            return default(T);
        }

        public IEnumerable<T> Find<T>(string key, string value, string[] setfields = null, int skip = 0, int limit = Int32.MaxValue)
        {
            return default(IEnumerable<T>);
        }

        public IEnumerable<T> Find<T>(string query, string[] setfields = null, int skip = 0, int limit = Int32.MaxValue)
        {
            return default(IEnumerable<T>);
        }

        public IEnumerable<T> Find<T>(string query, List<MongoDB.Bson.BsonValue> notInThisValues, string[] setfields = null, int skip = 0, int limit = Int32.MaxValue)
        {
            return default(IEnumerable<T>);
        }
    }
}
